﻿using Commonn.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication6.Commonn
{
    public class CryptographyHelper
    {
        public static string EncryptedStringUrlFriendly(string text, string password)
        {
            return HttpServerUtility.UrlTokenEncode(EncryptedString(text, password));
        }

        public static string DecryptedStringUrlFriendly(string text, string password)
        {
            return DecryptedString(HttpServerUtility.UrlTokenDecode(text), password);
        }

        public static byte[] EncryptedString(string text, string password)
        {
            return EncryptedStringForUser(text, password);
        }

        public static string DecryptedString(byte[] text, string password)
        {
            return DecryptedStringForUser(text, password);
        }

        public static byte[] EncryptedStringForUser(string text, string username)
        {
            byte[] encryptedBytes;
            using (var rijndael = InitSymmetric(Rijndael.Create(), username, 256))
            {
                var bytes = Encoding.UTF8.GetBytes(text);

                encryptedBytes = Transform(bytes, rijndael.CreateEncryptor);
            }

            return encryptedBytes;
        }

        public static string DecryptedStringForUser(byte[] text, string username)
        {
            return DecryptedStringForUser(text, username, null);
        }
        public static string DecryptedStringForUser(byte[] text, string username, string maskedValue)
        {
            try
            {
                byte[] encryptedBytes = text;// StrToByteArray(text);
                string decryptedText;
                using (var rijndael = InitSymmetric(Rijndael.Create(), username, 256))
                {
                    var decryptedBytes = Transform(encryptedBytes, rijndael.CreateDecryptor);
                    decryptedText = ByteArrayToString(decryptedBytes);
                }
                return decryptedText;
            }
            catch (Exception)
            {
                if (maskedValue == null)
                    throw;
                else
                    return maskedValue;
            }
        }

        private static SymmetricAlgorithm InitSymmetric(SymmetricAlgorithm algorithm, string password, int keyBitLength)
        {
            var salt = new byte[] { 7, 78, 167, 200, 179, 234, 60, 177, 208, 39, 39 };

            const int Iterations = 854;
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, salt, Iterations))
            {
                if (!algorithm.ValidKeySize(keyBitLength))
                    throw new InvalidOperationException("Invalid size key");

                algorithm.Key = rfc2898DeriveBytes.GetBytes(keyBitLength / 8);
                algorithm.IV = rfc2898DeriveBytes.GetBytes(algorithm.BlockSize / 8);
                return algorithm;
            }
        }

        private static byte[] Transform(byte[] bytes, Func<ICryptoTransform> selectCryptoTransform)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, selectCryptoTransform(), CryptoStreamMode.Write))
                {
                    cryptoStream.Write(bytes, 0, bytes.Length);
                    cryptoStream.FlushFinalBlock();
                }
                return memoryStream.ToArray();
            }
        }  

        #region hmac

        public static string HmacEncode256_128(string key, string message)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

            byte[] messageBytes = encoding.GetBytes(message);

            return HmacEncode256_128(key, messageBytes);
        }

        public static string HmacEncode256_128(string key, byte[] messageBytes)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

            byte[] keyByte = HexToByte(key);

            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);

            byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
            //truncate to half size
            byte[] truncArray = new byte[16];
            Array.Copy(hashmessage, truncArray, truncArray.Length);

            return ByteToHex(truncArray);
        }

        #endregion

        private static byte[] StrToByteArray(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
        private static string ByteArrayToString(byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }
        public static byte[] HexToByte(string hex)
        {
            if ((hex.Length % 2) == 1) hex += '0';
            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }
        public static string ByteToHex(byte[] buff)
        {
            string sbinary = "";

            for (int i = 0; i < buff.Length; i++)
            {
                sbinary += buff[i].ToString("X2"); // hex format
            }
            return (sbinary);
        }
        public static string ResolveUrl(string relativePath, bool https)
        {
            if (https)
                return string.Format("https://{0}{1}", HttpContext.Current.Request.Url.Host, ResolveUrl(relativePath));
            else
                return string.Format("http://{0}{1}", HttpContext.Current.Request.Url.Host, ResolveUrl(relativePath));
        }
        private static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        public static string ResolveDomainUrl(string relativeUrl)
        {
            bool useHttps = false;

            string protocal = useHttps ? "https://" : "http://";
            var baseUrl = $"{protocal}{ HttpContext.Current.Request.Url.Host}";

            return $"{baseUrl}{ResolveUrl(relativeUrl)}";
        }

    }
}
