﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commonn.DTOs
{
    public class InvoiceViewModal
    {
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public string Year { get; set; }
        public long MonthId { get; set; }
        public string MonthName { get; set; }
        public string PaidAmount { get; set; }
        public string LogoImage { get; set; }
    }
}
