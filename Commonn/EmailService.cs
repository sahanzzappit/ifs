﻿using OpenPop.Pop3;
using PostmarkDotNet;
using PostmarkDotNet.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using OpenPop.Mime;

namespace WebApplication6.Commonn
{
    public interface IEmailService
    {
        Task Send(string Subject, string Content);
        Task SendBulkEmail(string Subject, string Content, List<string> Emails);
        Task SendInvoiceEmail(string Subject, string attachments, string To);
        List<EmailService.POPEmail> GetEmails();
    }
    public class EmailService : IEmailService
    {
        public async Task Send(string Subject, string Content)
        {
            try
            {
                var message = new PostmarkMessage()
                {
                    To = "sahandinendra@gmail.com",
                    TextBody = Content,
                    From = "sahan@zzappit.com",
                    //Subject = Subject
                };

                Log.Log.Info("---START SENDING SINGLE EMAIL TO :- sahandinendra@gmail.com---");

                //Postmark new TLS configurations
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var client = new PostmarkClient("13a80ed5-9595-420d-b1a0-8d9ee1237e47");
                var result = await client.SendMessageAsync(message);

                Task<EmailSetSatusDto> task = Task.Run(() => Status(result.ToString()));

                Log.Log.Info("---FINISH SENDING SINGLE EMAIL---");
                Log.Log.Info(result.Status.ToString());

            }
            catch (Exception ex)
            {
                Log.Log.Error("fail sending single email" + ex);
            }


        }

        public EmailSetSatusDto Status(string result)
        {
            var statusObj = new EmailSetSatusDto();

            statusObj.status = result;

            return statusObj;
        }

        public async Task SendBulkEmail(string Subject, string Content, List<string> Emails)
        {
            try
            {
                Log.Log.Info("---START SENDING BULK EMAILS---");

                EmailSetSatusDto emailSetSatusDto = new EmailSetSatusDto();
                Thread BulkEmail = new Thread(async () =>
                {
                    try
                    {
                        foreach (var item in Emails)
                        {
                            var message = new PostmarkMessage()
                            {
                                To = item,
                                TextBody = Content,
                                From = "sahan@zzappit.com",
                                Subject = Subject
                            };

                            //Postmark new TLS configurations
                            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                            var client = new PostmarkClient("13a80ed5-9595-420d-b1a0-8d9ee1237e47");
                            var result = await client.SendMessageAsync(message);
                        }

                    }
                    finally
                    {
                        Log.Log.Info("---FINISH SENDING BULK EMAILS---");
                    }
                });
                BulkEmail.Start();
                BulkEmail.Join();

            }
            catch (Exception ex)
            {
                Log.Log.Error("fail sending bulk email" + ex);
            }
        }

        public async Task SendInvoiceEmail(string Subject,  string attachments, string To)
        {
            try
            {
                var message = new PostmarkMessage()
                {
                    To = To,
                    From = "sahan@zzappit.com",
                    Subject = Subject,
                  //  Attachments = postmarkMessageAttachments,
                    TextBody = "Payment Invoice",
                    Headers = new HeaderCollection(new Dictionary<string, string>() { { "X-CUSTOM-HEADER", "Header content" } })

                };

                var localPath = File.ReadAllBytes(attachments);
                message.AddAttachment(localPath, Path.GetFileName(attachments));

                Log.Log.Info("---START SENDING INVOICE EMAIL TO :- "+ To +"---");

                //Postmark new TLS configurations
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                var client = new PostmarkClient("13a80ed5-9595-420d-b1a0-8d9ee1237e47");
                var result = await client.SendMessageAsync(message);

                if (result.Status == PostmarkStatus.ServerError)
                {
                    Log.Log.Error("Server error");
                }
                else
                {
                    Log.Log.Info("---FINISH SENDING INVOICE EMAIL TO :- "+ To + "---");
                }
            }
            catch (Exception ex)
            {
                Log.Log.Error("fail sending invoice email to :- "+ To + "" + ex);
            }

        }
        public List<POPEmail> GetEmails()
        {
            try
            {
                Pop3Client pop3Client = new Pop3Client();
                pop3Client.Connect("pop.gmail.com", 995, true);
                pop3Client.Authenticate("wsdfernando1127@gmail.com", "sahan123$");

                int count = pop3Client.GetMessageCount();
                var Emails = new List<POPEmail>();

                int counter = 0;
                for (int i = count; i >= 1; i--)
                {
                    OpenPop.Mime.Message message = pop3Client.GetMessage(i);
                    if (message.Headers.DateSent.Date == DateTime.Now.Date)
                    {
                        POPEmail email = new POPEmail()
                        {
                            MessageNumber = i,
                            Subject = message.Headers.Subject,
                            DateSent = message.Headers.DateSent,
                            From = string.Format("<a href = 'mailto:{1}'>{0}</a>", message.Headers.From.DisplayName, message.Headers.From.Address),
                        };
                        MessagePart body = message.FindFirstHtmlVersion();
                        if (body != null)
                        {
                            email.Body = body.GetBodyAsText();
                        }
                        else
                        {
                            body = message.FindFirstPlainTextVersion();
                            if (body != null)
                            {
                                email.Body = body.GetBodyAsText();
                            }
                        }
                        List<MessagePart> attachments = message.FindAllAttachments();

                        foreach (MessagePart attachment in attachments)
                        {
                            email.Attachments.Add(new Attachment
                            {
                                FileName = attachment.FileName,
                                ContentType = attachment.ContentType.MediaType,
                                Content = attachment.Body
                            });
                        }
                        Emails.Add(email);
                        counter++;
                    }

                }

                return Emails;
            }
            catch (Exception ex)
            {
                Log.Log.Error("Error receiving email:-", ex);
                throw;
            }
        }

        public class EmailSetSatusDto
        {
            public string status { get; set; }
        }
        [Serializable]
        public class POPEmail
        {
            public POPEmail()
            {
                this.Attachments = new List<Attachment>();
            }
            public int MessageNumber { get; set; }
            [AllowHtml]
            public string From { get; set; }
            [AllowHtml]
            public string Subject { get; set; }
            [AllowHtml]
            public string Body { get; set; }
            public DateTime DateSent { get; set; }
            [AllowHtml]
            public List<Attachment> Attachments { get; set; }
        }
        [Serializable]
        public class Attachment
        {
            public string FileName { get; set; }
            public string ContentType { get; set; }
            public byte[] Content { get; set; }
        }
    }
}
