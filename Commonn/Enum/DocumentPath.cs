﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commonn.Enum
{
   
        public enum DocumentPathEnum
        {
            [Description(@"\Payments\")]
            PaymentsFilesPath,
            [Description(@"\PendingBooks\")]
            PendingBooksPath,
            [Description(@"\PaymentInvoice\")]
            PaymentInvoice,
        }


    
}
