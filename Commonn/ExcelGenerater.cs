﻿using Commonn.Enum;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using WebApplication6.Commonn;

namespace WebApplication6.Commonn
{
    public class ExcelGenerater
    {
        public static string ApplicationPath
        {
            get
            {
                var s = HostingEnvironment.ApplicationPhysicalPath;
                return s.TrimEnd(new char[] { '\\' });


            }
        }

    }

    public class Common
    {
        //public static readonly object CryptographyHelper;

        public static string CleanFileName(string fileName)
        {
            return System.Text.RegularExpressions.Regex.Replace(fileName, @"[\[\]\\\^\$\|\?\*\(\)\{\}%,;:><!@#&\-\+/]", "_");
        }

        public static string ReadEnumDescription(DocumentPathEnum path)
        {
            var type = typeof(DocumentPathEnum);
            var memInfo = type.GetMember(path.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute),
                false);
            return ((DescriptionAttribute)attributes[0]).Description;
        }

        private static string ResolveUrl(string relativeUrl)
        {
            if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

            if (relativeUrl.Length == 0 || relativeUrl[0] == '/' || relativeUrl[0] == '\\')
                return relativeUrl;

            int idxOfScheme = relativeUrl.IndexOf(@"://", StringComparison.Ordinal);
            if (idxOfScheme != -1)
            {
                int idxOfQM = relativeUrl.IndexOf('?');
                if (idxOfQM == -1 || idxOfQM > idxOfScheme) return relativeUrl;
            }

            StringBuilder sbUrl = new StringBuilder();
            sbUrl.Append(HttpRuntime.AppDomainAppVirtualPath);
            if (sbUrl.Length == 0 || sbUrl[sbUrl.Length - 1] != '/') sbUrl.Append('/');

            // found question mark already? query string, do not touch!
            bool foundQM = false;
            bool foundSlash; // the latest char was a slash?
            if (relativeUrl.Length > 1
                && relativeUrl[0] == '~'
                && (relativeUrl[1] == '/' || relativeUrl[1] == '\\'))
            {
                relativeUrl = relativeUrl.Substring(2);
                foundSlash = true;
            }
            else foundSlash = false;
            foreach (char c in relativeUrl)
            {
                if (!foundQM)
                {
                    if (c == '?') foundQM = true;
                    else
                    {
                        if (c == '/' || c == '\\')
                        {
                            if (foundSlash) continue;
                            else
                            {
                                sbUrl.Append('/');
                                foundSlash = true;
                                continue;
                            }
                        }
                        else if (foundSlash) foundSlash = false;
                    }
                }
                sbUrl.Append(c);
            }

            return sbUrl.ToString();
        }

        /// <summary>
        /// resolve relative url to a complete url
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns>complete url</returns>
        public static string ResolveUrl(string relativePath , bool https)
        {
            var host = string.Empty;
            int port = 0;

            if (HttpContext.Current != null)
            {
                host = HttpContext.Current.Request.Url.Host;
                port = HttpContext.Current.Request.Url.Port;

                host = host + ":" + port;
            }
            else
            {
                var ctx = (HttpContext)Thread.GetData(Thread.GetNamedDataSlot("Context"));
                if (ctx != null)
                {
                    host = ctx.Request.Url.Host;
                }
                else
                {
                    host = ExcelGenerater.ApplicationPath;
                }

            }

            if (https)
                return string.Format("https:\\{0}{1}", host, ResolveUrl(relativePath));
            else
                // return string.Format("http:\\{0}{1}", host, ResolveUrl(relativePath));
                return string.Format(ResolveUrl(relativePath));
        }


        //public static string GetEncryptedRelativePath(string filePath)
        //{
        //    string key = ConfigurationManager.AppSettings["publickey"].ToString();

        //    if (!string.IsNullOrEmpty(key))
        //    {

        //        string encryptedFilePath = CryptographyHelper.EncryptedStringUrlFriendly(filePath, key);
        //        return @"~/document/" + encryptedFilePath;
        //    }

        //    return string.Empty;
        //}
        public static string GetEncryptedRelativePath(string filePath, string displayName = "")
        {
            string key = ConfigurationManager.AppSettings["publickey"].ToString();
            string fileName = !string.IsNullOrWhiteSpace(displayName) ? displayName : Path.GetFileName(filePath);

            if (!string.IsNullOrEmpty(key))
            {
                string encryptedFilePath = CryptographyHelper.EncryptedStringUrlFriendly(filePath, key);
                return @"~/Document/" + encryptedFilePath + "/" + fileName;
            }

            return string.Empty;
        }

        public static byte[] ReadFile(string path)
        {
            byte[] fileBytes = File.ReadAllBytes(path);
            return fileBytes;
        }

        public static string SaveAsExcel(string reportPath, string fileNamePrefix, out string fileName, DataSet dataSet, DocumentPathEnum documentPathEnum)
        {
            LocalReport lr = new LocalReport
            {
                ReportPath = ExcelGenerater.ApplicationPath + reportPath,
            };

            lr.DataSources.Add(new ReportDataSource("DataSet1", dataSet.Tables[0]));
            string mimeType, encoding, extension;

            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "EXCEL",
                   null,
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();

            fileName = String.Format($"{fileNamePrefix}_{DateTime.Now.ToString("yyyy_MM_dd_hh:mm:ss")}.xls");
            string cleanedFileName = Common.CleanFileName(fileName);
            var folder = Common.ReadEnumDescription(documentPathEnum);
            string outputPath = rootPath + folder + cleanedFileName;
            var saveAs = string.Format("{0}", outputPath);


            using (var stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write))
            {
                stream.Write(renderedBytes, 0, renderedBytes.Length);
                stream.Close();
            }

            return cleanedFileName;

        }

    }
}

