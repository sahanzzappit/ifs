﻿using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Parsing;
using System.Drawing;
using System.Diagnostics;



namespace Commonn.Helpers
{
    public class Watermark
    {
        public static string AddWatermark(string outputPath, string rootPath, string folder, string originalFile)
        {
            PdfLoadedDocument loadedDocument = new PdfLoadedDocument(outputPath);
            PdfPageBase loadedPage = loadedDocument.Pages[0];
            PdfGraphics graphics = loadedPage.Graphics;
            var font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
            PdfGraphicsState state = graphics.Save();
            graphics.SetTransparency(0.25f);
            graphics.RotateTransform(-40);
            graphics.DrawString("Original", font, PdfPens.Gray, PdfBrushes.Gray, new PointF(0, 250));
            loadedDocument.Save(rootPath + folder + originalFile);
            loadedDocument.Close(true);
            Process.Start(rootPath + folder + originalFile);

            return rootPath + folder + originalFile;
        }
    }
}
