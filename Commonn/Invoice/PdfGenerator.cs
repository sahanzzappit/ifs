﻿using Commonn.Enum;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commonn.Helpers;
using WebApplication6.Commonn;
using Commonn.DTOs;

namespace Commonn.Invoice
{
    public class PdfGenerator
    {
        public static string SaveInvoicePdf(string fileName, string data, InvoiceViewModal invoice)
        {
            string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();

            string cleanedFileName = Common.CleanFileName(fileName);
            var folder = Common.ReadEnumDescription(DocumentPathEnum.PaymentInvoice);
            string outputPath = rootPath + folder + cleanedFileName;
            var saveAs = string.Format("{0}", outputPath);

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

            using (var stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write))
            {
                StringReader sr = new StringReader(data);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
            }

            var originalFile = Common.CleanFileName("Payment_Invoice_original" + invoice.StudentId + "_" + invoice.MonthId + "_" + invoice.Year + ".pdf");
            var addWatermark = Watermark.AddWatermark(outputPath, rootPath, folder, originalFile);


            return addWatermark;
        }
    }
}
