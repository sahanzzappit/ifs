﻿using Commonn.DTOs;
using Commonn.Enum;
using RazorEngine;
using RazorEngine.Templating;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Commonn.Invoice
{
    public interface ITemplateProcessor
    {
        string GenerateInvoiceTemplate(string tempate, InvoiceViewModal invoice);
    }
    public class TemplateProcessor: ITemplateProcessor
    {
        public string GenerateInvoiceTemplate(string tempate, InvoiceViewModal invoice)
        {

            var razorTemplateString = Engine.Razor.RunCompile(tempate, DateTime.Now.TimeOfDay.ToString(), null, invoice);

            var PDFView = new ViewAsPdf("Print", razorTemplateString)
            {
                FileName = "Invoice_"+ invoice.StudentId + "_" + invoice.MonthId + "_" + invoice.Year + ".pdf",
            };


            var file =  PdfGenerator.SaveInvoicePdf(PDFView.FileName, razorTemplateString, invoice);

            return file;
        }
    }

}
