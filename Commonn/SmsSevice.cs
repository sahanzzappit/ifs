﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebApplication6.Commonn.Log;

namespace WebApplication6.Commonn
{
    public interface ISmsService
    {
        bool SendSms(string cellNo, string sms);
        bool SendBulkSms(List<string> cellNo, string sms);
    }
    public class SmsSevice : ISmsService
    {

        public bool SendSms(string cellNo, string sms)
        {
            Log.Log.Info("-----START SENDING SINGLE SMS TO "+ cellNo +"------");
            try
            {
                SerialPort serialPort = new SerialPort();
                serialPort.PortName = "COM9";
                serialPort.Open();
                serialPort.BaudRate = 9600;
                serialPort.Parity = Parity.None;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.DtrEnable = true;
                serialPort.RtsEnable = true;
                serialPort.NewLine = System.Environment.NewLine;

                string messages = null;
                messages = sms;

                if (serialPort.IsOpen == true)
                {
                    if (!string.IsNullOrEmpty(cellNo))
                    {
                        serialPort.WriteLine("AT" + (char)(13));
                        Thread.Sleep(4);
                        serialPort.WriteLine("AT+CMGF=1" + (char)(13));
                        Thread.Sleep(5);
                        serialPort.WriteLine("AT+CMGS=\"" + cellNo + "\"");
                        Thread.Sleep(10);
                        serialPort.WriteLine("" + messages + (char)(26));
                    }
                    serialPort.Close();
                    Log.Log.Info("-----FINISH SENDING SINGLE SMS TO------");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Log.Error("fail sending SMS to :- "+ cellNo + ex);
                throw;
            }
        }

        public bool SendBulkSms(List<string> cellNo, string sms)
        {
            Log.Log.Info("----- START SENDING BULK SMS ------");
            try
            {
                SerialPort serialPort = new SerialPort();
                serialPort.PortName = "COM9";
                serialPort.Open();
                serialPort.BaudRate = 9600;
                serialPort.Parity = Parity.None;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                //serialPort.Handshake = Handshake.RequestToSend;
                serialPort.DtrEnable = true;
                serialPort.RtsEnable = true;
                serialPort.NewLine = System.Environment.NewLine;

                string messages = null;
                messages = sms;

                if (serialPort.IsOpen == true)
                {
                    foreach (var number in cellNo)
                    {
                        if (!string.IsNullOrEmpty(number))
                        {
                            serialPort.WriteLine("AT" + (char)(13));
                            Thread.Sleep(4);
                            serialPort.WriteLine("AT+CMGF=1" + (char)(13));
                            Thread.Sleep(5);
                            serialPort.WriteLine("AT+CMGS=\"" + number + "\"");
                            Thread.Sleep(10);
                            serialPort.WriteLine("" + messages + (char)(26));
                        }
                    }
                    serialPort.Close();
                    Log.Log.Info("----- FINISH SENDING BULK SMS ------");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Log.Error("fail sending bulk SMS "+ ex);
                throw;
            }
        }
    }
}
