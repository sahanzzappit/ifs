﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication6
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
            
            bundles.Add(new ScriptBundle("~/Scripts/DataTables/"));
            bundles.Add(new StyleBundle("~/Content/Data Tables/jquery.dataTables.min.css"));
            bundles.Add(new StyleBundle("~/Content/Login.css"));
            bundles.Add(new ScriptBundle("~/Scripts/plugin/jquery.validation.js"));
            bundles.Add(new ScriptBundle("~/Scripts/script.js"));
            bundles.Add(new ScriptBundle("~/Scripts/DataTables/Tables.js"));
            bundles.Add(new ScriptBundle("~/Scripts/DataTables/jquery-ui.js"));
            bundles.Add(new ScriptBundle("~/Scripts/plugin/footable.core.min.js"));
            bundles.Add(new ScriptBundle("~/Scripts/pnotify.js"));
            bundles.Add(new StyleBundle("~/Content/plugin/pnotify.css"));
            bundles.Add(new ScriptBundle("~/Scripts/plugin/Summernote/SummerNote.js"));
        }
    }
}
