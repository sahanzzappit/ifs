﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using WebApplication6.App_Start;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{

    [SessionCheck]
    public class AdminDetailController : Controller
    {

        private readonly DataContext _dbContext = null;
        private ApplicationUserManager _userManager;

        public AdminDetailController()
        {
            _dbContext = new DataContext();
        }
        public AdminDetailController(ApplicationUserManager userManager)
        {
            this._userManager = userManager;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: AdminDetail
        public ActionResult Index()
        {
  
            return View();
        }
       
        [HttpGet]
        public async Task<ActionResult> GetDetail()
        {

            var id = Session["Id"].ToString();
            var user = await _userManager.FindByIdAsync(id);

            var userDetails = new DetailViewModal()
            {
                Id = user.Id,
                UserName = user.UserName,
                CurrentPassword = user.PasswordHash
            };

            return Json(new { userDetails }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(string password, string id, string currentPassword, string hdnPassword)
        {
           
            try
            {

                var user = await _userManager.FindByIdAsync(id);
                var result = await _userManager.ChangePasswordAsync(user.Id, currentPassword, password);

                if (result.Succeeded)
                    return Json("done");
                else
                    return Json(result.Errors);


            }
            catch (Exception e)
            {
                return Json("error");
                throw;
            }

        }
    }

    public class DetailViewModal
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string CurrentPassword { get; set; }
        
    }
}