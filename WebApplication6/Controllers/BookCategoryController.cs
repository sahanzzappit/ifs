﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{

    [SessionCheck]
    public class BookCategoryController : Controller
    {
        private readonly DataContext  _dbContext = null;

        public BookCategoryController()
        {
            _dbContext = new DataContext();
        }
        // GET: BookCategory
        public ActionResult Index()
        {

            return View();
        }
        [HttpGet]
        public ActionResult GetList()
        {
            try
            {
                List<BookCategoryViewMadal> bookCategoryViewMadals = new List<BookCategoryViewMadal>();

                var details = _dbContext.BookCategories.ToList();
                

                foreach (var detail in details)
                {
                    var books = _dbContext.Books.Where(x => x.CategoryId == detail.Id).ToList();
                    var withcpy = _dbContext.Books.Where(x => x.CategoryId == detail.Id).Select(x => x.NoOfCopies).ToList();

                    if (books.Count() ==  0)
                    {
                        BookCategoryViewMadal data = new BookCategoryViewMadal
                        {
                            Id = detail.Id,
                            Name = detail.CategoryName,
                            NoOfBooks = 0,
                            WithCopies = 0

                        };

                        bookCategoryViewMadals.Add(data);
                    }
                    else
                    {
                        BookCategoryViewMadal data = new BookCategoryViewMadal
                        {
                            Id = detail.Id,
                            Name = detail.CategoryName,
                            NoOfBooks = books.Count(),
                            WithCopies = withcpy.Sum()

                        };

                        bookCategoryViewMadals.Add(data);
                    }

            
                }

                return Json(new { data = bookCategoryViewMadals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public JsonResult AddCategory(string Name)
        {
            try
            {
    
                if (_dbContext.BookCategories.Any(x => x.CategoryName == Name))
                {
                    return Json("false");
                }

                else
                {
                    BookCategory bookCategory = new BookCategory
                    {
                        CategoryName = Name,
                        CreatedDate = DateTime.Now
                    };

                    _dbContext.BookCategories.Add(bookCategory);

                    _dbContext.SaveChanges();

                    return Json("success");
                }

                
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public JsonResult GetData(long Id)
        {
            try
            {
                List<BookCategoryViewMadal> bookCategoryViewMadals = new List<BookCategoryViewMadal>();

                var id = _dbContext.BookCategories.Where(x => x.Id == Id).ToList();

                foreach (var item in id)
                {
                    BookCategoryViewMadal bookCategory = new BookCategoryViewMadal
                    {
                        Id = item.Id,
                        Name = item.CategoryName
                    };
                    bookCategoryViewMadals.Add(bookCategory);
                }

                return Json(new { bookCategoryViewMadals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool EditCategory(long Id ,string Name)
        {
            try
            {
      
                    BookCategory bookCategory = _dbContext.BookCategories.Find(Id);

                    bookCategory.CategoryName = Name;

                    _dbContext.SaveChanges();

                    return true;

                

            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public JsonResult DeleteCategory(long Id)
        {
            try
            {              

                if (_dbContext.Books.Any(x => x.BookCategory.Id == Id))
                {
                    return Json("error");
                }
                else
                {
                    var categoryId = _dbContext.BookCategories.Find(Id);

                    _dbContext.BookCategories.Remove(categoryId);

                    var categories = _dbContext.StudentLibDetails.Where(x => x.BookCategoryId == Id).ToList();
                    _dbContext.StudentLibDetails.RemoveRange(categories);

                    _dbContext.SaveChanges();

                    return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }











        public class BookCategoryViewMadal
        {
            public long Id { get; set; }
            public string Name { get; set; }
            public long NoOfBooks { get; set; }
            public long WithCopies { get; set; }
        }
    }
}