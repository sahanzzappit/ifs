﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn.Log;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class BooksController : Controller
    {
        private readonly DataContext _dbContext= null;

        public BooksController()
        {
            _dbContext = new DataContext();
        }


        // GET: Books
        public ActionResult Index()
        {



            ViewBag.categoryName = new SelectList(_dbContext.BookCategories.ToList(), "CategoryName", "CategoryName");
            ViewBag.categoryId = new SelectList(_dbContext.BookCategories.ToList(), "Id", "CategoryName");

            return View();
        }
        public JsonResult GetList()
        {
            try
            {
                List<BooksViewModal> booksViewModals = new List<BooksViewModal>();

                var List = _dbContext.Books.ToList();

                foreach (var item in List)
                {
                    BooksViewModal data = new BooksViewModal
                    {
                        Id = item.Id,
                        BookNo = item.BookNo,
                        BookName = item.BookName,
                        CategoryId = item.CategoryId,
                        CategoryName = _dbContext.BookCategories.Where(x => x.Id == item.CategoryId).Select(x => x.CategoryName).FirstOrDefault(),
                        NoOfCopies = item.NoOfCopies,
                        BookDetail = item.BookDetail
                    };
                    booksViewModals.Add(data);
                }

                return Json(new { data = booksViewModals }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult SaveData(long No, long CategoryId, string BookName, long NoOfCopies, List<BookCopies> bookCopies)
        {
            try
            {

                if (bookCopies != null)
                {
                    var bookNoList = bookCopies.Select(x => x.BookNo).ToList();
                    var bookCopyNoList = bookCopies.Select(x => x.CopyNo).ToList();
                    var alreadyExistBook = _dbContext.Books.Where(x => bookCopyNoList.Contains(x.BookNo) || bookNoList.Contains(x.BookNo)).Any();
                    var alreadyExistCopy = _dbContext.BookCopies.Where(x => bookCopyNoList.Contains(x.CopyNo) || bookNoList.Contains(x.CopyNo)).Any();

                    if (alreadyExistBook || alreadyExistCopy)
                    {
                        return Json("error");
                    }

                }

                if (_dbContext.Books.Any(x => x.BookNo == No) || _dbContext.BookCopies.Any(x => x.CopyNo == No))
                {
                    return Json("error");
                }
                if (bookCopies == null)
                {
                    var book = new Books
                    {
                        BookNo = No,
                        CategoryId = CategoryId,
                        BookName = BookName,
                        NoOfCopies = NoOfCopies,
                        BookCategory = _dbContext.BookCategories.Where(x => x.Id == CategoryId).FirstOrDefault()
                    };

                    _dbContext.Books.Add(book);

                    _dbContext.BookCopies.Add(new BookCopies()
                    {
                        BookNo = No,
                        CopyNo = No,
                        IsAvailable = true,
                        Books = book
                    });
                    _dbContext.SaveChanges();

                    return Json("success");
                }
                else
                {

                    var books = new Books
                    {
                        BookNo = No,
                        CategoryId = CategoryId,
                        BookName = BookName,
                        NoOfCopies = NoOfCopies,
                        BookCategory = _dbContext.BookCategories.Where(x => x.Id == CategoryId).FirstOrDefault()
                    };

                    _dbContext.Books.Add(books);


                    _dbContext.BookCopies.Add(new BookCopies()
                    {
                        BookNo = No,
                        CopyNo = No,
                        IsAvailable = true,
                        Books = books
                    });

                    foreach (var item in bookCopies)
                    {
                        var data = new BookCopies
                        {
                            BookNo = item.BookNo,
                            CopyNo = item.CopyNo,
                            Books = books,
                            IsAvailable = true
                        };
                        _dbContext.BookCopies.Add(data);
                    }
                    _dbContext.SaveChanges();
                    return Json("success");
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }


        //get the maximum book no from database
        [HttpGet]
        public ActionResult GetMaxValue()
        {
            try
            {

                var list = _dbContext.BookCopies.ToList();
                 

                if (list.Count == 0)
                {
                    var val = 1;

                    return Json(new { val }, JsonRequestBehavior.AllowGet);
                }

                    var maxVal1 = _dbContext.BookCopies.Max(x => x.CopyNo);
                    var maxVal2 = _dbContext.Books.Max(x => x.BookNo);

                if (maxVal1 >= maxVal2)
                {
                     return Json(new { maxVal1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { maxVal2 }, JsonRequestBehavior.AllowGet);
                }

                   
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public JsonResult GetData(long Id)
        {
            try
            {
                List<BooksViewModal> booksViewModals = new List<BooksViewModal>();

                var details = _dbContext.Books.Where(x => x.Id == Id).ToList();

                foreach (var detail in details)
                {
                    BooksViewModal data = new BooksViewModal
                    {
                        Id = detail.Id,
                        BookNo = detail.BookNo,
                        BookName = detail.BookName,
                        CategoryId = detail.CategoryId,
                        CategoryName = _dbContext.BookCategories.Where(x => x.Id == detail.CategoryId).Select(x => x.CategoryName).FirstOrDefault(),
                        NoOfCopies = detail.NoOfCopies,
                        BookDetail = detail.BookDetail
                    };
                    booksViewModals.Add(data);
                }

                return Json(new { booksViewModals }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public JsonResult GetCopies(long Id)
        {
            try
            {
                List<CopiesViewModal> copiesViewModals = new List<CopiesViewModal>();

                var List = _dbContext.Books.Where(x => x.Id == Id).ToList();

                foreach (var item in List)
                {
                    var cpList = _dbContext.BookCopies.Where(x => x.BookNo == item.BookNo).ToList();

                    foreach (var item1 in cpList)
                    {

                        CopiesViewModal data = new CopiesViewModal
                        {
                            CopyNo = item1.CopyNo
                        };
                        copiesViewModals.Add(data);
                    }

                }

                return Json(new { copiesViewModals }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //get book edit details
        [HttpPost]
        public ActionResult Changed(long id)
        {
            try
            {

                var list = _dbContext.BookCopies.Where(x => x.BookNo == id).Select(x => x.Id).ToList();

                foreach (var item in list)
                {
                    var bookCopies = _dbContext.BookCopies.Find(item);
                    _dbContext.BookCopies.Remove(bookCopies);
                }

                var list2 = _dbContext.BookLends.Where(x => x.BookId == id).Select(x => x.Id).ToList();

                foreach (var item in list2)
                {
                    var bookLend = _dbContext.BookLends.Find(item);
                    _dbContext.BookLends.Remove(bookLend);
                }

                _dbContext.SaveChanges();

                return Json("success");
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        [HttpPost]
        public ActionResult EditBook(long Id ,long No, string Name, string Category, long NoOfCopies, long IniNo, List<BookCopies> bookCo,List<BookCopies> bookCo2,  string WithOneCopy)
        {
            try
            {
                //var alreadyExistCount = 0;
                //if (bookCo2 != null)
                //{
                //    foreach (var item in bookCo2)
                //    {
                //        if (this._dbContext.Books.Any(x => x.BookNo == item.CopyNo))
                //        {
                //            alreadyExistCount++;
                //        }
                //        else if (this._dbContext.BookCopies.Any(x => x.CopyNo == item.CopyNo))
                //        {
                //            alreadyExistCount++;
                //        }
                //    }
                //}
                //if (alreadyExistCount > 0)
                //{
                //    return Json("error1");
                //}
                //if (_dbContext.Books.Any(x => x.BookNo == No && x.BookNo != IniNo) || _dbContext.BookCopies.Any(x => x.CopyNo == No))
                //{
                //    return Json("error");
                //}

                    var list = _dbContext.BookCopies.Where(x => x.BookNo == IniNo).Select(x => x.Id).ToList();

                    foreach (var item in list)
                    {
                        var bookCopies = _dbContext.BookCopies.Find(item);
                        bookCopies.BookNo = No;
                    }

                    var lendList = _dbContext.BookLends.Where(x => x.BookId == IniNo).Select(x => x.Id).ToList();

                    foreach (var item in lendList)
                    {
                        var bookLend = _dbContext.BookLends.Find(item);
                        bookLend.BookId = No;
                    }

                    var retList = _dbContext.BookReturns.Where(x => x.BookId == IniNo).Select(x => x.Id).ToList();

                    foreach (var item in retList)
                    {
                        var bookReturn = _dbContext.BookReturns.Find(item);
                        bookReturn.BookId = No;
                    }

                    var books = _dbContext.Books.Find(Id);

                    books.BookNo = No;
                    books.BookName = Name;
                    books.CategoryId = _dbContext.BookCategories.Where(x => x.CategoryName == Category).Select(x => x.Id).FirstOrDefault();
                    books.NoOfCopies = NoOfCopies;


                    if (bookCo != null)
                    {
                        var copyList1 = _dbContext.BookCopies.Where(x => x.BookNo == IniNo).Select(x => x.Id).ToList();

                        foreach (var item in copyList1)
                        {
                            BookCopies bookCopy = _dbContext.BookCopies.Find(item);

                            _dbContext.BookCopies.Remove(bookCopy);
                        }


                        foreach (var item in bookCo)
                        {
                            BookCopies bookCopy = new BookCopies
                            {
                                CopyNo = item.CopyNo,
                                BookNo = item.BookNo,
                                Books = this._dbContext.Books.Where(x => x.BookNo == item.BookNo).FirstOrDefault(),
                                IsAvailable = true
                            };

                            _dbContext.BookCopies.Add(bookCopy);
                        }
                    }

                    if (bookCo2 != null)
                    {
                        foreach (var item in bookCo2)
                        {
        
                            var copyList2 = _dbContext.BookCopies.Where(x => x.BookNo == IniNo).Select(x => x.Id).ToList();

                            foreach (var copy in copyList2)
                            {
                                var Copy = _dbContext.BookCopies.Find(copy);

                                _dbContext.BookCopies.Remove(Copy);
                            }

                            var bookCopy = new BookCopies
                            {
                                CopyNo = item.CopyNo,
                                BookNo = item.BookNo,
                                Books = this._dbContext.Books.Where(x => x.BookNo == item.BookNo).FirstOrDefault(),
                                IsAvailable = true
                            };

                            _dbContext.BookCopies.Add(bookCopy);
                        }
                    }

                    if (WithOneCopy != null)
                    {
                        var copyList1 = _dbContext.BookCopies.Where(x => x.BookNo == IniNo).Select(x => x.Id).ToList();

                        foreach (var item in copyList1)
                        {
                            var bookCopy = _dbContext.BookCopies.Find(item);

                            _dbContext.BookCopies.Remove(bookCopy);
                        }

                    }

                    this._dbContext.SaveChanges();

                    return Json("success");

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult DeleteBook(long Id)
        {
            try
            {
                    var bookId = _dbContext.Books.Find(Id);

                    _dbContext.Books.Remove(bookId);

                    var list = _dbContext.BookCopies.Where(x => x.BookNo == bookId.BookNo).Select(x => x.Id).ToList();

                    foreach (var item in list)
                    {
                        BookCopies bookCopies = _dbContext.BookCopies.Find(item);

                        _dbContext.BookCopies.Remove(bookCopies);
                    }

                    var lendList = _dbContext.BookLends.Where(x => x.BookId == bookId.BookNo).Select(x => x.Id).ToList();

                    foreach (var item in lendList)
                    {
                        BookLend bookLend = _dbContext.BookLends.Find(item);

                        _dbContext.BookLends.Remove(bookLend);
                    }

                    var retList = _dbContext.BookReturns.Where(x => x.BookId == bookId.BookNo).Select(x => x.Id).ToList();

                    foreach (var item in retList)
                    {
                        BookReturn bookReturn = _dbContext.BookReturns.Find(item);

                        _dbContext.BookReturns.Remove(bookReturn);
                    }

                    _dbContext.SaveChanges();


                    return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
                

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public JsonResult TestQrScanner(string Data, long Id)
        {

            try
            {
                var book = _dbContext.Books.Where(x => x.Id == Id).FirstOrDefault();
                book.BookDetail = Data;

                _dbContext.SaveChanges();

                return Json(200);
            }
            catch (Exception ex)
            {
                Log.Error("fail to read QR code" + ex);
                return Json(500);
            }            
        }



        public class BooksViewModal
        {
            public long Id { get; set; }
            public long BookNo { get; set; }
            public string BookName { get; set; }
            public long CategoryId { get; set; }
            public string CategoryName { get; set; }
            public long NoOfCopies { get; set; }
            public string BookDetail { get; set; }

        }
        public class CopiesViewModal
        {
            public long CopyNo { get; set; }
        }
    }
}