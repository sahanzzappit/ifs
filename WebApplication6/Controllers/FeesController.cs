﻿using Commonn;
using Commonn.DTOs;
using Commonn.Enum;
using Commonn.Invoice;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn;
using WebApplication6.Commonn.Log;
using WebApplication6.Helpers;
using WebApplication6.Helpers.PdfHelper;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class FeesController : Controller
    {
        private readonly DataContext _dbContext = null;
        private readonly IEmailService _emailService = null;
        private readonly ITemplateProcessor _templateProcessor = null;

        public FeesController(IEmailService emailService, ITemplateProcessor templateProcessor)
        {
            _dbContext = new DataContext();
            this._emailService = emailService;
            this._templateProcessor = templateProcessor;
        }
        // GET: Fees
        public ActionResult Index()
        {


            ViewBag.stuName = new SelectList(_dbContext.Students.ToList(), "Name", "Name");
            ViewBag.grades = new SelectList(_dbContext.Grades.ToList(), "Id", "Name");

         
            return View();
        }

        [HttpGet]
        public JsonResult GetList(int year, string grade, string scheduleId)
        {
            var students = _dbContext.Students.ToList();
            var months = _dbContext.months.ToList();
            var grades = _dbContext.ClassSchedules.ToList();

            if (grade != null && grade != string.Empty)
            {
                students = students.Where(x => x.ClassSchedule.GradeId == int.Parse(grade)).ToList();
            }

            if (scheduleId != null && scheduleId != string.Empty)
            {
                students = students.Where(x => x.ClassScheduleId == int.Parse(scheduleId)).ToList();
            }

                List<FeesViewModal> feesViewModals = new List<FeesViewModal>();

            foreach (var student in students)
            {
                FeesViewModal data = new FeesViewModal
                {
                    StudentName = student.Name,
                    Id = student.Id,
                    Fee = _dbContext.ClassSchedules.Where(x => x.Id == student.ClassScheduleId).Select(x => x.Grade.Fee).FirstOrDefault(),
                    GradeId = _dbContext.Grades.Where(x => x.Id == student.ClassSchedule.GradeId).Select(x => x.Id).FirstOrDefault(),
                    ClassScheduleId = student.ClassScheduleId
                };

                var feesCheck = _dbContext.classFees.Where(x => x.StudentId == student.Id && x.Year == year).ToList();

                var monthList = new List<MonthViewModal>();

                foreach (var month in months)
                {
                    bool isPaid = false;
                    isPaid = feesCheck.Any(x => x.MonthId == month.Id);
                    string date = isPaid ? feesCheck.Where(x => x.MonthId == month.Id ).Select(y => y.PaidDate).FirstOrDefault().ToString("dd/MM/yyyy") : null;

                    monthList.Add(new MonthViewModal
                    {
                        Id = month.Id,
                        Name = month.Name,
                        IsPaid = isPaid,
                        DateTime = date,
                        Year = year
                    });
                }

                data.Months = monthList;
                

                feesViewModals.Add(data);
                
            }

            return Json(new { data = feesViewModals }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool SavePaid(long StudentId, long MonthId, int Year, string Fee)    
        {

            try
            {       
                ClassFee classFee = new ClassFee
                {
                    StudentId = StudentId,
                    MonthId = MonthId,
                    PaidDate = DateTime.Now,
                    Year = Year,
                    Fee = decimal.Parse(Fee)
                };

                this._dbContext.classFees.Add(classFee);
                this._dbContext.SaveChanges();

                 return true;

            }
            catch(Exception e)
            {
                return false;
            }
        }
        [HttpGet]
        public JsonResult GetDropDownVal(long grdVal)
        {
            try
            {
                var schedules = _dbContext.ClassSchedules.Where(x => x.GradeId == grdVal).ToList();
               

                List<ScheduleViewModel> scheduleViewModels = new List<ScheduleViewModel>();

                foreach (var schedule in schedules)
                {
                    var students = _dbContext.Students.Where(x => x.ClassScheduleId == schedule.Id).ToList();

                    foreach (var student in students)
                    {
                        ScheduleViewModel data = new ScheduleViewModel
                        {
                            ScheduleId = schedule.Id,
                            DateName = _dbContext.DateNames.Where(x => x.Id == schedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                            StartTime = schedule.SartTime.ToString(),
                            EndTime = schedule.EndTime.ToString()
                        };

                        if (scheduleViewModels.Any(x => x.ScheduleId == schedule.Id))
                        {
                            scheduleViewModels.Remove(data);
                        }
                        else
                        {
                            scheduleViewModels.Add(data);
                        }
                        
                        
                    }

                }

                return Json(new { scheduleViewModels}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteFee(long StudentId, long MonthId, int Year)
        {      

            var classFee = _dbContext.classFees.Where(x => x.StudentId == StudentId && x.MonthId == MonthId && x.Year == Year);
    

            _dbContext.classFees.RemoveRange(classFee);
            _dbContext.SaveChanges();

            return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> SendInvoice(long StudentId, long MonthId, int Year)
        {
            var month = _dbContext.months.Where(x => x.Id == MonthId).Select(x => x.Name).FirstOrDefault();
            var studentData = _dbContext.Students.Where(x => x.Id == StudentId).Include(x => x.ClassSchedule)
                                .Include(x => x.ClassSchedule.Grade).FirstOrDefault();
            try
            {

                var invoiceViewModal = new InvoiceViewModal()
                {
                    StudentId = studentData.StudentId,
                    Year = Year.ToString(),
                    MonthId = MonthId,
                    MonthName = month,
                    PaidAmount = studentData.ClassSchedule.Grade.Fee.ToString(),
                    StudentName = studentData.Name
                };

                var email = this._dbContext.Students.Where(x => x.Id == StudentId).Select(i => i.Email).FirstOrDefault();
                var invoiceTemplate = this._dbContext.Invoice.Where(x => x.InvoiceType == (int)InvoiceTypeEnum.Payment_Invoice).Select(k => k.Body).FirstOrDefault();   

                //feed invoice template
                var originalInvoice = this._templateProcessor.GenerateInvoiceTemplate(invoiceTemplate, invoiceViewModal);
                var invoceDocumentName = Path.GetFileName(originalInvoice);
                //update fee table
                var fee = this._dbContext.classFees.Where(x => x.StudentId == StudentId && x.MonthId == MonthId && x.Year == Year).FirstOrDefault();
                fee.IsInvoiceSent = true;
                fee.Invoice = Common.GetEncryptedRelativePath(originalInvoice);

                this._dbContext.SaveChanges();

                await this._emailService.SendInvoiceEmail("Payment Invoice", originalInvoice, email);

                return Json("sent");
            }
            catch (Exception ex)
            {
                Log.Error("fail sending invoice email" + ex);
                return Json("error");
            }
        }


    }

    public class FeesViewModal
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string StudentName { get; set; }
        public long MonthId { get; set; }
        public string Month { get; set; }
        public DateTime PaidDate { get; set; }
        public List<MonthViewModal> Months { get; set; }
        public decimal Fee { get; set; }
        public long GradeId { get; set; }
        public long ClassScheduleId { get; set; }

    }

    public class MonthViewModal
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsPaid { get; set; }
        public string DateTime { get; set; }
        public int Year { get; set; }
    }
    public class YearViewModal
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public class ScheduleViewModel
    {
        public long ScheduleId { get; set; }
        public string DateName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }


  
  
}