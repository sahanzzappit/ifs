﻿using Commonn;
using Commonn.Enum;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using PostmarkDotNet;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication6.App_Start;
using WebApplication6.Commonn;
using WebApplication6.Content.Data_Tables;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class GradeController : Controller
    {
         private readonly DataContext _dbContext = null;
         private readonly ISmsService _smsService = null;
        private readonly IEmailService _emailService = null;
        private ApplicationUserManager _userManager;


        // DataContext _dbContext;
        public GradeController(ISmsService  smsService, ApplicationUserManager userManager, IEmailService emailService)
        {
            _dbContext = new DataContext();
            this._smsService = smsService;
            UserManager = userManager;
            this._emailService = emailService;
        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
 
            var gradeList = new List<string>();
            var gradeDistinct = from c in _dbContext.Grades orderby c.Name select c.Name ;
      

            gradeList.AddRange(gradeDistinct.Distinct());
            ViewBag.GradesToFilter = new SelectList(_dbContext.Grades.ToList(), "Id", "Name");
            ViewBag.grade = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");
            ViewBag.classDate = new SelectList(_dbContext.DateNames.ToList(), "Name", "Name");
            var name = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");
            ViewBag.IsLecturer = UserManager.GetRoles(Session["Id"].ToString()).FirstOrDefault() == "Lecturer" ? true : false;
            ViewBag.className = name;

            return View();
        }

        public ActionResult AddClass()
        {
            var name = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");

            ViewBag.className = name;
            return View();
        }

        [HttpGet]
        public ActionResult GetList(jQueryDataTableParamModel param, string grade, string scheduleId)
        {
            
            var classSchedules = _dbContext.ClassSchedules.Include(x => x.Grade).ToList();


            if (grade != null && grade != string.Empty)
            {
                classSchedules = classSchedules.Where(x => x.GradeId == int.Parse(grade)).ToList();
            }

            if (scheduleId != null && scheduleId != string.Empty)
            {
                classSchedules = classSchedules.Where(x => x.Id == int.Parse(scheduleId)).ToList();
            }

                List<GradeViewModal> returnList = new List<GradeViewModal>();

                foreach (var item in classSchedules)
                {
                    GradeViewModal data = new GradeViewModal
                    {
                        Id = item.Id,
                        GradeName = item.Grade.Name,
                        ClassDate = _dbContext.DateNames.Where(x => x.Id == item.DateNameId).Select(x => x.Name).FirstOrDefault(),
                        StartTime = item.SartTime.ToString(),
                        EndTime = item.EndTime.ToString(),
                        Fees = item.Grade.Fee,
                        GradeId = item.Grade.Id,
                        NoOfStudents = _dbContext.Students.Where(x => x.ClassScheduleId == item.Id).ToList().Count(),

           
                    };

                    returnList.Add(data);
                }


                return Json(new { data = returnList }, JsonRequestBehavior.AllowGet);
            
           
        }

        [HttpPost]
        public ActionResult SaveData(List<ClassSchedule> classSchedule, long GradeName)
        {
            try
            {               

                foreach (var item in classSchedule)
                {
                    var checkLists = _dbContext.ClassSchedules.Where(x => x.DateNameId == item.DateNameId).ToList();

                    bool result = _dbContext.ClassSchedules.Where(s => s.DateNameId == item.DateNameId)
                                    .Any(s => (s.SartTime >= item.SartTime && s.SartTime <= item.EndTime) ||
                                    (s.EndTime <= item.EndTime && s.EndTime >= item.SartTime) || (item.SartTime >= item.EndTime));

                    if (result)
                    {
                        return Json("Invalid");
                    }
                }

                    foreach (var item in classSchedule)
                    {
                        
                         item.GradeId = GradeName;

                        _dbContext.ClassSchedules.Add(item);

                    }

                   _dbContext.SaveChanges();

                    return Json("done");
                

            }
            catch (Exception ex)
            {
                return Json("error");
              
            }
       
        }


        
        [HttpPost]
        public bool UpdateClass(long Id,TimeSpan StartTime, TimeSpan EndTime, string ClassDateId, decimal Fees, long GradeId)
        {

            try
            {

                var date = _dbContext.DateNames.Where(x => x.Name == ClassDateId).Select(x => x.Id).FirstOrDefault();


                bool result = _dbContext.ClassSchedules.Where(s => s.DateNameId == date && s.Id != Id)
                                .Any(s => (s.SartTime >= StartTime && s.SartTime <= EndTime) ||
                             (s.EndTime <= EndTime && s.EndTime >= StartTime) || (StartTime >= EndTime));

                if (result)
                {
                    return false;
                }

                    var dateId = _dbContext.DateNames.Where(x => x.Name == ClassDateId).Select(x => x.Id).FirstOrDefault();

                    ClassSchedule classSchedule = _dbContext.ClassSchedules.Find(Id);


                    classSchedule.SartTime = StartTime;
                    classSchedule.EndTime = EndTime;
                    classSchedule.DateNameId = dateId;

                    var grade = _dbContext.Grades.Find(GradeId);
                    grade.Fee = Fees;

                    _dbContext.SaveChanges();



                    return true;
                
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        
        [HttpGet]
        public JsonResult GetSchedule(long Id)
        {
            try
            {
                var classSchedules = _dbContext.ClassSchedules.Where(x => x.Id == Id).Include(x => x.Grade).ToList();

                List<GradeViewModal> returnList = new List<GradeViewModal>();

                foreach (var classSchedule in classSchedules)
                {
                    var data = new GradeViewModal
                    {
                        Id = classSchedule.Id,
                        StartTime = classSchedule.SartTime.ToString(),
                        EndTime = classSchedule.EndTime.ToString(),
                        GradeName = classSchedule.Grade.Name,
                        Fees = classSchedule.Grade.Fee,
                        ClassDateId = classSchedule.DateNameId,
                        ClassDate = _dbContext.DateNames.Where(x => x.Id == classSchedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                        GradeId = classSchedule.Grade.Id,
                        HdnGrdId = classSchedule.GradeId

                    };
                    returnList.Add(data);
                }
                return Json(new { returnList }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception)
            {
                return Json("error");
            }

        }


        [HttpGet]
        public JsonResult GetDropDownVal(long grdVal)
        {
            try
            {
                var schedules = _dbContext.ClassSchedules.Where(x => x.GradeId == grdVal).ToList();


                List<ScheduleViewModel> scheduleViewModels = new List<ScheduleViewModel>();

                foreach (var schedule in schedules)
                {

                        ScheduleViewModel data = new ScheduleViewModel
                        {
                            ScheduleId = schedule.Id,
                            DateName = _dbContext.DateNames.Where(x => x.Id == schedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                            StartTime = schedule.SartTime.ToString(),
                            EndTime = schedule.EndTime.ToString()
                        };

        
                    scheduleViewModels.Add(data);
                }

                return Json(new { scheduleViewModels }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteGrade(long Id)
        {

            var session = Session["UserName"];
            var roles = UserManager.GetRoles((string)Session["Id"]);

            if (!roles.Contains(UserRolesEnum.Lecturer.ToString()))
            {
                return Json(data: "NotAuthorized", behavior: JsonRequestBehavior.AllowGet);
            }

            if (session == null)
            {
                return Json(data: "Loggedout", behavior: JsonRequestBehavior.AllowGet);
            }

            if (_dbContext.Students.Any(x => x.ClassScheduleId == Id))
            {
                return Json("error");
            }

            var classSchedule = _dbContext.ClassSchedules.Find(Id);
            _dbContext.ClassSchedules.Remove(classSchedule);
            _dbContext.SaveChanges();

            return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SendMessage(string message, List<long> selectedItems)
        {
            try
            {
                var studentsForClasses = _dbContext.Students.Where(x => selectedItems.Contains(x.ClassScheduleId)).Select(x => x.MobileNo).ToList();
                var send = _smsService.SendBulkSms(studentsForClasses, message);

                return Json(send);

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public ActionResult ViewMails()
        {
           var mails =  this._emailService.GetEmails();
            return Json(mails);
        }




        public class GradeViewModal
        {
        public long GradeId { get; set; }
        public long Id { get; set; }
        public string GradeName { get; set; }
        public string ClassDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public decimal Fees { get; set; }
        public long ClassDateId { get; set; }
        public long HdnGrdId { get; set; }
        public long NoOfStudents { get; set; }
        

        }
        public class GradeModel
        {
            public long Id { get; set; }
            public string GradeName { get; set; }
            public decimal Fees { get; set; }
        }
        public class ScheduleViewModel
        {
            public long ScheduleId { get; set; }
            public string DateName { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
        }
    }
}
