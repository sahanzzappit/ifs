﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class GradeListController : Controller
    {

        private readonly DataContext _dbContext = null;



        // DataContext _dbContext;
        public GradeListController()
        {
            _dbContext = new DataContext();
        }
        // GET: GradeList
        public ActionResult Index()
        {       
            return View();
        }
        [HttpGet]
        public ActionResult GetGrdList()
        {
            var grades = _dbContext.Grades.ToList();
            var students = this._dbContext.Students.ToList().Count();

            List<GradeModel> gradeModels = new List<GradeModel>();

            foreach (var grade in grades)
            {
                GradeModel gradeModel = new GradeModel
                {
                    Id = grade.Id,
                    GradeName = grade.Name,
                    Fees = grade.Fee,
                    Description = grade.Description,
                    IsActive = grade.IsActive,
                };
                gradeModels.Add(gradeModel);
            }

            return Json(new { data = gradeModels, TotalActive = gradeModels.Count(x => x.IsActive), TotalInactiveCount = gradeModels.Count(x => !x.IsActive), TotStudents = students }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveGrade(string GradeName, decimal Fees, string Description)
        {
            try
            {
                if (_dbContext.Grades.Any(x => x.Name == GradeName))
                {
                    return Json("error");

                }

                else
                {
                    Grade grade = new Grade
                    {
                        Name = GradeName,
                        Fee = Fees,
                        CreatedDate = DateTime.Now,
                        Description = Description
                    };
                    _dbContext.Grades.Add(grade);

                    _dbContext.SaveChanges();
                    return Json("success");
                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpGet]
        public JsonResult GetGrade(long Id)
        {
            try
            {
                var grade = _dbContext.Grades.Where(x => x.Id == Id).ToList();

                List<GradeModel> gradeModels = new List<GradeModel>();

                foreach (var item in grade)
                {
                    var data = new GradeModel
                    {
                        Id = item.Id,
                        GradeName = item.Name,
                        Fees = item.Fee,
                        Description = item.Description
                    };
                    gradeModels.Add(data);
                }

                return Json(new { gradeModels }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool EditGrade(long Id , string GrdName, decimal Fees, string Description)
        {
            try
            {
                    Grade grade = _dbContext.Grades.Find(Id);

                    grade.Name = GrdName;
                    grade.Fee = Fees;
                    grade.Description = Description;

                    _dbContext.SaveChanges();

                    return true;

            }
            catch (Exception ex)
            {

                return false;
            }
        }
        [HttpPost]
        public JsonResult DeleteGrade(long Id)
        {
            try
            {    
                if (_dbContext.ClassSchedules.Any(x => x.GradeId == Id))
                {

                    var name = _dbContext.Grades.Where(x => x.Id == Id).ToList();

                    List<GradeModel> gradeModels = new List<GradeModel>();

                    foreach (var item in name)
                    {
                        var data = new GradeModel
                        {
                            GradeName = item.Name,
                        };
                        gradeModels.Add(data);
                    }

                    return Json(new { gradeModels }, JsonRequestBehavior.AllowGet);
                }

                var grade = _dbContext.Grades.Find(Id);

                _dbContext.Grades.Remove(grade);

                _dbContext.SaveChanges();

                return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {

                return Json("error");
            }
        }

        public bool ChangeStatus(long id, bool status) 
        {
            try
            {
                var grade = _dbContext.Grades.Where(x => x.Id == id).FirstOrDefault();

                grade.IsActive = status;
                _dbContext.SaveChanges();

                return true;

            }
            catch (Exception ex)
            {
                return false;                
            }
        }


        public class GradeModel
        {
            public long Id { get; set; }
            public string GradeName { get; set; }
            public decimal Fees { get; set; }
            public string Description { get; set; }
            public bool IsActive { get; set; }
        }
    }
}