﻿using Commonn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn;
using WebApplication6.Commonn.Log;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class LendBooksController : Controller
    {
        private readonly DataContext _dbContext = null;
        private readonly ISmsService _smsService = null;

        public LendBooksController(ISmsService smsService) : base()
        {
            _dbContext = new DataContext();
            this._smsService = smsService;
        }
        // GET: LendBooks
        public ActionResult Index()
        {

            List<StuLendDetails> stuLendDetails = new List<StuLendDetails>();

            var list = _dbContext.Students.ToList();

            foreach (var item in list)
            {
                var std = _dbContext.StudentLibDetails.Where(x => x.StudentId == item.Id).Select(x => x.StudentId).FirstOrDefault();
                var stdDet = _dbContext.StudentLibDetails.Where(x => x.StudentId == std).Select(x => x.BookCategoryId).FirstOrDefault();

                StuLendDetails data = new StuLendDetails
                {
                    Id = item.Id,
                    StuName = _dbContext.Students.Where(x => x.Id == std).Select(x => x.Name).FirstOrDefault(),
                    CategoryId = _dbContext.StudentLibDetails.Where(x => x.StudentId == item.Id).Select(x => x.BookCategoryId).FirstOrDefault(),
                    BookName = _dbContext.Books.Where(x => x.BookCategory.Id == stdDet).Select(x => x.BookName).FirstOrDefault(),
                    
                };

                if (std != 0)
                {
                    stuLendDetails.Add(data);
                }

            }

            ViewBag.std = new SelectList(stuLendDetails.ToList(), "StuName", "StuName");
            ViewBag.categories = new SelectList(_dbContext.BookCategories.ToList(), "Id", "CategoryName");
            

            return View();
        }
        [HttpGet]
        public JsonResult GetList()
        {
            try
            {
                List<LendViewModal> lendViewModals = new List<LendViewModal>();

                var list = _dbContext.Books.ToList();

                foreach (var item in list)
                {
                    var stdIds = _dbContext.StudentLibDetails.Where(x => x.BookCategoryId == item.CategoryId).Select(x => x.StudentId).ToList();
                    var copies = _dbContext.BookCopies.Where(x => x.BookNo == item.BookNo).ToList();

                    var books = _dbContext.Books.Where(x => x.BookNo == item.BookNo).ToList();


                    string seperate = ",";

                    if (copies.Count == 0)
                    {
                        seperate = null;
                    }


                    LendViewModal data = new LendViewModal
                    {
                        Id = item.Id,
                        CategoryId = _dbContext.BookCategories.Where(x => x.Id == item.CategoryId).Select(x => x.Id).FirstOrDefault(),
                        BookName = item.BookName,
                        NoOfCopies = string.Join("", from book in books select book.BookNo) + seperate +  string.Join(",", from copy in copies select copy.CopyNo), //get multiple copies as a single row
                        BookNo = item.BookNo,
                        CategoryName = _dbContext.BookCategories.Where(x => x.Id == item.CategoryId).Select(x => x.CategoryName).FirstOrDefault()

                    };

                    foreach (var stdId in stdIds)
                    {
                        LendViewModal lendViewModal = new LendViewModal
                        {
                            StuName = _dbContext.Students.Where(x => x.Id == stdId).Select(x => x.Name).FirstOrDefault(),
                            StuId = _dbContext.Students.Where(x => x.Id == stdId).Select(x => x.Id).FirstOrDefault()
                        };

                        lendViewModals.Add(lendViewModal);
                    }

                    lendViewModals.Add(data);
                }

             

                

                return Json(new { lendViewModals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
        public JsonResult GetStuLendDetails()
        {
            try
            {
                List<StuLendDetails> stuLendDetails = new List<StuLendDetails>();

                var list = _dbContext.Students.ToList();

                foreach (var item in list)
                {
                    var std = _dbContext.StudentLibDetails.Where(x => x.StudentId == item.Id).Select(x => x.StudentId).FirstOrDefault();
                    var stdDet = _dbContext.StudentLibDetails.Where(x => x.StudentId == std).Select(x => x.BookCategoryId).FirstOrDefault();

                    StuLendDetails data = new StuLendDetails
                    {
                        Id = std,
                        StuName = _dbContext.Students.Where(x => x.Id == std).Select(x => x.Name).FirstOrDefault(),
                        CategoryId = _dbContext.StudentLibDetails.Where(x => x.StudentId == item.Id).Select(x => x.BookCategoryId).FirstOrDefault(),
                        BookName = _dbContext.Books.Where(x => x.BookCategory.Id == stdDet).Select(x => x.BookName).FirstOrDefault(),
                        LentDate = _dbContext.BookLends.Where(x => x.StudentId == item.Id).Select(x => x.LendDate).FirstOrDefault().ToString("yyyy-MM-dd"),

                    };

                    if (std != 0)
                    {
                        stuLendDetails.Add(data);
                    }

                }

                return Json(new { data = stuLendDetails }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        
        [HttpGet]
        public JsonResult GetData()
        {
            try
            {
                List<LendViewModal> categories = new List<LendViewModal>();

                var list = _dbContext.StudentLibDetails.ToList();

                foreach (var item in list)
                {
                    var bookList = _dbContext.Books.Where(x => x.CategoryId == item.BookCategoryId).ToList();

                    foreach (var item1 in bookList)
                    {

                        LendViewModal data = new LendViewModal
                        {
                            StuId = item.StudentId,
                            CategoryId = item1.CategoryId,
                            LentDate = _dbContext.BookLends.Where(x => x.StudentId == item.StudentId && x.BookId == item1.BookNo).Select(x => x.LendDate).FirstOrDefault().ToString("yyyy-MM-dd"),
                            BookNo = item1.BookNo,
                            ReturnedDate = _dbContext.BookReturns.Where(x => x.StudentId == item.StudentId && x.BookId == item1.BookNo).Select(x => x.ReturnedDate).FirstOrDefault().ToString("yyyy-MM-dd")
                        };
                        categories.Add(data);
                    }


                }

                return Json(new { categories }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public JsonResult GetCopyDetails(long BookId)
        {
            try
            {
                List<Copies> copiesModal = new List<Copies>();

                var list = _dbContext.BookCopies.Where(x => x.BookNo == BookId).Select(x => x.CopyNo).ToList();
                var list2 = _dbContext.BookLends.Where(x => x.BookId == BookId).Select(x => x.CopyId).ToList();
                var ckeckList = _dbContext.BookLends.Where(x => x.CopyId == BookId).Select(x => x.CopyId).ToList();
                var list3 = _dbContext.Books.Where(x => x.BookNo == BookId).Select(x => x.BookNo).ToList();

                var dif = list.Except(list2).ToList();
                var dif2 = list3.Except(ckeckList).ToList();


                 if (list.Count + list3.Count == 0)
                 {
                    string error2 = "hasNoCopies";

                    return Json(new { error2 }, JsonRequestBehavior.AllowGet);
                 }

                else
                {
                    foreach (var item in dif.Distinct())
                    {

                        Copies data = new Copies
                        {
                            CopyNo = item
                        };
                        copiesModal.Add(data);
                    }

                    foreach (var item in dif2.Distinct())
                    {
                        Copies data = new Copies
                        {
                            CopyNo = item
                        };
                        copiesModal.Add(data);
                    }

                    var copies = copiesModal.Select(x => x.CopyNo).Distinct();

                    return Json(new { copies }, JsonRequestBehavior.AllowGet);
                }

           
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        
        public ActionResult CheckLend(long BookId)
        {
            try
            {
                var list = _dbContext.BookCopies.Where(x => x.BookNo == BookId).ToList();
                string data1 = "error";
                string data2 = "done";

                if (list.Count <= 1)
                {
                    return Json(new { data1 }, JsonRequestBehavior.AllowGet);
                }
                else
                {               
                    return Json(new { data2 }, JsonRequestBehavior.AllowGet);
                }

                
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult SaveLend(long CopyId, long StudentId, long BookId)
        {
            try
            {
                var category = _dbContext.Books.Where(x => x.BookNo == BookId).Select(x => x.CategoryId).FirstOrDefault();
                var stuNo = _dbContext.Students.Where(x => x.StudentId == StudentId).Select(x => x.Id).FirstOrDefault();

                if (!_dbContext.StudentLibDetails.Where(x => x.StudentId == stuNo && x.BookCategoryId == category).Any())
                {
                    return Json("not valid student");
                }
                else
                {
                    var bookLend = new BookLend
                    {
                        BookId = BookId,
                        CopyId = CopyId,
                        LendDate = DateTime.Now,
                        StudentId = StudentId
                    };

                    var studentMobNo = _dbContext.Students.Where(x => x.StudentId == StudentId).Select(x => x.MobileNo).FirstOrDefault();

                    this._dbContext.BookLends.Add(bookLend);

                    var bookCopy = this._dbContext.BookCopies.Where(x => x.BookNo == BookId && x.CopyNo == CopyId).FirstOrDefault();
                    if (bookCopy != null)
                    {
                        bookCopy.IsAvailable = false;
                    }

                    this._dbContext.SaveChanges();

                    var send = this._smsService.SendSms(studentMobNo, "you have lent book no :-" + BookId + "Date :" + DateTime.Now + "-IFS-");

                    return Json("done");
                }

            }
            catch (Exception ex)
            {
                Log.Error("Error: unable to update lending for book no : " + BookId + "", ex);
                throw;
            }
        }
        [HttpPost]
        public ActionResult SaveLendWithOneCopy(DateTime LentDate, long StudentId, long BookId)
        {
            try
            {
                var list2 = _dbContext.BookLends.Where(x => x.BookId == BookId).ToList();

                if (list2.Any(x => x.CopyId == 0))
                {
                    return Json("noCopiesAvailable");
                }

                var bookLend = new BookLend
                {
                    BookId = BookId,
                    LendDate = LentDate,
                    StudentId = StudentId
                };

                this._dbContext.BookLends.Add(bookLend);

                var bookCopy = this._dbContext.BookCopies.Where(x => x.BookNo == BookId).FirstOrDefault();

                bookCopy.IsAvailable = false;
                this._dbContext.SaveChanges();

                return Json("done");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpGet]
        public JsonResult GetRetDetails(long BookId)
        {
            try
            {
                List<ReturnViewModal> returnViewModals = new List<ReturnViewModal>();

                //var returnList = _dbContext.BookLends.Where(x => x.BookId == BookId && x.StudentId == StudentId).ToList();
                var returnList = _dbContext.BookLends.Where(x => x.BookId == BookId).ToList();

                foreach (var item in returnList)
                {
                    var data = new ReturnViewModal
                    {
                         CopyNo = item.CopyId,
                         BookNo = item.BookId,
                         BookName = _dbContext.Books.Where(x => x.BookNo == item.BookId).Select(x => x.BookName).FirstOrDefault(),
                         StudentId = item.StudentId,
                    };

                    returnViewModals.Add(data);
                }

                return Json(new { returnViewModals},  JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult SaveReturn(long CopyNo, long BookNo, long StudentId)
        {
            try
            {
                var bookReturn = new BookReturn
                {
                     BookId = BookNo,
                     CopyId = CopyNo,
                     StudentId = StudentId,
                     ReturnedDate = DateTime.Now                         
                };

                this._dbContext.BookReturns.Add(bookReturn);

                var bookLend = _dbContext.BookLends.Where(x => x.CopyId == CopyNo).FirstOrDefault();
                this._dbContext.BookLends.Remove(bookLend);

                var bookCopy = this._dbContext.BookCopies.Where(x => x.BookNo == BookNo && x.CopyNo == CopyNo).FirstOrDefault();

                bookCopy.IsAvailable = true;
                this._dbContext.SaveChanges();

                return Json("done");
            }
            catch (Exception ex)
            {
                Log.Error("Error: unable to update return", ex);
                throw;
            }
        }
        [HttpGet]
        public ActionResult GetLibraryDet(long CategoryId)
        {
            try
            {
                var bookList = new List<BooksViewModel>();
                var books = _dbContext.Books.ToList();
                var categories = _dbContext.BookCategories.Where(x => x.Id == CategoryId).ToList();

                foreach (var category in categories)
                {
                    if (category.Books != null && category.Books.Any())
                    {
                        foreach (var book in category.Books)
                        {
                            var noOfCopies = _dbContext.BookCopies.Where(x => x.BookNo == book.BookNo).Count();

                            if (book != null)
                            {
                                if (noOfCopies != 0)
                                {
                                    bookList.Add(new BooksViewModel()
                                    {
                                        BookId = book.BookNo,
                                        BookName = book.BookName,
                                        CategoryId = book.CategoryId,
                                        CategoryName = category.CategoryName,
                                        IsLent = _dbContext.BookLends.Any(x => x.BookId == book.BookNo) ? true : false,
                                        IsAvailable = _dbContext.BookLends.Where(x => x.BookId == book.BookNo).Count() == noOfCopies ?
                                                        false : true,
                                        NoOfCopies = noOfCopies,

                                    });
                                }
                                else
                                {
                                    bookList.Add(new BooksViewModel()
                                    {
                                        BookId = book.BookNo,
                                        BookName = book.BookName,
                                        CategoryId = book.CategoryId,
                                        CategoryName = category.CategoryName,
                                        IsLent = _dbContext.BookLends.Any(x => x.BookId == book.BookNo) ? true : false,
                                        IsAvailable = _dbContext.BookLends.Where(x => x.BookId == book.BookNo).Count() == 1 ?
                                                        false : true,
                                        NoOfCopies = noOfCopies,

                                    });
                                }
                            }

                        }
                    }
                }

                return Json(new { BookList = bookList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        public class LendViewModal
        {
            public long Id { get; set; }
            public long CategoryId { get; set; }
            public string BookName { get; set; }
            public string StuName { get; set; }
            public long StuId { get; set; }
            public long StdId { get; set; }
            public long BookNo { get; set; }
            public string NoOfCopies { get; set; }
            public string LentDate { get; set; }
            public long BookId { get; set; }
            public string ReturnedDate { get; set; }
            public string CategoryName { get; set; }
        }
        public class StuLendDetails
        {
            public long Id { get; set; }
            public string StuName { get; set; }
            public long StuId { get; set; }
            public long CategoryId { get; set; }
            public string BookName { get; set; }
            public string LentDate { get; set; }
            public long BookNo { get; set; }
        }
        public class Copies
        {
            public long CopyNo { get; set; }
        }

        public class ReturnViewModal
        {
            public long CopyNo { get; set; }
            public long BookNo { get; set; }
            public long StudentId { get; set; }
            public string BookName { get; set; }
        }
        public class BooksViewModel
        {
            public long Id { get; set; }
            public long BookId { get; set; }
            public string BookName { get; set; }
            public long CategoryId { get; set; }
            public string CategoryName { get; set; }
            public bool IsLent { get; set; }
            public bool IsAvailable { get; set; }
            public long NoOfCopies { get; set; }
            public List<BookCopies>  BookCopies { get; set; }

            public BooksViewModel()
            {
                this.BookCopies = new List<BookCopies>();
            }
        }
    }
}