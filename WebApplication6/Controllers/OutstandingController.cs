﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Models;
using WebApplication6.Helpers;
using log4net;
using WebApplication6.Commonn.Log;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class OutstandingController : Controller
    {
        private readonly DataContext _dbContext = null;
        // GET: Outstanding
        public OutstandingController()
        {
            _dbContext = new DataContext();
        }
        public ActionResult Index()
        {

            ViewBag.months = new SelectList(_dbContext.months.ToList(), "Id", "Name");
            ViewBag.grades = new SelectList(_dbContext.Grades.ToList(), "Id", "Name");
            return View();
        }
        [HttpGet]
        public JsonResult GetList(string year, string Startmonth, string EndMonth, string grade, string SchId)
        {
            try
            {
                if (string.IsNullOrEmpty(EndMonth))
                {
                    EndMonth = (DateTime.Now.Month + 1).ToString();
                }
                var convertedYear = new DateTime(Convert.ToInt32(year), 1, 1);

                var list = _dbContext.Students.Include(x => x.ClassFee).ToList();
                var monthList = _dbContext.months.ToList();

                var unpaidDetail = list.Where(x => x.ClassFee.Count == 0).ToList();

                List<UnpaidViewModal> returnList = new List<UnpaidViewModal>();

                int from = int.Parse(Startmonth);
                int to = int.Parse(EndMonth);

                    var details = _dbContext.classFees.Where(x => x.Year.ToString() == year &&   (x.MonthId > from && x.MonthId < to || x.MonthId == from || x.MonthId == to )).Select(x => x.StudentId).Distinct().ToList();                   
           
                    var totalLists = _dbContext.Students.Select(x => x.Id).ToList();
                    var detailDis = details.Distinct().ToList();
                    var moLis = _dbContext.months.Where(x => x.Id > from && x.Id < to || x.Id == from || x.Id == to).ToList();

                    if (moLis.Count >= detailDis.Count)
                    {
                        foreach (var item in moLis)
                        {
                            foreach (var item1 in detailDis)
                            {
                                var paid = _dbContext.classFees.Where(x => x.StudentId == item1).Any(x => x.MonthId == item.Id && x.Year.ToString() == year);

                                if (paid == false)
                                {
                                    var stdId = details.Remove(item1);
                                }
                            }

                        }
                    }
                    else if (detailDis.Count > moLis.Count)
                    {
                        foreach (var item1 in detailDis)
                        {
                            foreach (var item in moLis)
                            {
                                var paid = _dbContext.classFees.Where(x => x.StudentId == item1).Any(x => x.MonthId == item.Id && x.Year.ToString() == year);

                                if (paid == false)
                                {
                                    var stdId = details.Remove(item1);
                                }
                            }
                        }
                    }

                    var difference = totalLists.Except(details).ToList();
                  

                    foreach (var item in difference)
                    {
                        var student = _dbContext.Students.Where(x => x.Id == item).Include(x => x.ClassSchedule.Grade).Include(x => x.ClassFee).FirstOrDefault();
                        var listIds = student.ClassFee.Where(x => x.Year.ToString() == year).Select(x => x.MonthId).ToList();
                        var monthIds = monthList.Where(x => x.Id > from && x.Id < to || x.Id == from || x.Id == to).Select(x => x.Id).ToList();
                        var unpaidMonths = monthIds.Except(listIds).ToList();

                        
                        foreach (var unpaidMonth in unpaidMonths)
                        {
                            var months = monthList.Where(x => x.Id == unpaidMonth).Select(x => x.Name).FirstOrDefault();
                            var monthId = monthList.Where(x => x.Id == unpaidMonth).Select(x => x.Id).FirstOrDefault();

                        if (grade != string.Empty && SchId == string.Empty)
                        {
                            
                            var student1 = _dbContext.Students.Where(x => x.Id == item && x.ClassSchedule.Grade.Id.ToString() == grade).Include(x => x.ClassFee).FirstOrDefault();

                            if (student1 != null)
                            {
                                returnList.Add(new UnpaidViewModal
                                {
                                    stuId = student1.Id,
                                    StuName = student1.Name,
                                    GrdName = student1.ClassSchedule.Grade.Name,
                                    //MonthId = _dbContext.classFees.Where(x => x.StudentId == item2.Id).Include(x => x.Month).Select(x => x.Month.Name).FirstOrDefault(),
                                    UnpaidMonth = _dbContext.months.Where(x => x.Id == monthId).Select(x => x.Name).FirstOrDefault(),
                                    PaymentAmount = student1.ClassSchedule.Grade.Fee,
                                    IsGracePeriodExceeded = student1.ClassFee.Where(x => x.Year == convertedYear.Year).Count() <= 9 ? true : false
                                });
                            }

                        }
                        else if (grade != string.Empty &&  SchId != string.Empty)
                        {
                            var student2 = _dbContext.Students.Where(x => x.Id == item && x.ClassSchedule.Grade.Id.ToString() == grade && x.ClassScheduleId.ToString() == SchId).Include(x => x.ClassFee).FirstOrDefault();

                            if (student2 != null)
                            {
                                returnList.Add(new UnpaidViewModal
                                {
                                    stuId = student2.Id,
                                    StuName = student2.Name,
                                    GrdName = student2.ClassSchedule.Grade.Name,
                                    //MonthId = _dbContext.classFees.Where(x => x.StudentId == item2.Id).Include(x => x.Month).Select(x => x.Month.Name).FirstOrDefault(),
                                    UnpaidMonth = _dbContext.months.Where(x => x.Id == monthId).Select(x => x.Name).FirstOrDefault(),
                                    PaymentAmount = student2.ClassSchedule.Grade.Fee,
                                    IsGracePeriodExceeded = student2.ClassFee.Where(x => x.Year == convertedYear.Year).Count() <= 9 ? true : false
                                });
                            }

                        }

                        else
                        {
                            returnList.Add(new UnpaidViewModal
                            {
                                stuId = student.Id,
                                StuName = student.Name,
                                GrdName = student.ClassSchedule.Grade.Name,
                                MonthId = unpaidMonth,
                                UnpaidMonth = months,
                                PaymentAmount = student.ClassSchedule.Grade.Fee,
                                IsGracePeriodExceeded = student.ClassFee.Where(x => x.Year == convertedYear.Year).Count() < 9 ? true : false
                            });
                        }

         
                        }
                    }

                Log.Info("done");
                return Json(new { data = returnList }, JsonRequestBehavior.AllowGet);
                

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult GetDropDownVal(long grdVal)
        {
            try
            {
                var schedules = _dbContext.ClassSchedules.Where(x => x.GradeId == grdVal).ToList();


                List<ScheduleViewModel> scheduleViewModels = new List<ScheduleViewModel>();

                foreach (var schedule in schedules)
                {
                    var students = _dbContext.Students.Where(x => x.ClassScheduleId == schedule.Id).ToList();

                    foreach (var student in students)
                    {
                        ScheduleViewModel data = new ScheduleViewModel
                        {
                            ScheduleId = schedule.Id,
                            DateName = _dbContext.DateNames.Where(x => x.Id == schedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                            StartTime = schedule.SartTime.ToString(),
                            EndTime = schedule.EndTime.ToString()
                        };

                        if (scheduleViewModels.Any(x => x.ScheduleId == schedule.Id))
                        {
                            scheduleViewModels.Remove(data);
                        }
                        else
                        {
                            scheduleViewModels.Add(data);
                        }


                    }

                }

                return Json(new { scheduleViewModels }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }


    public class UnpaidViewModal
    {
        public string StuName { get; set; }
        public string GrdName { get; set; }
        public long stuId { get; set; }
        public long MonthId { get; set; }
        public string UnpaidMonth { get; set; }
        public decimal PaymentAmount { get; set; }
        public bool IsGracePeriodExceeded { get; set; }
    }

}