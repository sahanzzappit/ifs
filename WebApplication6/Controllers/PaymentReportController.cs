﻿using Commonn;
using Commonn.Enum;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn;
using WebApplication6.Commonn.Log;
using WebApplication6.ExcelHelper;
using WebApplication6.Helpers;
using WebApplication6.Models;
using WebApplication6.Reports.Dataset;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class PaymentReportController : Controller
    {
        private readonly DataContext _dbContext = null;

        public PaymentReportController()
        {
            _dbContext = new DataContext();
        }
        // GET: PaymentReport
        public ActionResult Index()
        {
            

            ViewBag.grade = new SelectList(_dbContext.Grades.ToList(), "Id", "Name");

            return View();
        }
        [HttpGet]
        public JsonResult GetDetail(string grade, string scheduleId, string FromDate, string Todate)
        {
            var paidDetails = _dbContext.classFees.Include(x => x.Student.ClassSchedule.Grade).Include(x => x.Student.ClassFee)
                                    .Include(k => k.Month).ToList();

            DateTime fromDate = DateTime.Today.AddMonths(-1);
            DateTime toDate = DateTime.Today;
            var fileName = string.Empty;
            var filePath = string.Empty;
            var encryptedPath = string.Empty;

            fromDate = !string.IsNullOrEmpty(FromDate) ? fromDate = DateTime.Parse(FromDate) : fromDate;
            toDate = !string.IsNullOrEmpty(Todate) ? toDate = DateTime.Parse(Todate) : toDate;

            if (grade != null && grade != string.Empty)
            {
                paidDetails = paidDetails.Where(x => x.Student.ClassSchedule.GradeId == int.Parse(grade)).ToList();
            }

            if (scheduleId != null && scheduleId != string.Empty)
            {
                paidDetails = paidDetails.Where(x => x.Student.ClassScheduleId == int.Parse(scheduleId)).ToList();  
            }
            if (!string.IsNullOrEmpty(FromDate))
            {
                paidDetails = paidDetails.Where(x => x.PaidDate >= fromDate).ToList();
            }

            if (!string.IsNullOrEmpty(Todate))
            {
                paidDetails = paidDetails.Where(x => x.PaidDate <= toDate).ToList();
            }

            List<PaymentViewModal> paymentViewModals = new List<PaymentViewModal>();

            foreach (var paidDetail in paidDetails)
            {
                PaymentViewModal data = new PaymentViewModal
                {
                    StudentName = paidDetail.Student.Name,
                    PaidDate = paidDetail.PaidDate.ToString("yyyy/MM/dd"),
                    Grade = paidDetail.Student.ClassSchedule.Grade.Name,
                    PaymentAmount = paidDetail.Fee,
                    year = paidDetail.Year.ToString() + "-" + paidDetail.Month.Name,
                    //month = _dbContext.months.Where(x => x.Id == paidDetail.MonthId).Select(x => x.Name).FirstOrDefault(),
                    GradeId = paidDetail.Student.ClassSchedule.GradeId,
                    ScheduleId = _dbContext.Students.Where(x => x.Id == paidDetail.StudentId).Select(x => x.ClassScheduleId).FirstOrDefault(),
                    InvoiceDocument = !string.IsNullOrEmpty(paidDetail.Invoice) ? CryptographyHelper.ResolveDomainUrl(paidDetail.Invoice) : string.Empty,
                    CleanPaidDate = paidDetail.PaidDate,
                    PaidFor = paidDetail.Year + "-" + paidDetail.Month.Name
                };
                    
                paymentViewModals.Add(data);
            }
            return Json(new { data = paymentViewModals.OrderByDescending(x => x.CleanPaidDate) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDropDownVal(long grdVal)
        {
            try
            {
                var schedules = _dbContext.ClassSchedules.Where(x => x.GradeId == grdVal).ToList();


                List<ScheduleViewModel> scheduleViewModels = new List<ScheduleViewModel>();

                foreach (var schedule in schedules)
                {
                    var students = _dbContext.Students.Where(x => x.ClassScheduleId == schedule.Id).ToList();

                    foreach (var student in students)
                    {
                        ScheduleViewModel data = new ScheduleViewModel
                        {
                            ScheduleId = schedule.Id,
                            DateName = _dbContext.DateNames.Where(x => x.Id == schedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                            StartTime = schedule.SartTime.ToString(),
                            EndTime = schedule.EndTime.ToString()
                        };

                        if (scheduleViewModels.Any(x => x.ScheduleId == schedule.Id))
                        {
                            scheduleViewModels.Remove(data);
                        }
                        else
                        {
                            scheduleViewModels.Add(data);
                        }


                    }

                }

                return Json(new { scheduleViewModels }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult Generate(List<PaymentViewModal> tableList)
        {
            try
            {

                string cleanedFileName =  GenerateUserListExcel(tableList);

                var downloadUrl = Common.ResolveUrl("~/PaymentReport/DownloadFile?fileName=" + cleanedFileName, false);

                return Json(downloadUrl);
            }
            catch (Exception ex)
            {
                Log.Error("Error generating payment report:", ex);
                throw;
            }
      
        }

        public FileResult DownloadFile(string fileName)
        {
            try
            {
                string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();
                var folder = Common.ReadEnumDescription(DocumentPathEnum.PaymentsFilesPath);
                string filePath = rootPath + folder + fileName;


                byte[] fileBytes = Common.ReadFile(filePath);
                return File(fileBytes, "text/csv", fileName);
            }
            catch (Exception ex)
            {
             
                throw;
            }
        }


        [HttpPost]
        private string GenerateUserListExcel(List<PaymentViewModal> tableList)
        {
            string reportPath = @"\Reports\DataSet\" + "PaymentReport.rdlc";
            string downloadUrl = string.Empty;
            string fileName = string.Empty;


            PaymentReport dataSet = new PaymentReport();

            foreach (var item in tableList)
            {
                dataSet.DataTable1.AddDataTable1Row(
                      item.StudentName,
                      item.Grade,
                      item.PaidDate,
                      item.PaymentAmount.ToString(),
                      item.year

                    

                    );
            }

            return Common.SaveAsExcel(reportPath, "paymentlist", out fileName, dataSet, DocumentPathEnum.PaymentsFilesPath);
        }

    }

   


    public class PaymentViewModal
    {
        public string StudentName { get; set; }
        public string PaidDate { get; set; }
        public string Grade { get; set; }
        public decimal PaymentAmount { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public long GradeId { get; set; }
        public long ScheduleId { get; set; }
        public string InvoiceDocument { get; set; }
        public DateTime CleanPaidDate { get; set; }
        public string PaidFor { get; set; }
    }
  
}