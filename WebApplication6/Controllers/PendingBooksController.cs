﻿using Commonn;
using Commonn.Enum;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;
using WebApplication6.Models;
using WebApplication6.Reports.Dataset;
using System.Data.Entity;
using WebApplication6.Commonn;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class PendingBooksController : Controller
    {
        private readonly DataContext _dbContext = null;

        public PendingBooksController()
        {
            _dbContext = new DataContext();
        }
        // GET: PendingBooks
        public ActionResult Index()
        {


            ViewBag.bookNo = new SelectList(_dbContext.Books.ToList(), "BookNo", "BookNo");
            ViewBag.categories = new SelectList(_dbContext.BookCategories.ToList(), "CategoryName", "CategoryName");
            ViewBag.student = new SelectList(_dbContext.Students.ToList(), "Name", "Name");

            return View();
        }
        [HttpGet]
        public JsonResult GetList(long? bookNo , long? studentNo)
        {
            try
            {

                List<PendingBookDTO> returnViewModals = new List<PendingBookDTO>();
                var lentList = this._dbContext.BookLends.ToList();
                if (bookNo.HasValue)
                {
                    lentList = lentList.Where(x => x.CopyId == bookNo || x.BookId == bookNo).ToList();
                }
                if (studentNo.HasValue)
                {
                    lentList = lentList.Where(x => x.StudentId == studentNo).ToList();
                }

                foreach (var item in lentList)
                {
                    var returnedLists = _dbContext.BookReturns.Where(x => x.BookId == item.BookId && x.StudentId == item.StudentId).ToList();


                    if (returnedLists.Count  >= 0)
                    {
                        PendingBookDTO data = new PendingBookDTO
                        {
                            BookNo = item.CopyId != 0 ? item.CopyId.ToString() : item.BookId.ToString(),
                            //BookNo = _dbContext.BookReturns.Where(x => x.BookId == item.BookId).Select(x => x.CopyId).FirstOrDefault().ToString(),
                            BookName = _dbContext.Books.Where(x => x.BookNo == item.BookId).Select(x => x.BookName).FirstOrDefault(),
                            StudentName = _dbContext.Students.Where(x => x.StudentId == item.StudentId).Select(x => x.Name).FirstOrDefault(),
                            LentDate = item.LendDate.ToString("yyyy-MM-dd"),
                            CategoryName = _dbContext.Books.Where(x => x.BookNo == item.BookId).Select(x => x.BookCategory.CategoryName).FirstOrDefault()
                        };

                        returnViewModals.Add(data);
                    }
                   
                }

                return Json(new { data = returnViewModals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public JsonResult Generate(Filter filterPendingBookReport)
        {
            try
            {
                string cleanedFileName = GeneratePendingBooksListExcel(filterPendingBookReport);

                var downloadUrl = Common.ResolveUrl("~/PendingBooks/DownloadFile?fileName=" + cleanedFileName, false);

                return Json(downloadUrl);


            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public FileResult DownloadFile(string fileName)
        {
            try
            {
                string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();
                var folder = Common.ReadEnumDescription(DocumentPathEnum.PendingBooksPath);
                string filePath = rootPath + folder + fileName;


                byte[] fileBytes = Common.ReadFile(filePath);
                return File(fileBytes, "text/csv", fileName);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private string GeneratePendingBooksListExcel(Filter filterPendingBookReport)
        {
            var pendingBooks = this._dbContext.BookLends.ToList();
            if (filterPendingBookReport.BookNo.HasValue)
            {
                pendingBooks = pendingBooks.Where(x => x.BookId == filterPendingBookReport.BookNo).ToList();
            }
            if (filterPendingBookReport.StudentId.HasValue)
            {
                pendingBooks = pendingBooks.Where(x => x.StudentId == filterPendingBookReport.StudentId).ToList();
            }

            var bookIds = pendingBooks.Select(x => x.BookId).ToList();
            var bookDetails = this._dbContext.Books.Where(x => bookIds.Contains(x.BookNo)).Include(k => k.BookCategory).ToList();
            var studentIds = pendingBooks.Select(x => x.StudentId).ToList();
            var students = this._dbContext.Students.Where(x => studentIds.Contains(x.StudentId)).ToList();

            var filteredBooks = new List<PendingBookDTO>();

            foreach (var book in pendingBooks)
            {
                var bookDetail = bookDetails.Where(x => x.BookNo == book.BookId).FirstOrDefault();
                var studentDetail = students.Where(x => x.StudentId == book.StudentId).FirstOrDefault();

                filteredBooks.Add(new PendingBookDTO() {
                    BookNo = book.BookId.ToString(),
                    BookName = bookDetail.BookName,
                    CategoryName = bookDetail.BookCategory.CategoryName,
                    LentDate = book.LendDate.ToString(),
                    StudentName = studentDetail.Name,
                });
            }
            string reportPath = @"\Reports\" + "PendingBooks.rdlc";
            string downloadUrl = string.Empty;
            string fileName = string.Empty;


            PendingBooks dataSet = new PendingBooks();

            foreach (var item in filteredBooks)
            {
                dataSet.DataTable1.AddDataTable1Row(
                        item.BookNo,
                        item.BookName,
                        item.StudentName,
                        item.LentDate

                    );
            }

            return Common.SaveAsExcel(reportPath, "pendingBooks", out fileName, dataSet, DocumentPathEnum.PendingBooksPath);
             
        }

        public class PendingBookDTO
        {
            public string BookNo { get; set; }
            public string BookName { get; set; }
            public string StudentName { get; set; }
            public string LentDate { get; set; }
            public string CategoryName { get; set; }
        }

        public class Filter
        {
            public long? BookNo { get; set; }
            public long? StudentId { get; set; }
        }
    }
}