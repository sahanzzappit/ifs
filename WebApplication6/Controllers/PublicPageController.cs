﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class PublicPageController : Controller
    {
        private readonly DataContext _dbContext;
        // GET: PublicPage
        public PublicPageController()
        {
            this._dbContext = new DataContext();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> Index(Subscriber Subscriber, string Username, string Password)
        {
            byte[] encData_byte = new byte[Password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(Password);
            string encodedData = Convert.ToBase64String(encData_byte);

            var validate = _dbContext.Subscriber.Where(x => x.Name == Username && x.Password == encodedData).FirstOrDefault();

            if (validate != null)
            {
                FormsAuthentication.SetAuthCookie(Subscriber.Name, false);

                Session["UserName"] = validate.Name;
                Session["Id"] = validate.StudentId;
                TempData["session"] = validate.Id;


                return RedirectToAction("Index", "SubscriberPaymentDetail", new { area = "" });
            }
            else
            {
                TempData["Referrer"] = "Invalid";

                return View();
            }
        }

        // POST: LogOff
        public ActionResult LogOff()
        {
            var first = Session["UserName"];

            FormsAuthentication.SignOut();
            Session.Abandon();

            Session.RemoveAll();

            if (Session["UserName"] == null)
            {
                return Json("Logout");
            }

            else
            {
                return View();
            }

        }
        public ActionResult ContactUs()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetAllBlogs()
        {
            var blogs = this._dbContext.blogs.ToList();

            return Json(blogs, JsonRequestBehavior.AllowGet);
        }
    }
}