﻿using Commonn;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    [SessionCheck]
    public class StudentController : Controller
    {
        private readonly DataContext _dbContext = null;
        private readonly IEmailService _emailService = null;

        // DataContext _dbContext;
        public StudentController(IEmailService emailService)
        {
            _dbContext = new DataContext();
            this._emailService = emailService;
        }
        // GET: Student
        public ActionResult Index()
        {
            var nameList = new List<string>();
            var nameDistinct = from a in _dbContext.Students orderby a.Name select a.Name;

            nameList.AddRange(nameDistinct.Distinct());

            ViewBag.gradeName = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");
            ViewBag.stdId = new SelectList(nameList);

            return View();
        }
        public ActionResult AddStudent()
        {
            ViewBag.gradeName = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");

            return View();
        }

        [HttpGet]
        public JsonResult GetData(long GradeId)
        {
            try
            {
                var classSchedules = _dbContext.ClassSchedules.Where(x => x.GradeId == GradeId).Include(x => x.Grade).ToList();

                List<GradeViewModal> returnList = new List<GradeViewModal>();

                foreach (var classSchedule in classSchedules)
                {
                    var data = new GradeViewModal
                    {
                        Id = classSchedule.Id,
                        StartTime = classSchedule.SartTime.ToString(),
                        EndTime = classSchedule.EndTime.ToString(),
                        GradeName = classSchedule.Grade.Name,
                        ClassDateId = classSchedule.DateNameId,
                        ClassDate = _dbContext.DateNames.Where(x => x.Id == classSchedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                        GradeId = classSchedule.Grade.Id,

                    };
                    returnList.Add(data);
                }
                return Json(new { returnList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("error");
            }

        }
        [HttpGet]
        public ActionResult GetMaxValue()
        {
            try
            {
                var maxVal = _dbContext.Students.Max(x => x.StudentId);

                return Json(new { maxVal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpGet]
        public JsonResult GetEditData(long GradeId)
        {
            try
            {
                var classSchedules = _dbContext.ClassSchedules.Where(x => x.GradeId == GradeId).Include(x => x.Grade).ToList();

                List<GradeViewModal> returnList = new List<GradeViewModal>();

                foreach (var classSchedule in classSchedules)
                {
                    var data = new GradeViewModal
                    {
                        Id = classSchedule.Id,
                        StartTime = classSchedule.SartTime.ToString(),
                        EndTime = classSchedule.EndTime.ToString(),
                        GradeName = classSchedule.Grade.Name,
                        ClassDateId = classSchedule.DateNameId,
                        ClassDate = _dbContext.DateNames.Where(x => x.Id == classSchedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                        GradeId = classSchedule.Grade.Id,

                    };
                    returnList.Add(data);
                }
                return Json(new { returnList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("error");
            }

        }
        public ActionResult EditStudent(long Id)
        {
            ViewBag.gradeName = new SelectList(_dbContext.Grades.Where(x => x.IsActive).ToList(), "Id", "Name");
            ViewData["studentId"] = Id;

            return View();
        }

        [HttpPost]
        public bool SaveData(long Id, long StudentId, string Name, string MobileNo, string LandNo, string Email)
        {  

            try
            {
                if (_dbContext.Students.Any(x => x.StudentId == StudentId ))
                {
                    return false;
                }
                else
                {
              
                    Student student = new Student
                    {
                        ClassScheduleId = Id,
                        StudentId = StudentId,
                        Name = Name,
                        MobileNo = MobileNo,
                        LandNo = LandNo,
                        Email = Email
                    };

                    _dbContext.Students.Add(student);
                }
                _dbContext.SaveChanges();

                return true;

            }
            catch(Exception Ex)
            {
                return false;
            }
        }
        [HttpGet]
        public JsonResult GetList(string grade, string scheduleId)
        {
            var students = _dbContext.Students.Include(x => x.ClassSchedule.Grade).ToList();

            if (!(string.IsNullOrEmpty(grade)))
            {
                students = students.Where(x => x.ClassSchedule.GradeId == int.Parse(grade)).ToList();
            }
            if (scheduleId != null && scheduleId != string.Empty)
            {
                students = students.Where(x => x.ClassScheduleId == int.Parse(scheduleId)).ToList();
            }

            List<StudentViewModal> studentViewModals = new List<StudentViewModal>();

            foreach (var item in students)
            {
                StudentViewModal data = new StudentViewModal
                {
                    Id = item.Id,
                    StudentId = item.StudentId,
                    ClassScheduleId = item.ClassScheduleId,
                    Name = item.Name,
                    MobileNo = item.MobileNo,
                    LandNo = item.LandNo,
                    GradeName = item.ClassSchedule.Grade.Name,
                    ClassDate = _dbContext.DateNames.Where(x => x.Id == item.ClassSchedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                    GradeId = item.ClassSchedule.Grade.Id,
                    Email = item.Email
                };
                studentViewModals.Add(data);
            }
            return Json(new { data = studentViewModals }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDropDownVal(long grdVal)
        {
            try
            {
                var schedules = _dbContext.ClassSchedules.Where(x => x.GradeId == grdVal).ToList();


                List<ScheduleViewModel> scheduleViewModels = new List<ScheduleViewModel>();

                foreach (var schedule in schedules)
                {
                    var students = _dbContext.Students.Where(x => x.ClassScheduleId == schedule.Id).ToList();

                    foreach (var student in students)
                    {
                        ScheduleViewModel data = new ScheduleViewModel
                        {
                            ScheduleId = schedule.Id,
                            DateName = _dbContext.DateNames.Where(x => x.Id == schedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                            StartTime = schedule.SartTime.ToString(),
                            EndTime = schedule.EndTime.ToString()
                        };

                        if (scheduleViewModels.Any(x => x.ScheduleId == schedule.Id))
                        {
                            scheduleViewModels.Remove(data);
                        }
                        else
                        {
                            scheduleViewModels.Add(data);
                        }


                    }

                }

                return Json(new { scheduleViewModels }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult DeleteStudent(long Id)
        {   
            var student = _dbContext.Students.Find(Id);

            _dbContext.Students.Remove(student);

            var stuId = _dbContext.StudentLibDetails.Where(x => x.StudentId == Id).Select(x => x.Id).FirstOrDefault();

            StudentLibDetails libDetail =  _dbContext.StudentLibDetails.Find(stuId);

            if (libDetail != null)
            {
                _dbContext.StudentLibDetails.Remove(libDetail);
            }

            var retId = _dbContext.BookReturns.Where(x => x.StudentId == student.StudentId).Select(x => x.Id).ToList();
            foreach (var ret in retId)
            {
                BookReturn bookReturn = _dbContext.BookReturns.Find(ret);
                if (bookReturn != null)
                {
                    _dbContext.BookReturns.Remove(bookReturn);
                }

            }

            var lentdet = _dbContext.BookLends.Where(x => x.StudentId == student.StudentId).Select(x => x.Id).ToList();
            foreach (var lent in lentdet)
            {
                BookLend bookLend = _dbContext.BookLends.Find(lent);
                if (bookLend != null)
                {
                    _dbContext.BookLends.Remove(bookLend);
                }

            }

            var subscriber = _dbContext.Subscriber.Where(x => x.StudentId == student.StudentId).FirstOrDefault();
            if (subscriber != null)
            {
                _dbContext.Subscriber.Remove(subscriber);
            }


            _dbContext.SaveChanges();

            return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
            

          
        }

        [HttpGet]
        public JsonResult GetSchedule(long Id)
        {
            var students = _dbContext.Students.Where(x => x.Id == Id).Include(x => x.ClassSchedule).Include(k => k.ClassSchedule.Grade).ToList();
            

            List<StudentViewModal> studentViewModals = new List<StudentViewModal>();

            foreach (var item in students)
            {
                StudentViewModal data = new StudentViewModal
                {
                    Id = item.Id,
                    StudentId = item.StudentId,
                    ClassScheduleId = item.ClassScheduleId,
                    Name = item.Name,
                    MobileNo = item.MobileNo,
                    LandNo = item.LandNo,
                    GradeId = _dbContext.ClassSchedules.Where(x => x.GradeId == item.ClassSchedule.GradeId).Select(x => x.GradeId).FirstOrDefault(),
                    ClassDateId = _dbContext.DateNames.Where(x => x.Id == item.ClassSchedule.DateNameId).Select(x => x.Id).FirstOrDefault(),
                    ClassDate = _dbContext.DateNames.Where(x => x.Id == item.ClassSchedule.DateNameId).Select(x => x.Name).FirstOrDefault(),
                    StartTime = item.ClassSchedule.SartTime.ToString(),
                    EndTime = item.ClassSchedule.EndTime.ToString(),
                    Email = item.Email,
                    GradeName = item.ClassSchedule.Grade.Name,
                    GradeCurrentlyInactive = item.ClassSchedule.Grade.IsActive ? false : true
                    
                };

                studentViewModals.Add(data);
            }


            return Json(new { studentViewModals }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCategories(long Id)
        {
            try
            {
                List<StudentViewModal> studentViewModals = new List<StudentViewModal>();

                var categories = _dbContext.StudentLibDetails.Where(x => x.StudentId == Id).ToList();

                foreach (var item in categories)
                {
                    StudentViewModal studentViewModal = new StudentViewModal
                    {
                        Id = item.Id,
                        CategoryId = item.BookCategoryId,
                        CategoryName = _dbContext.BookCategories.Where(x => x.Id == item.BookCategoryId).Select(x => x.CategoryName).FirstOrDefault()
                    };
                    studentViewModals.Add(studentViewModal);
                }
                return Json(new { studentViewModals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }



        [HttpGet]
        public bool UpdateData(long ClassScheduleId, long Id , string LandNo, string MobileNo, string studentName, long studentId, string Email)
        {
            try
            {
                if (_dbContext.Students.Any(x => x.StudentId == studentId && x.Id != Id))
                {
                    return false;
                }
                Student student = _dbContext.Students.Find(Id);

                student.StudentId = studentId;
                student.Name = studentName;
                student.ClassScheduleId = ClassScheduleId;
                student.LandNo = LandNo;
                student.MobileNo = MobileNo;
                student.Email = Email;

                _dbContext.SaveChanges();

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
        [HttpGet]
        public JsonResult GetLibDetails()
        {
            try
            {
                List<LibraryViewModal> libraryViewModals = new List<LibraryViewModal>();

                var details = _dbContext.BookCategories.ToList();

                foreach (var detail in details)
                {
                    LibraryViewModal data = new LibraryViewModal
                    {
                        Id = detail.Id,
                        CategoryId = detail.Id,
                        CategoryName = detail.CategoryName
                    };

                    libraryViewModals.Add(data);
                }

                return Json(new { libraryViewModals }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult SaveLibDetails(List<BookCategory> categoryLists, long stdId)
        {
            try
            {
                var id = _dbContext.Students.Where(x => x.StudentId == stdId).Select(x => x.Id).FirstOrDefault();
     
                    foreach (var categoryList in categoryLists)
                    {
                        StudentLibDetails studentLibDetails = new StudentLibDetails
                        {
                             StudentId = id,
                             BookCategoryId = categoryList.Id,
                             CategoryName = _dbContext.BookCategories.Where(x => x.Id == categoryList.Id).Select(x => x.CategoryName).FirstOrDefault()

                        };
                         _dbContext.StudentLibDetails.Add(studentLibDetails);
                    }
                    

                   
                
                _dbContext.SaveChanges();

                return Json("");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult UpdateLibDetails(List<BookCategory> categoryLists, long id)
        {
            try
            {
                var list = _dbContext.StudentLibDetails.Where(x => x.StudentId == id).Select(x => x.Id).ToList();

                foreach (var item in list)
                {
                    
                    var studentLibDetails = _dbContext.StudentLibDetails.Find(item);

                    _dbContext.StudentLibDetails.Remove(studentLibDetails);

                   
                }

                var list2 = _dbContext.BookLends.Where(x => x.StudentId == id).Select(x => x.Id).ToList();

                foreach (var item in list2)
                {
                    var lendDet = _dbContext.BookLends.Find(item);
                    _dbContext.BookLends.Remove(lendDet);
                }
                

                var stdId = _dbContext.Students.Where(x => x.Id == id).Select(x => x.Id).FirstOrDefault();



                if (categoryLists != null)
                {
                    foreach (var categoryList in categoryLists)
                    {
                        StudentLibDetails studentLibDetails = new StudentLibDetails
                        {
                            StudentId = stdId,
                            BookCategoryId = categoryList.Id,
                            CategoryName = _dbContext.BookCategories.Where(x => x.Id == categoryList.Id).Select(x => x.CategoryName).FirstOrDefault()

                        };
                        _dbContext.StudentLibDetails.Add(studentLibDetails);
                    }
                }
                _dbContext.SaveChanges();

                return Json("");
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult SendEmail(string Subject, string Content, List<long> Selected)
        {
            try
            {
                var studentEmails = _dbContext.Students.Where(x => Selected.Contains(x.Id) && !string.IsNullOrEmpty(x.Email)).Select(i => i.Email).ToList();
                var result = this._emailService.SendBulkEmail(Subject, Content, studentEmails);

                return Json("done");
            }
            catch (Exception ex)
            {

                throw;
            }
        }


    }
 







    public class GradeViewModal
    {
        public long GradeId { get; set; }
        public long Id { get; set; }
        public string GradeName { get; set; }
        public string ClassDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public long ClassDateId { get; set; }

    }

    public class StudentViewModal
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long ClassScheduleId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string LandNo { get; set; }
        public string Email { get; set; }
        public long GradeId { get; set; }
        public long ClassDateId { get; set; }
        public string ClassDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string GradeName { get; set; }
        public  string CategoryName { get; set; }
        public  long CategoryId { get; set; }
        public bool GradeCurrentlyInactive { get; set; }

    }
    public class LibraryViewModal
    {
        public long Id { get; set; }
        public long CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}