﻿using Commonn;
using Commonn.DTOs;
using Commonn.Enum;
using Genesis.QRCodeLib;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn;
using WebApplication6.Models;
using WebApplication6.Reports.Dataset;

namespace WebApplication6.Controllers
{
    public class TestController : Controller
    {

        private readonly DataContext _dbContext = null;
        private readonly IEmailService _emailService = null;
        private readonly ISmsService _smsService = null;

        public TestController(IEmailService emailService, ISmsService smsService) : base()
        {            
            _emailService = emailService;
            _smsService = smsService;
            _dbContext = new DataContext();
        }



        public ActionResult testEmail()
        {
             var result = _emailService.SendInvoiceEmail("", "", "");
            return Json("done", JsonRequestBehavior.AllowGet);
        }
        public ActionResult TestPdf()
        {
            InvoiceViewModal tableList = new InvoiceViewModal();
            tableList.StudentId = 1;
            tableList.MonthId = 2;
            tableList.MonthName = "Feb";
            tableList.Year = "2020";
            tableList.PaidAmount = "1000";
            //ar result = _pdfHelper.GeneratePDF(tableList);
            var result = GeneratePDF(tableList);

            return Json("done", JsonRequestBehavior.AllowGet);
        }

        public string GeneratePDF(InvoiceViewModal tableList)
        {
            string reportPath = @"\Reports\" + "InvoiceReport.rdlc";
            string downloadUrl = string.Empty;
            string fileName = string.Empty;


            InvoiceDataSet dataSet = new InvoiceDataSet();

            dataSet.DataTable1.AddDataTable1Row(
              tableList.StudentId.ToString(),
              tableList.Year,
              tableList.MonthName,
              tableList.MonthId.ToString(),
              tableList.PaidAmount,
              tableList.StudentName
            );


            return SaveAsExcel(reportPath, "invoice", out fileName, dataSet);
        }

        public static string SaveAsExcel(string reportPath, string fileNamePrefix, out string fileName, DataSet dataSet)
        {
            LocalReport lr = new LocalReport
            {
                ReportPath = ExcelGenerater.ApplicationPath + reportPath,
            };



            lr.DataSources.Add(new ReportDataSource("DataSet1", dataSet.Tables[0]));
            string mimeType, encoding, extension;



            Warning[] warnings;
            string[] streams;
            var renderedBytes = lr.Render
                (
                    "PDF",
                   null,
                    out mimeType,
                    out encoding,
                    out extension,
                    out streams,
                    out warnings
                );

            string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();

            fileName = String.Format($"{fileNamePrefix}_{DateTime.Now.ToString("yyyy_MM_dd_hh:mm:ss")}.pdf");
            string cleanedFileName = Common.CleanFileName(fileName);
            var folder = Common.ReadEnumDescription(DocumentPathEnum.PaymentInvoice);
            string outputPath = rootPath + folder + cleanedFileName;
            var saveAs = string.Format("{0}", outputPath);




            using (var stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write))
            {
                stream.Write(renderedBytes, 0, renderedBytes.Length);
                stream.Close();
            }

            return outputPath;

        }


    }
}