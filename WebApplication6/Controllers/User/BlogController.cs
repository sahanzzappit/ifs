﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn.Log;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers.User
{
    [SessionCheckSubscriber]
    public class BlogController : Controller
    {
        private readonly DataContext _dbContext;


        public BlogController()
        {
            this._dbContext = new DataContext();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveBlog(string heading, string content)
        {
            try
            {
                var blog = new Blog() { 
                    Heading = heading,
                    CreatedDate = DateTime.Now,
                    CreateBy = Session["UserName"].ToString(),
                    Content = content
                };

                this._dbContext.blogs.Add(blog);
                this._dbContext.SaveChanges();

                return Json(new { Response = true });

            }
            catch (Exception ex)
            {
                Log.Error($"Error save blog { ex }");
                throw;
            }
        }
        [HttpGet]
        public ActionResult GetAllBlogs()
        {
            var blogs = this._dbContext.blogs.ToList();

            return Json(blogs, JsonRequestBehavior.AllowGet);
        }
    }
}