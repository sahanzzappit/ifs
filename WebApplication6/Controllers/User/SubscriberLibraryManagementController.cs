﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Commonn.Log;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers.User
{
    [SessionCheckSubscriber]
    public class SubscriberLibraryManagementController : Controller
    {
        private readonly DataContext _dbContext;
        public SubscriberLibraryManagementController()
        {
            this._dbContext = new DataContext();
        }
        // GET: SubscriberLibraryManagement
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetStudentLibraryData()
        {
            var studentNo = Session["Id"];
            var studentId = this._dbContext.Students.Where(x => x.StudentId == (long)studentNo).Select(k => k.Id).FirstOrDefault();
            try
            {
                var studentBookCategories = this._dbContext.StudentLibDetails.Where(x => x.StudentId == studentId).Select(k => k.BookCategoryId).ToList();
                var bookList = this._dbContext.Books.Where(x => studentBookCategories.Contains(x.CategoryId)).ToList();
                var bookIds = bookList.Select(x => x.BookNo).ToList();
                var lendingDataList = this._dbContext.BookLends.Where(x => bookIds.Contains(x.BookId)).ToList();
                var bookCopyList = this._dbContext.BookCopies.Where(x => bookIds.Contains(x.BookNo)).ToList();

                var returnLibraryDataList = new List<StudentLibaryDTO>();

                foreach (var studentLibrarydata in bookList)
                {
                    var lendingData = lendingDataList.Where(x => x.BookId == studentLibrarydata.BookNo).ToList();

                    returnLibraryDataList.Add(new StudentLibaryDTO() { 
                        BookNo = studentLibrarydata.BookNo,
                        Name = studentLibrarydata.BookName,
                        Summary = studentLibrarydata.BookDetail != null ? studentLibrarydata.BookDetail : string.Empty,
                        LastLendDate = lendingData.Where(x => x.BookId == studentLibrarydata.BookNo).Any() ? lendingData.Where(k => k.BookId == studentLibrarydata.BookNo).Max(i => i.LendDate.ToShortDateString())
                                        : string.Empty,
                        IsAvailable = bookCopyList.Where(x => x.BookNo == studentLibrarydata.BookNo).Any(k => k.IsAvailable) ? true : false
                    });
                }

                return Json(new { data = returnLibraryDataList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Log.Error("Error: unable to get library details for student - " + (long)studentId + "", ex);
                throw;
            }
        }

        public class StudentLibaryDTO
        {
            public long BookNo { get; set; }
            public string Name { get; set; }
            public string LastLendDate { get; set; }
            public string Summary { get; set; }
            public bool IsAvailable { get; set; }
        }
    }
}