﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.Helpers;
using WebApplication6.Models;

namespace WebApplication6.Controllers.User
{
    [SessionCheckSubscriber]
    public class SubscriberPaymentDetailController : Controller
    {
        private readonly DataContext _dbContext;

        public SubscriberPaymentDetailController()
        {
            this._dbContext = new DataContext();

        }
        public ActionResult Index()
        {
            ViewBag.months = new SelectList(_dbContext.months.ToList(), "Id", "Name");
            return View();
        }

        [HttpGet]       
        public ActionResult GetSubscriberPaymentRec()
        {
            var subscriber = Session["Id"];

            var student = _dbContext.Students.Where(x => x.StudentId == (long)subscriber).Include(k => k.ClassSchedule).Include(k => k.ClassSchedule.Grade).FirstOrDefault();
            var payments = _dbContext.classFees.Where(x => x.StudentId == student.Id && x.Year == DateTime.Now.Year).Include(k => k.Month).ToList();
            var months = payments.Select(i => i.Month).ToList();
            var monthIds = months.Select(x => x.Id).ToList();
            var outstanding = _dbContext.months.Where(x => !monthIds.Contains(x.Id)).ToList();
            var fee = student.ClassSchedule.Grade.Fee;

            var paymentList = new List<SubscriberPaymentDetDTO>();

            foreach (var paymentRecord in payments)
            {
                paymentList.Add(new SubscriberPaymentDetDTO() { 
                    Month = months.Where(x => x.Id == paymentRecord.MonthId).Select(k => k.Name).FirstOrDefault(),
                    Amount = paymentRecord.Fee,
                    PaidDate  = paymentRecord.PaidDate.ToString()
                });
            }

            return Json(new {  Unpaid = outstanding, Payments = paymentList, StudentId = student.Id, PaymentDetailYear = DateTime.Now.Year, Fee = fee } , JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubPaymentDet(long Id)
        {
            try
            {
                var subscriber = Session["Id"];

                var student = _dbContext.Students.Where(x => x.StudentId == (long)subscriber).Select(i => i.Id).FirstOrDefault();
                var feeDetail = _dbContext.classFees.Where(x => x.StudentId == student && x.MonthId == Id && x.Year == DateTime.Now.Year).FirstOrDefault();

                var payemntData = new SubscriberPaymentDetDTO() { 
                    PaidDate = feeDetail.PaidDate.ToString(),
                    Amount  = feeDetail.Fee,
                };

                return Json(payemntData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public class SubscriberPaymentDetDTO
        {
            public string PaidDate { get; set; }
            public decimal Amount { get; set; }
            public string Month { get; set; }

        }

    }
}