﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication6.Models;

namespace WebApplication6.Controllers.User
{
    public class SubscriberRegisterController : Controller
    {
        private readonly DataContext _dbContext = null;

        public SubscriberRegisterController()
        {
            _dbContext = new DataContext();
        }
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }
        // POST: Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(Subscriber subscriber, string Password)
        {
            try
            {
                byte[] encData_byte = new byte[Password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(subscriber.Password);
                string encodedData = Convert.ToBase64String(encData_byte);

                bool isVaildId = _dbContext.Students.Any(x => x.StudentId == subscriber.StudentId && x.Email == subscriber.Email);
                bool existingSubscriber = _dbContext.Subscriber.Any(x => x.StudentId == subscriber.StudentId);

                if (!isVaildId)
                {
                    TempData["Referrer"] = "Invalid";

                    return View();
                }

                if (existingSubscriber)
                {
                    TempData["Referrer"] = "Exist";

                    return View();
                }

                else {

                    FormsAuthentication.SetAuthCookie(subscriber.Name, true);

                    Session["UserName"] = subscriber.Name;
                    Session["Id"] = subscriber.StudentId;
                    TempData["session"] = subscriber.Id;

                    Subscriber newSubscriber = new Subscriber();
                    
                    newSubscriber.StudentId = subscriber.StudentId;
                    newSubscriber.Name = subscriber.Name;
                    newSubscriber.Email = subscriber.Email;
                    newSubscriber.Password = encodedData;
                    _dbContext.Subscriber.Add(newSubscriber);

                    _dbContext.SaveChanges();

                    return RedirectToAction("Index", "SubscriberPaymentDetail", new { area = "" });
                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}