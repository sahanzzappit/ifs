﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication6.Models;

namespace WebApplication6.Controllers
{
    public class UserController : Controller
    {

        private readonly DataContext _dbContext = null;

        public UserController()
        {
            _dbContext = new DataContext();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
           
            return View();
        }

        // POST: Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(UserProfile userProfile, string UserName, string Password)
        {


            byte[] encData_byte = new byte[Password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(Password);
            string encodedData = Convert.ToBase64String(encData_byte);



            var result = _dbContext.userProfiles.Where(x => x.UserName == UserName && x.Password == encodedData).FirstOrDefault();

            if (result != null)
            {
                FormsAuthentication.SetAuthCookie(userProfile.UserName, false);

                Session["UserName"] = result.UserName;
                Session["Id"] = result.Id;
                TempData["session"] = result.Id;


                return RedirectToAction("Index", "GradeList", new { area = "" });
            }
            else
            {
                TempData["Referrer"] = "Invalid";

                return View();
            }
        }



        // POST: LogOff
        public ActionResult LogOff()
        {
            var first = Session["UserName"];

            FormsAuthentication.SignOut();
            Session.Abandon();

            Session.RemoveAll();

            var second = Session["UserName"];
            if (Session["UserName"] == null)
            {
                return Json("Logout");
            }

            else
            {
                return View();
            }

        }
    }
}