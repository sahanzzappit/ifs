﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;
using WebApplication6.Commonn;

namespace WebApplication6.Controllers.api
{
    public class DocumentController : ApiController
    {
        // GET: Document
        [Route("Document/{encryptedFilePath}/{filename}")]
        public HttpResponseMessage GetDocument(string encryptedFilePath, string fileName)
        {
            HttpResponseMessage result = null;
            var _key = ConfigurationManager.AppSettings["PublicKey"].ToString();
            var decryptedFilePath = string.Empty;
            var downloadedFileName = fileName;

            try
            {
                decryptedFilePath = CryptographyHelper.DecryptedStringUrlFriendly(encryptedFilePath, _key);



                if (!File.Exists(decryptedFilePath))
                {
                    result = Request.CreateResponse(HttpStatusCode.Gone);
                }

                else
                {
                    result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(new FileStream(decryptedFilePath, FileMode.Open, FileAccess.Read));
                    var ext = Path.GetExtension(decryptedFilePath);

                    if (string.IsNullOrWhiteSpace(downloadedFileName))
                    {
                        downloadedFileName = Path.GetFileName(decryptedFilePath);
                    }

                    if (ext == ".pdf")
                    {
                        result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("inline")
                        {
                            FileName = downloadedFileName,
                            Name = downloadedFileName
                        };
                        result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                    }
                }

            }
            catch (CryptographicException)
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            catch (Exception)
            {
                result = Request.CreateResponse(HttpStatusCode.BadGateway);
            }
            return result;
        }
    }
}