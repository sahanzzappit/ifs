﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplication6.Helpers.HtmlGenerator
{
    public class InvoiceHtml
    {
        public static string GenerateInvoiceHtml(DataSet dataSet)
        {
			try
			{
                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<html >");
                strHTMLBuilder.Append("<head>");
                strHTMLBuilder.Append("<h3>" + "Payment Invoice");
                strHTMLBuilder.Append("</h3>");
                strHTMLBuilder.Append("</head>");
                strHTMLBuilder.Append("<body>");
                strHTMLBuilder.Append("<p>" + "your payment has been updated");
                strHTMLBuilder.Append("</p>");
                strHTMLBuilder.Append("<table border='0.5px' cellpadding='1.5' cellspacing='1.5' bgcolor='lightyellow' style='font-family:Garamond; font-size:smaller'>");

                strHTMLBuilder.Append("<tr >");
                foreach (DataColumn myColumn in dataSet.Tables[0].Columns)
                {
                    strHTMLBuilder.Append("<td >");
                    strHTMLBuilder.Append(myColumn.ColumnName);
                    strHTMLBuilder.Append("</td>");

                }
                strHTMLBuilder.Append("</tr>");


                foreach (DataRow myRow in dataSet.Tables[0].Rows)
                {

                    strHTMLBuilder.Append("<tr >");
                    foreach (DataColumn myColumn in dataSet.Tables[0].Columns)
                    {
                        strHTMLBuilder.Append("<td >");
                        strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                        strHTMLBuilder.Append("</td>");

                    }
                    strHTMLBuilder.Append("</tr>");
                }

                //Close tags.  
                strHTMLBuilder.Append("</table>" + "<footer>" + "<p>" + "Ishani Fernando School" + "</p>" + "</footer>");
                strHTMLBuilder.Append("</body>");
                strHTMLBuilder.Append("</html>");

                return strHTMLBuilder.ToString();
            }
			catch (Exception ex)
			{

				throw;
			}
        }
    }
}