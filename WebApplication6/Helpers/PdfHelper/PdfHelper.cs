﻿using Commonn;
using Commonn.DTOs;
using Commonn.Enum;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using WebApplication6.Commonn;
using WebApplication6.Helpers.HtmlGenerator;
using WebApplication6.Reports.Dataset;

namespace WebApplication6.Helpers.PdfHelper
{
    public class PdfHelper
    {
        public static string GeneratePDF(InvoiceViewModal tableList)
        {
            string reportPath = @"\Reports\" + "InvoiceReport.rdlc";
            string downloadUrl = string.Empty;
            string fileName = string.Empty;

            InvoiceDataSet dataSet = new InvoiceDataSet();

            dataSet.DataTable1.AddDataTable1Row(
              tableList.StudentId.ToString(),
              tableList.Year,
              tableList.MonthName,
              tableList.MonthId.ToString(),
              tableList.PaidAmount,
              tableList.StudentName
            );


            return SaveAsPdf(reportPath, "invoice", out fileName, dataSet);
        }

        public static string SaveAsPdf(string reportPath, string fileNamePrefix, out string fileName, DataSet dataSet)
        {

            string rootPath = ConfigurationManager.AppSettings["RootApplicationDocumentPath"].ToString();

            fileName = String.Format($"{fileNamePrefix}_{DateTime.Now.ToString("yyyy_MM_dd_hh:mm:ss")}.pdf");
            string cleanedFileName = Common.CleanFileName(fileName);
            var folder = Common.ReadEnumDescription(DocumentPathEnum.PaymentInvoice);
            string outputPath = rootPath + folder + cleanedFileName;
            var saveAs = string.Format("{0}", outputPath);

            string Htmltext = InvoiceHtml.GenerateInvoiceHtml(dataSet);

            Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);

            using (var stream = new FileStream(saveAs, FileMode.Create, FileAccess.Write))
            {
                StringReader sr = new StringReader(Htmltext);
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                pdfDoc.Close();
            }


            return outputPath;

        }
    }
}