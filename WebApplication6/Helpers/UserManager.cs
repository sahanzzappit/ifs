﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication6.App_Start;

namespace WebApplication6.Helpers
{
    public class AppUserManager : Controller
    {
        public ApplicationUserManager AppUser
        {
            get
            {
                return Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
    }
}