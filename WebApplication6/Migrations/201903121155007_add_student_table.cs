namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_student_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StudentId = c.Long(nullable: false),
                        ClassScheduleId = c.Long(nullable: false),
                        Name = c.String(),
                        MobileNo = c.String(),
                        LandNo = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassSchedules", t => t.ClassScheduleId, cascadeDelete: true)
                .Index(t => t.ClassScheduleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "ClassScheduleId", "dbo.ClassSchedules");
            DropIndex("dbo.Students", new[] { "ClassScheduleId" });
            DropTable("dbo.Students");
        }
    }
}
