namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_class_fee_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClassFees",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StudentId = c.Long(nullable: false),
                        MonthId = c.Long(nullable: false),
                        PaidDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Months", t => t.MonthId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.StudentId)
                .Index(t => t.MonthId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ClassFees", "StudentId", "dbo.Students");
            DropForeignKey("dbo.ClassFees", "MonthId", "dbo.Months");
            DropIndex("dbo.ClassFees", new[] { "MonthId" });
            DropIndex("dbo.ClassFees", new[] { "StudentId" });
            DropTable("dbo.ClassFees");
        }
    }
}
