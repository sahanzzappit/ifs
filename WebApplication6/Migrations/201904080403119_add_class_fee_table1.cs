namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_class_fee_table1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClassFees", "Year", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClassFees", "Year");
        }
    }
}
