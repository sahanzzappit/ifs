namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_colum_to_grades : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Grades", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Grades", "Description");
        }
    }
}
