namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_bookCategory_tblAdd_books_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookCategories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryName = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookNo = c.Long(nullable: false),
                        BookName = c.String(),
                        BookCategory_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BookCategories", t => t.BookCategory_Id)
                .Index(t => t.BookCategory_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "BookCategory_Id", "dbo.BookCategories");
            DropIndex("dbo.Books", new[] { "BookCategory_Id" });
            DropTable("dbo.Books");
            DropTable("dbo.BookCategories");
        }
    }
}
