namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_to_books1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "CategoryId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "CategoryId");
        }
    }
}
