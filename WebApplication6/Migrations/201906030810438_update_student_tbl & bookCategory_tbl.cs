namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_student_tblbookCategory_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentBookCategories",
                c => new
                    {
                        Student_Id = c.Long(nullable: false),
                        BookCategory_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_Id, t.BookCategory_Id })
                .ForeignKey("dbo.Students", t => t.Student_Id, cascadeDelete: true)
                .ForeignKey("dbo.BookCategories", t => t.BookCategory_Id, cascadeDelete: true)
                .Index(t => t.Student_Id)
                .Index(t => t.BookCategory_Id);
            
            AddColumn("dbo.BookCategories", "StudentId", c => c.Long(nullable: false));
            AddColumn("dbo.Students", "BookCategoryId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentBookCategories", "BookCategory_Id", "dbo.BookCategories");
            DropForeignKey("dbo.StudentBookCategories", "Student_Id", "dbo.Students");
            DropIndex("dbo.StudentBookCategories", new[] { "BookCategory_Id" });
            DropIndex("dbo.StudentBookCategories", new[] { "Student_Id" });
            DropColumn("dbo.Students", "BookCategoryId");
            DropColumn("dbo.BookCategories", "StudentId");
            DropTable("dbo.StudentBookCategories");
        }
    }
}
