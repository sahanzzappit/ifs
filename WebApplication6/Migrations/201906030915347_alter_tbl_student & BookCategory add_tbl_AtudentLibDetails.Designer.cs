// <auto-generated />
namespace WebApplication6.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class alter_tbl_studentBookCategoryadd_tbl_AtudentLibDetails : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(alter_tbl_studentBookCategoryadd_tbl_AtudentLibDetails));
        
        string IMigrationMetadata.Id
        {
            get { return "201906030915347_alter_tbl_student & BookCategory add_tbl_AtudentLibDetails"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
