namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class alter_tbl_studentBookCategoryadd_tbl_AtudentLibDetails : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentBookCategories", "Student_Id", "dbo.Students");
            DropForeignKey("dbo.StudentBookCategories", "BookCategory_Id", "dbo.BookCategories");
            DropIndex("dbo.StudentBookCategories", new[] { "Student_Id" });
            DropIndex("dbo.StudentBookCategories", new[] { "BookCategory_Id" });
            CreateTable(
                "dbo.StudentLibDetails",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookCategoryId = c.Long(nullable: false),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.BookCategories", "StudentId");
            DropTable("dbo.StudentBookCategories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.StudentBookCategories",
                c => new
                    {
                        Student_Id = c.Long(nullable: false),
                        BookCategory_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Student_Id, t.BookCategory_Id });
            
            AddColumn("dbo.BookCategories", "StudentId", c => c.Long(nullable: false));
            DropTable("dbo.StudentLibDetails");
            CreateIndex("dbo.StudentBookCategories", "BookCategory_Id");
            CreateIndex("dbo.StudentBookCategories", "Student_Id");
            AddForeignKey("dbo.StudentBookCategories", "BookCategory_Id", "dbo.BookCategories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.StudentBookCategories", "Student_Id", "dbo.Students", "Id", cascadeDelete: true);
        }
    }
}
