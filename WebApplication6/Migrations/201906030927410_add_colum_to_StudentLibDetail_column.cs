namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_colum_to_StudentLibDetail_column : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentLibDetails", "StudentId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentLibDetails", "StudentId");
        }
    }
}
