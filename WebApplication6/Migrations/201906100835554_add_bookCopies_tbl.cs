namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_bookCopies_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookCopies",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookNo = c.Long(nullable: false),
                        CopyNo = c.Long(nullable: false),
                        Books_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Books", t => t.Books_Id)
                .Index(t => t.Books_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookCopies", "Books_Id", "dbo.Books");
            DropIndex("dbo.BookCopies", new[] { "Books_Id" });
            DropTable("dbo.BookCopies");
        }
    }
}
