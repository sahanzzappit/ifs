namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_BookLend_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookLends",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookId = c.Long(nullable: true),
                        CopyId = c.Long(nullable: true),
                        StudentId = c.Long(nullable: true),
                        LendDate = c.DateTime(nullable: true),
                        ReturnedDate = c.DateTime(nullable: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BookLends");
        }
    }
}
