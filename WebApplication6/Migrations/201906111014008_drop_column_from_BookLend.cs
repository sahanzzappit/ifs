namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class drop_column_from_BookLend : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.BookLends", "ReturnedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BookLends", "ReturnedDate", c => c.DateTime(nullable: false));
        }
    }
}
