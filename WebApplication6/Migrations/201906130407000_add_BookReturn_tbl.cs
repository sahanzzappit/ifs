namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_BookReturn_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookReturns",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookId = c.Long(nullable: false),
                        CopyId = c.Long(nullable: false),
                        StudentId = c.Long(nullable: false),
                        ReturnedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BookReturns");
        }
    }
}
