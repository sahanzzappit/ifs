namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_BookDetail_column_to_books_tbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "BookDetail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "BookDetail");
        }
    }
}
