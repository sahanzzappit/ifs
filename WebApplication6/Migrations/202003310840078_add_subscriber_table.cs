namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_subscriber_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Subscribers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StudentId = c.Long(nullable: false),
                        Name = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ClassFees", "Subscriber_Id", c => c.Long());
            AddColumn("dbo.StudentLibDetails", "Subscriber_Id", c => c.Long());
            CreateIndex("dbo.ClassFees", "Subscriber_Id");
            CreateIndex("dbo.StudentLibDetails", "Subscriber_Id");
            AddForeignKey("dbo.ClassFees", "Subscriber_Id", "dbo.Subscribers", "Id");
            AddForeignKey("dbo.StudentLibDetails", "Subscriber_Id", "dbo.Subscribers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentLibDetails", "Subscriber_Id", "dbo.Subscribers");
            DropForeignKey("dbo.ClassFees", "Subscriber_Id", "dbo.Subscribers");
            DropIndex("dbo.StudentLibDetails", new[] { "Subscriber_Id" });
            DropIndex("dbo.ClassFees", new[] { "Subscriber_Id" });
            DropColumn("dbo.StudentLibDetails", "Subscriber_Id");
            DropColumn("dbo.ClassFees", "Subscriber_Id");
            DropTable("dbo.Subscribers");
        }
    }
}
