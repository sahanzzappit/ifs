namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_fee_column_to_fee_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClassFees", "Fee", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Subscribers", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.UserProfiles", "Password", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserProfiles", "Password", c => c.String());
            AlterColumn("dbo.Subscribers", "Password", c => c.String());
            DropColumn("dbo.ClassFees", "Fee");
        }
    }
}
