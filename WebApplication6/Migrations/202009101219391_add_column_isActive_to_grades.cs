namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_isActive_to_grades : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Grades", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Grades", "IsActive");
        }
    }
}
