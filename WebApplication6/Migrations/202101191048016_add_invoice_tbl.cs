namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_invoice_tbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ClassFees", "IsInvoiceSent", c => c.Boolean(nullable: false));
            AddColumn("dbo.ClassFees", "Invoice", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ClassFees", "Invoice");
            DropColumn("dbo.ClassFees", "IsInvoiceSent");
            DropTable("dbo.Invoices");
        }
    }
}
