namespace WebApplication6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_type_to_invoice_tbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "InvoiceType", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "InvoiceType");
        }
    }
}
