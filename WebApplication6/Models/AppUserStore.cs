﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApplication6.Models;

namespace Virtuozzo.Web.Models
{
    public class AppUserStore<TUser> : UserStore<TUser, AppRole, string, IdentityUserLogin, IdentityUserRole, IdentityUserClaim>, IUserStore<TUser>
        where TUser : ApplicationUser
    {
        public string TenantId { get; set; }
        public AppUserStore(DbContext dbContext) : base(dbContext)
        {

        }

        public override Task CreateAsync(TUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }


            return base.CreateAsync(user);
        }

        public Task AddToRoleAsync(TUser user, string roleName, int tenantId)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Role name is empty");

            var role = this.Context.Set<AppRole>().Where(x => x.Name.ToUpper() == roleName.ToUpper() && x.TenantId == tenantId).FirstOrDefault();
            if (role == null)
            {
                throw new InvalidOperationException("Role is not found");
            }

            IdentityUserRole userRole = new IdentityUserRole();
            userRole.UserId = user.Id;
            userRole.RoleId = role.Id;

            user.Roles.Add(userRole);
            role.Users.Add(userRole);

            return (Task)Task.FromResult<int>(0);

        }

        internal Task RemoveFromRoleAsync(TUser user, string roleName, int tenantId)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentException("Role name is empty");

            var role = this.Context.Set<AppRole>().Where(x => x.Name.ToUpper() == roleName.ToUpper() && x.TenantId == tenantId).FirstOrDefault();
            if (role == null)
            {
                throw new InvalidOperationException("Role is not found");
            }

            IdentityUserRole userRole = new IdentityUserRole();
            userRole.UserId = user.Id;
            userRole.RoleId = role.Id;

            user.Roles.Remove(userRole);
            role.Users.Remove(userRole);

            return (Task)Task.FromResult<int>(0);
        }
    }

    public class AppRoleStore<TRole> : RoleStore<TRole>, IRoleStore<TRole> where TRole : AppRole, new()
    {
        public AppRoleStore(DbContext context) : base(context)
        {

        }

        public override Task CreateAsync(TRole role)
        {
            return base.CreateAsync(role);
        }

        public IQueryable<TRole> FindRolesByName(string roleName)
        {
            return this.Context.Set<TRole>();
        }


    }
}