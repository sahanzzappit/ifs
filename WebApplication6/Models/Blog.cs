﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Blog
    {
        public long Id { get; set; }
        public string Heading { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreateBy { get; set; }
        public string Content { get; set; }
    }
}