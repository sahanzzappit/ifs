﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Models
{
    public class BookCategory
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public DateTime CreatedDate { get; set; }        
        public ICollection<Books> Books { get; set; }
       
    }
}