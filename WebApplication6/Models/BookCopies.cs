﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class BookCopies
    {
        public long Id { get; set; }
        public long BookNo { get; set;}
        public long CopyNo { get; set; }
        public bool IsAvailable { get; set; }

        public virtual Books Books { get; set; }
    }
}