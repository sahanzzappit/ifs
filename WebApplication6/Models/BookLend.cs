﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class BookLend
    {
        public long Id { get; set; }
        public long BookId { get; set; }
        public long CopyId { get; set; }
        public long StudentId { get; set; }
        public DateTime LendDate { get; set; }
       
    }
}