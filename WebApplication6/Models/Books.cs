﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication6.Models

{
    public class Books
    {
        public long Id { get; set; }
        public long BookNo { get; set; }
        public string BookName { get; set; }
        public long CategoryId { get; set; }
        public long NoOfCopies { get; set; }
        public string BookDetail { get; set; }
        public virtual BookCategory BookCategory { get; set; }
        
        public virtual ICollection<BookCopies> BookCopies { get; set; }
    }
}