﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class ClassFee
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long MonthId { get; set; }
        public DateTime PaidDate { get; set; }
        public decimal Fee { get; set; }

        public int Year { get; set; }
        public bool IsInvoiceSent { get; set; }
        public string Invoice { get; set; }
        public virtual Student Student { get; set; }

        public virtual Month Month { get; set; }
        
        public Subscriber Subscriber { get; set; }
    }
}