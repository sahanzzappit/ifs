﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    [Serializable]
    public class ClassSchedule
    {
        
        public long Id { get; set; }
        public long GradeId { get; set; }
        public long DateNameId { get; set; }
        public TimeSpan  SartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public virtual Grade Grade { get; set; }

       

    }
}