﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data;
using WebApplication6.Controllers;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebApplication6.Models
{
    public class DataContext : DbContext
    {

        static DataContext()
        {
            Database.SetInitializer<DataContext>(null);

        }

        public DataContext() : base("project_IFSEntities1") {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Grade> Grades { get; set; }

        public DbSet<DateName> DateNames { get; set; }

        public DbSet<ClassSchedule> ClassSchedules { get; set; }

        public DbSet<Student> Students { get; set; }
        public DbSet<UserProfile> userProfiles { get; set; }
        public DbSet<Month> months { get; set; }
        public DbSet<ClassFee> classFees { get; set; }
        public DbSet<BookCategory> BookCategories { get; set; }
        public DbSet<Books> Books { get; set; }
        public DbSet<StudentLibDetails> StudentLibDetails { get; set; }
        public DbSet<BookCopies> BookCopies{ get; set; }
        public DbSet<BookLend> BookLends { get; set; }
        public DbSet<BookReturn> BookReturns { get; set; }
        public DbSet<Subscriber> Subscriber { get; set; }
        public DbSet<Invoice> Invoice { get; set; }
        public DbSet<Blog> blogs { get; set; }
  
    }
  
}