﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Month
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClassFee> ClassFee { get; set; }
    }
}