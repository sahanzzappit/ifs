﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Student
    {
        
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long ClassScheduleId { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string LandNo { get; set; }
        public long BookCategoryId { get; set; }
        public string Email { get; set; }
        public virtual ClassSchedule ClassSchedule { get; set; }

        public virtual ICollection<ClassFee> ClassFee { get; set; }
       

    }
    
}