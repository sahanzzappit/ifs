﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class StudentLibDetails
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public long BookCategoryId { get; set; }
        public string CategoryName { get; set; }

        public Subscriber Subscriber { get; set; }
    }
}