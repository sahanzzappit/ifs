﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication6.Models
{
    public class Subscriber
    {
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }


        public ICollection<ClassFee> ClassFees { get; set; }
        public ICollection<StudentLibDetails> StudentLibDetails { get; set; }
    }
}