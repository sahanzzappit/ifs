﻿$(document).ready(function () {
    
    GetDetail();


});

function GetDetail() {

    $.ajax({

        type: "GET",
        url: "/AdminDetail/GetDetail",
        contentType: "application/json",
        success: function (data) {

            var list = data;            
            console.log(list);

            $("#userName").val(data.userDetails.UserName);
            $("#hdnPassword").val(data.userDetails.CurrentPassword);
            $("#hdnId").val(data.userDetails.Id);
           
            
        }

    });

}


function changePassword() {

    var currentPassword = $("#currentPassword").val();

    var hdnPassword = $("#hdnPassword").val();
    var password = $("#newPassword").val();
    var id = $("#hdnId").val();

    var data = { Password: password, currentPassword: currentPassword, hdnPassword: hdnPassword };

    $("#errorMsg1").empty();
    $("#errorMsg2").empty();

   
    if ($("#newPassword").val() == "") {
        $("#errorMsg2").append("please enter new password");
    }


    else {
        $.ajax({

            type: "POST",
            url: "/AdminDetail/ChangePassword?id=" + id,
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (data) {
                console.log(data);
                if (data ==  "done") {
                    window.location.href = "/Grade/Index/"
                }
                else if (data == "Passwords must be at least 6 characters.") {
                    $("#errorMsg2").append("weak password");
                }
                else if (data == "Incorrect password.") {
                    $("#errorMsg1").append("passwords do not match");
                }
            }
        });
    }

 
}