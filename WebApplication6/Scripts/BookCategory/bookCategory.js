﻿$(document).ready(function () {
    BookCategoryGrid();
    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });
});

$(".categoryName").on("change paste keyup", function () {
    if ($(this).val() != "") {
        $(".categoryNameEmpty").addClass('hidden');
    }
    else {
        $(".categoryNameEmpty").removeClass('hidden');
    }
});


function BookCategoryGrid() {
    var table = $('#bookCategory').DataTable();
    table.destroy();
    var table = $('#bookCategory').dataTable({
        "responsive": true,
        "sAjaxSource": "/BookCategory/GetList",
        "bProcessing": true,
        "columnDefs": [
            { "width": "20px", "className": "dt-center" ,"targets": 1 },
            { "width": "20px", "className": "dt-center", "targets": 2 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 3 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 4 },

        ],
        "aoColumns": [
            { "data": "Name" },
            { "data": "NoOfBooks" },
            { "data": "WithCopies" },
            //{ "data": "Id" },
            {
                "render": function (data, type, full, meta) { return '<a class="btn btn-edit glyphicon glyphicon-edit" style="margin-left:30px;" onclick="GetCategory(' + full.Id + ' ) " "></a>'; }
            },
            {
                data: null, render: function (data, type, row) {
                    return "<a class=' glyphicon glyphicon-trash' style='margin-left:30px;color:red;margin-top:10px;' onclick='DeleteData(" + row.Id + ")'' ></a>";
                }
            },
        ]


    });
}
function openModal() {
    $('#addCategory').modal();
    $(".categoryNameEmpty").addClass('hidden');
}



function SaveAddCategory() {
    var name = $("#categoryName").val();

    if (name == "" ) {
        $(".categoryNameEmpty").removeClass('hidden');
    }

    else {
        var obj = {Name: name }

        $.ajax({

            type: "POST",
            url: "/BookCategory/AddCategory",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "success") {
                    $('#bookCategory').dataTable().fnDraw();
                    $("#categoryName").val("");
                    toastr.success('Saved category...');
                }
                else if (response == "false") {
                    toastr.error('book category already exist...');
                 }

                console.log(response);
            }

        });
    }
}

function SaveCategory() {
    var name = $("#categoryName").val();

    if (name == "") {

        $(".categoryNameEmpty").removeClass('hidden');
    }

    else {
        var obj = { Name: name }

        $.ajax({

            type: "POST",
            url: "/BookCategory/AddCategory",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "success") {
                    $('#bookCategory').dataTable().fnDraw();
                    location.reload();
                    toastr.success('Saved category...');
                }
                if (response == "false") {
                    toastr.error('book category already exist...');
                }

            }

        });
    }
}

function GetCategory(Id) {
    $("#editCategory").modal();
    $(".categoryNameEmpty").addClass('hidden');

    $.ajax({

        type: "GET",
        url: "/BookCategory/GetData?id= " + Id,
        contentType: "application/json",
        success: function (response) {

            var data = response.bookCategoryViewMadals;

            console.log(data);

            $("#hdnId").val(data[0].Id);
            $("#editCategoryName").val(data[0].Name);


        }

    });
}

function UpdateCategory() {
    var name = $("#editCategoryName").val();
    var id = $("#hdnId").val();


    if (name == "") {
        $(".categoryNameEmpty").removeClass('hidden');
    }

    else {
        var obj = {Id: id,  Name: name }

        $.ajax({

            type: "POST",
            url: "/BookCategory/EditCategory",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {

                    $('#bookCategory').dataTable().fnDraw();
                    location.reload();
                toastr.success('Updated category...');

            }

        });
    }
}

function DeleteData(Id) {

    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete book category?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    $.ajax({
                        type: "POST",
                        url: "/BookCategory/DeleteCategory?id= " + Id,
                        contentType: "application/json",
                        success: function (response) {

                            if (response == "error") {
                                toastr.error("can not delete category , books are created ");
                            }
                            else if (response == "Deleted") {
                                $('#bookCategory').dataTable().fnDraw();
                                location.reload();

                            }
                        }

                    });
                });
            }
        });


}