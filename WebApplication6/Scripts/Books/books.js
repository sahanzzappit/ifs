﻿$(document).ready(function () {
    BooksGrid();


    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $('input[type=checkbox]').click(function (e) {

         var table = $('#books').DataTable();

        var column = table.column(2);

        column.visible(!column.visible());
    });


    $('.copy').change(function () {

        var copyNo = $(this).val();
       
    });
    $(".bookNameCheck").on("change paste keyup", function () {
        if ($(this).val() != "") {
            $(".bookNameEmpty").addClass('hidden');
        }
        else {
            $(".bookNameEmpty").removeClass('hidden');
        }
    });
});


function BooksGrid() {
    var table = $('#books').DataTable();
    table.destroy();
    var table = $('#books').dataTable({
        "responsive": true,
        "sAjaxSource": "/Books/GetList",
        "bProcessing": true,
        "columnDefs": [
            { "width": "30px", "className": "dt-center", "targets": 0 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 3},
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 4 },
            { "width": "5px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 5 },
            { "width": "15px",  "targets": 2 },
            { "bVisible": false, "aTargets": [2] }
        ],
        "aoColumns": [
            { "data": "BookNo" },
            { "data": "BookName" },
            { "data": "NoOfCopies" },
            {
                "render": function (data, type, full, meta) { return '<a class="btn btn-edit glyphicon glyphicon-edit"  onclick="GetBook(' + full.Id + ' ) " "></a>'; }
            },
            {
                data: null, render: function (data, type, row) {
                    return "<a class=' glyphicon glyphicon-trash'   onclick='DeleteData(" + row.Id + ")'  style='color:red;margin:10px 0px 0px 10px;'></a>";
                }
            },
            {
                "render": function (data, type, full, meta) {
                    if (full.BookDetail == null || full.BookDetail == "") {
                        return '<a class="glyphicon glyphicon-qrcode"  onclick="scanBookQR(' + full.Id + ' ) " "></a>';
                    }
                    else {
                        return '<a class="glyphicon glyphicon-info-sign"  onclick="getBookDet(' + full.Id + ' ) " "></a>';
                    }
                }
            },
        ]


    });
}

function openModal() {
    $('#addBook').modal();
    $(".bookNameEmpty").addClass('hidden');
    $(".formError").addClass('hidden');

    $.ajax({
        type: "GET",
        url: "/Books/GetMaxValue",
        contentType: "application/json",
        success: function (response) {
            var suggest = response.maxVal1 + 1 || response.maxVal2 + 1 || response.val;
            var copy2 = response.maxVal1 + 2 || response.maxVal2 + 2 ||  response.val + 1;
            $("#bookNo").val(suggest);
            $("#noOfCopies").val(1);

            $("#noOfCopies").on("input", function () {
                $(".append").empty();
                var val = $(this).val() - 1;

                for (var i = 0; i < val; i++) {
                    var cpNo = copy2 + i;
                    var lbl = i + 2;

   
                    $(".append").append('<div class="row new" style="margin-top:30px;"><label class="control-label col-sm-3" for="name">Copy ' + lbl + ':</label> <div class="col-sm-3">' +
                        '<input type="number" value="' + cpNo +'" class="form-control copy" id="copy2" name="copy2" style="text-align:right;">' + '</div>' + '</div>');
                }

            });



        }
    });
}

var dataObj = [];

function SaveBook() {
    var no = $("#bookNo").val();
    var copies = $("#noOfCopies").val();
    var category = $("#categoryName").val();
    var name = $("#bookName").val();

    if (no == "" ||  copies == "" || category == "") {
        $(".formError").removeClass('hidden');
    }
    if (name == "" || name == null) {
        $(".bookNameEmpty").removeClass('hidden');
        return false;
    }
    if ($("#noOfCopies").val() == 0) {
        toastr.error('no of copies can not be 0...');
        return false;
    }
    if ($("#bookNo").val() == 0) {
        toastr.error('book no can not be 0...');
        return false;
    }

    dataObj = [];

    $('.copy').each(function () {
        var bkNo = $("#bookNo").val();
        var copyNo = $(this).val();
        dataObj.push({ CopyNo: copyNo, BookNo: bkNo});
    });

    var obj = { No: no, NoOfCopies: copies, CategoryId: category, BookName: name, bookCopies: dataObj}


        $.ajax({
            type: "POST",
            url: "/Books/SaveData",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "success") {
                    BooksGrid();
                    location.reload();
                    toastr.success('Saved book...');
                }
                else if (response == "error") {
                    toastr.error('book no already exist...');
                }
            }
        });
    


}

function SaveAddBook() {
    var no = $("#bookNo").val();
    var copies = $("#noOfCopies").val();
    var category = $("#categoryName").val();
    var name = $("#bookName").val();

    if (no == "" || copies == "" || category == "" || name == "") {
        $(".formError").removeClass('hidden');
    }

    if ($("#noOfCopies").val() == 0) {
        toastr.error('no of copies can not be 0...');
        return false;
    }

    var dataObj = []



    $('.copy').each(function () {
        var bkNo = $("#bookNo").val();
        var copyNo = $(this).val();
        dataObj.push({ CopyNo: copyNo, BookNo: bkNo });
    });

  
        var obj = { No: no, NoOfCopies: copies, CategoryId: category, BookName: name, bookCopies: dataObj }

        $.ajax({
            type: "POST",
            url: "/Books/SaveData",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "success") {
                    $('#books').dataTable().fnDraw();
                    $('input').val("");
                    $('#categoryName').val("");
                    $('.append').empty();
                    openModal();
                    toastr.success('Saved book...');
                    
                }
                else if (response == "error") {
                    toastr.error('book no already exist...');
                }
            }
        });
    
 
}

function GetBook(Id) {
    $("#editBook").modal();
    $(".bookNameEmpty").addClass('hidden');
    $(".formError").addClass('hidden');

    $.ajax({

        type: "GET",
        url: "/Books/GetData?id= " + Id,
        contentType: "application/json",
        success: function (response) {

            var data = response.booksViewModals;

            

            $("#editBookNo").val(data[0].BookNo);
            $("#editBookName").val(data[0].BookName);
            $("#hdnId").val(data[0].Id);
            $("#hdnNo").val(data[0].BookNo);
            $("#editNoOfCopies").val(data[0].NoOfCopies);      
            $('#editCategoryName').val(data[0].CategoryName);
        }

    });

    $(".appendEdit").empty();

    $.ajax({

        type: "GET",
        url: "/Books/GetCopies?id= " + Id,
        contentType: "application/json",
        success: function (response) {
            
            var data = response.copiesViewModals;
            

            for (var i = 0; i < data.length; i++) {
                var lbl = i + 1;
                var cpNo = data[i].CopyNo;

                $(".appendEdit").append('<div class="row new" style="margin-top:30px;"><label class="control-label col-sm-3" for="name">Copy ' + lbl + ':</label> <div class="col-sm-3">' +
                    '<input type="number" value="' + cpNo + '" class="form-control copy" id="copy2" name="copy2" style="text-align:right;" >' + '</div>' + '</div>');

                
            }   

                            
        }

        
    });

    $('#editNoOfCopies').on("input", function () {
        $(".appendEdit").empty();
        var val = $(this).val();

        for (var i = 1; i < val; i++) {
            var lbl = i + 1;


            $(".appendEdit").append('<div class="row new" style="margin-top:30px;"><label class="control-label col-sm-3" for="name">Copy ' + lbl + ':</label> <div class="col-sm-3">' +
                '<input type="number" value="" class="form-control editCopy" id="copy2" name="copy2" style="text-align:right;">' + '</div>' + '</div>');


        }


        var id = $("#hdnNo").val()

        Clear(id);

    });


 
}

function Clear(id) {
    

    console.log(id);

    $.ajax({

        type: "POST",
        url: "/Books/Changed?id=" + id,
        contentType: "application/json",
        success: function (response) {

        }
    });
}


function UpdateBook() {
    var id = $("#hdnId").val();
    var iniNo = $("#hdnNo").val();
    var no = $("#editBookNo").val();
    var name = $("#editBookName").val();
    var copy = $("#editNoOfCopies").val();
    var category = $("#editCategoryName").val();


     var dataObj = [];
     var dataObj2 = [];
    
        $('.editCopy').each(function () {
            var bkNo = $("#hdnNo").val();
            var copyNo = $(this).val();
            dataObj.push({ CopyNo: copyNo, BookNo: bkNo });
            
        });
    
 

   
    $('.copy').each(function () {
        var bkNo = $("#hdnNo").val();
        var copyNo = $(this).val();
        dataObj2.push({ CopyNo: copyNo, BookNo: bkNo });
    });


    if (copy == 1) {
        var withOneCopy = "1";
    }
    if ($("#editNoOfCopies").val() == 0) {
        toastr.error('no of copies can not be 0...');
        return false;
    }

    if (no == "" || copy == "" || category == "") {
        $(".formError").removeClass('hidden');
    }
    if (name == "" || name == null) {
        $(".bookNameEmpty").removeClass('hidden');
        return false;
    }


    else {
        var obj = { Id: id, No: no, Name: name, Category: category, NoOfCopies: copy, IniNo: iniNo, bookCo: dataObj, WithOneCopy: withOneCopy, bookCo2: dataObj2}

        $.ajax({

            type: "POST",
            url: "/Books/EditBook",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {

                if (response == "success") {                   
                    BooksGrid();
                    location.reload();
                    toastr.success('book updated...');
                }
                else if (response == "error") {
                    toastr.error('book no already exist...');
                }
                else if (response == "error1") {
                    toastr.error('book no already exist...');
                }
            }

        });
    }
}

function DeleteData(Id) {
    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete book?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    $.ajax({
                        type: "POST",
                        url: "/Books/DeleteBook?id= " + Id,
                        contentType: "application/json",
                        success: function (response) {
                            if (response == "Deleted") {
                                BooksGrid();
                                location.reload();
                                toastr.success('Book deleted...');
                            }


                        }

                    });
                });
            }
        });

}

function test() {
    $.ajax({
        type: "GET",
        url: "/Books/GetMaxValue",
        contentType: "application/json",
        success: function (response) {
            var suggest = response.maxVal1 + 1 || response.maxVal2 + 1 || response.val;

            

           
            var val = $(this).val();

            for (var i = 0; i < val; i++) {
                var cpNo = suggest + i;
                var lbl = i + 1;
                console.log(suggest);


                $(".appendEdit").append("asas");
            }

        }
    });
}


function scanBookQR(id) {

    $("#scanQr").modal();
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (content) {
        console.log(content);
        var data = { Id: id, Data: content };
        $.ajax({

            type: "POST",
            url: "/Books/TestQrScanner",
            contentType: "application/json",
            data: JSON.stringify(data),
            success: function (response) {

                location.reload();
            }

        });
       
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        console.log(cameras);
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });
    $("#closeScanner").click(function () {
        scanner.stop();
    });
}

function getBookDet(Id) {
    $("#viewBookDetmodal").modal();

    $.ajax({
        type: "GET",
        url: "/Books/GetData?id= " + Id,
        contentType: "application/json",
        success: function (response) {

            var data = response.booksViewModals;
            $("#bookDet").val(data[0].BookDetail);

        }

    });
}