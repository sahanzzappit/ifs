﻿$(document).ready(function () {
    feesGrid();
 

    $("#Grade").change(function () {
        var grdVal = $(this).val();
        ScheduleDropDown(grdVal);
        feesGrid();
    });

    $("#schedule").change(function () {
        feesGrid();
    });

   
    $('.search').click(function () {
        var year = ($('.yearpicker').val());
        feesGrid(year);
    });

    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });
});




function feesGrid() {
    var year = $(".yearpicker").val();

    var table = $('#feesTable').DataTable();


    var grdVal = $("#Grade").val();
    var scheduleVal = $("#schedule").val();

    if (scheduleVal == 'value') {
        scheduleVal = null;
    }

    table.column(0).visible(false);
    table.destroy();
    var table = $('#feesTable').dataTable({
        "responsive": true,
        //"sAjaxSource": "/Fees/GetList?year=" + year,
        "ajax": {
            'url': '/Fees/GetList',
            'type': 'GET',
            'contentType': "application/json",
            "data": { year : year , grade: grdVal, scheduleId: scheduleVal },
        },
        "bProcessing": true,
        "bFilter": false,
        //"stateSave": true,
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [0, 1] },
            { "width": "40px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 2 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 3 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 4 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 5 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 6 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 7 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 8 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 9 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 10 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 11 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 12 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 13 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 14 },
            { "width": "30px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 15 },
        ],
        "columns": [
            { "data": "GradeId" }, 
            { "data": "ClassScheduleId" }, 
            { "data": "StudentName" },   
            { "data": "Fee" },   
            {
                "render": function (data, type, full, meta) {
                    if (full.Months.length >= 1) {
                        var isPaid = (full.Months[0].IsPaid) ? true : false;
                        if (isPaid)
                            return full.Months[0].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[0].Id + ' , ' + full.Months[0].Year + ') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                        else
                            return '<button type="submit" class="btn btn-add glyphicon glyphicon-add pay pay1" id="1"  onclick="SavePaid(' + full.Id + ', ' + full.Months[0].Id + ' , ' + full.Months[0].Year + ' , ' + full.Fee + ')">Pay</button>';

                    }
                }
            },           
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[1].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[1].DateTime + '<a class=" delete" mo-id="1" onclick="DeleteFee(' + full.Id + ', ' + full.Months[1].Id + ' , ' + full.Months[1].Year +' ) " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="2" onclick="SavePaid(' + full.Id + ', ' + full.Months[1].Id + ', ' + full.Months[1].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[2].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[2].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[2].Id + ' , ' + full.Months[2].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="2" onclick="SavePaid(' + full.Id + ', ' + full.Months[2].Id + ', ' + full.Months[2].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[3].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[3].DateTime + '<a class=" delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[3].Id + ' , ' + full.Months[3].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="4" onclick="SavePaid(' + full.Id + ', ' + full.Months[3].Id + ', ' + full.Months[3].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[4].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[4].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[4].Id + ' , ' + full.Months[4].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="5" onclick="SavePaid(' + full.Id + ', ' + full.Months[4].Id + ', ' + full.Months[4].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[5].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[5].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[5].Id + ' , ' + full.Months[5].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="6" onclick="SavePaid(' + full.Id + ', ' + full.Months[5].Id + ', ' + full.Months[5].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[6].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[6].DateTime + '<a class=" delete" mo-id="1" onclick="DeleteFee(' + full.Id + ', ' + full.Months[6].Id + '  , ' + full.Months[6].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="7" onclick="SavePaid(' + full.Id + ', ' + full.Months[6].Id + ', ' + full.Months[6].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[7].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[7].DateTime + '<a class=" delete" mo-id="1" onclick="DeleteFee(' + full.Id + ', ' + full.Months[7].Id + '  , ' + full.Months[7].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="8" onclick="SavePaid(' + full.Id + ', ' + full.Months[7].Id + ', ' + full.Months[7].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[8].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[8].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[8].Id + ' , ' + full.Months[8].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="9" onclick="SavePaid(' + full.Id + ', ' + full.Months[8].Id + ', ' + full.Months[8].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[9].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[9].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[9].Id + ' , ' + full.Months[9].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="10" onclick="SavePaid(' + full.Id + ', ' + full.Months[9].Id + ', ' + full.Months[9].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[10].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[10].DateTime + '<a class="  delete" mo-id="1" onclick="DeleteFee(' + full.Id + ' , ' + full.Months[10].Id + ' , ' + full.Months[10].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="11" onclick="SavePaid(' + full.Id + ', ' + full.Months[10].Id + ', ' + full.Months[10].Year + ' , ' + full.Fee + ')">Pay</button>';
                }
            },
            {
                "render": function (data, type, full, meta) {
                    var isPaid = (full.Months[11].IsPaid) ? true : false;
                    if (isPaid)
                        return full.Months[11].DateTime + '<a class=" delete" mo-id="1" onclick="DeleteFee(' + full.Id + ', ' + full.Months[11].Id + '  , ' + full.Months[11].Year +') " style="margin:3px 3px 3px 20px;color:red;"><span class="glyphicon glyphicon-trash"></span></a >';
                    else
                        return '<button  class="btn btn-add glyphicon glyphicon-add pay pay1" id="12" onclick="SavePaid(' + full.Id + ', ' + full.Months[11].Id + ', ' + full.Months[11].Year + ' , ' + full.Fee + ')">Pay</button>' 
                      
                }
            },
              
        ]
       
    });

 
    
}




function ScheduleDropDown(grdVal) {
    $("#schedule").empty();
    $("#schedule").append('<option value="">Classes</option>');

    $.ajax({
        type: "GET",
        url: "/Fees/GetDropDownVal?grdVal=" + grdVal,
        contentType: "application/json;charset=utf-8",
        success: function(response) {
            var data = (response.scheduleViewModels);

            

            for (var i = 0; i < data.length; i++) {
                var day = data[i].DateName;
                var startTime = data[i].StartTime;
                var endTime = data[i].EndTime;
                var schId = data[i].ScheduleId;


                
                
                $("#schedule").append($("<option></option>").attr("value", schId).text(day + "-" + startTime + "-" + endTime));
            }
            
        }
    });
}




function SavePaid(studnentId, monthId, Year, Fee) {

    var data = { StudentId: studnentId, MonthId: monthId, Year: Year, Fee: Fee};

    $.ajax({
        type: "POST",
        url: "/Fees/SavePaid",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(data),
        success: function (data) {    
            toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button> <button type='button' id='cancelConfirmation' class='btn clear'>No</button>", 'send invoice?',
                {
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationRevertYes").click(function () {

                            var url = "/Fees/SendInvoice";
                            $.post(url, { StudentId: studnentId, MonthId: monthId, Year: Year }, function (data) {
                                if (data == "sent") {
                                    toastr.success('Invoice sent...');
                                    feesGrid();
                                    location.reload();
                                }
                                else {
                                    toastr.error('Unable to send invoice...');

                                }
                            });
                        });
                        $("#cancelConfirmation").click(function () {
                            feesGrid();
                            location.reload();
                        });
                    }
                });

            toastr.success('Payment saved...');
        },
        error: function () {
            toastr.error('Unable to save payment...');
        }

    });


}




function DeleteFee(StudentId, Id, Year) {
    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete payment?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {

                    var url = "/Fees/DeleteFee";
                    $.post(url, { StudentId: StudentId, MonthId: Id, Year: Year }, function (data) {
                        if (data == "Deleted") {
                            feesGrid();
                            location.reload();
                            toastr.success('Payment deleted...');
                        }
                        else {
                            toastr.error('Unable to delete payment...');

                        }
                    });
                });
            }
        });
    console.log(Id);
}

