﻿$(document).ready(function() {
    grdList();
    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $(".gradeNameCheck").on("change paste keyup", function () {
        if ($(this).val() != "") {
            $(".gradeNameEmpty").addClass('hidden');
        }
        else {
            $(".gradeNameEmpty").removeClass('hidden');
        }
    });
    $('#gradesDisTable').on('change', '.toggle-button', function () {
        var isActive = false;
        var id = $(this).attr('id');
        if ($(this).prop("checked") == true) {
            isActive = true;
        }

        var obj = { id: id, status: isActive };

        $.ajax({

            type: "POST",
            url: "/GradeList/ChangeStatus",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {


            }

        });
    });  

})



function openModal() {
    $('#grademodal').modal();
    $(".gradeNameEmpty").addClass('hidden');
}

function grdList() {

    var table = $('#gradesDisTable').DataTable();
    table.destroy();
    var table = $('#gradesDisTable').dataTable({
        "responsive": true,
        "sAjaxSource": "/GradeList/GetGrdList",
        "bProcessing": true,
        "columnDefs": [
            //{ "width": "10px","targets": 2 },
            //{ "width": "10px", "targets": 3 },
            { "width": "50px", "className": "dt-center button-wrapper", "targets": 0 },
            { "width": "200px", "className": "text-wrapper", "targets": 1 },
            { "width": "200px", "className": "text-wrapper", "targets": 2 },
            { "className": "text-wrapper", "targets": 3 },
            { "width": "20px", "className": "dt-center text-wrapper-1", "targets": 4 },
            { "width": "20px", "className": "dt-center text-wrapper-1", "targets": 5 },
        ],
        "aoColumns": [
            {

                "render": function (data, type, full, meta) {
                    if (full.IsActive) {
                        return '<div><input type="checkbox" class="toggle-button" data-toggle="toggle" value="active" checked  id="' + full.Id + '" data-onstyle="success" data-offstyle="danger" style="width:5px;"></div>';
                    }
                    else {
                        return '<div><input type="checkbox" class="toggle-button" data-toggle="toggle" value="inActive"  id="' + full.Id + ' "  data-onstyle="success" data-offstyle="danger"></div>';
                    }
                    
                }
            },
            { "data": "GradeName" },
            { "data": "Fees" },
            { "data": "Description" },
            {
                "render": function (data, type, full, meta) { return '<a class="btn btn-edit glyphicon glyphicon-edit" style="float:center;" onclick="GetSchedule(' + full.Id + ' ) " "></a>'; }
            },
            {
                data: null, render: function (data, type, row) {
                    return "<a class='glyphicon glyphicon-trash' style='float:center;color:red;margin-top:10px;' onclick='DeleteData(" + row.Id + ")'' ></a>";
                }
            },
        ],
        "fnDrawCallback": function () {
            $('.toggle-button').bootstrapToggle({
                on: '',
                off: '',
            });
            
        },
        "initComplete": function (settings, json) {
            $("#activeGrdCount").append(json.TotalActive);
            $("#inactiveGrdCount").append(json.TotalInactiveCount);
            $("#totStudents").append(json.TotStudents);
        }


    });
}

function SaveGrade() {
    var gradeName = $("#gradeName").val();
    var fees = $("#fees").val();
    var des = $("#description").val();

    if (gradeName == "") {
        $(".gradeNameEmpty").removeClass('hidden');
    }

    else {
        var obj = { GradeName: gradeName, Fees: fees, Description: des }

        $.ajax({

            type: "POST",
            url: "/GradeList/SaveGrade",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "success") {
                    toastr.success('Saved grade...');
                    grdList();
                    location.reload();
                    
                }
                else if (response == "error") {
                    toastr.error('Grade already exist...');
                }

               
            }

        });
    }


}

function GetSchedule(Id) {
    $("#editGrademodal").modal();
    $(".gradeNameEmpty").addClass('hidden');

    $.ajax({
        type: "GET",
        url: "/GradeList/GetGrade?Id=" + Id,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(),
        success: function (data) {
            var response = data.gradeModels;

            $("#editGradeName").val(response[0].GradeName);
            $("#editFees").val(response[0].Fees);
            $("#editDescription").val(response[0].Description);
            $("#hdnGrdId").val(response[0].Id);
      



        }
    });
}

function EditGrade() {
    var name = $("#editGradeName").val();
    var fees = $("#editFees").val();
    var des = $("#editDescription").val();
    var id = $("#hdnGrdId").val();

    var data = {Id:id, GrdName: name, Fees: fees, Description: des };

    $.ajax({
        type: "POST",
        url: "/GradeList/EditGrade",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(data),
        success: function (data) {
            
            grdList();
            toastr.success('Grade updated...');
            location.reload();
        }
    });
}


function DeleteData(Id) {
    //if (confirm("Are you Sure You Want To Delete")) {
    //        Delete(Id);
    //}
    //else {
    //        return false;
    //}

    Delete(Id);
}

function Delete(Id) {
    var url = "/GradeList/DeleteGrade";

    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete grade?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    $.post(url, { Id: Id }, function (data) {
                        var response = data.gradeModels;

                        if (data == "Deleted") {
                            
                            grdList();
                            //location.reload();
                            toastr.success('Grade deleted...');
                        }
                        else {
                            toastr.error("can not delete  " + response[0].GradeName + ", classes assigned to the grade, delete classes first");
                        }

                    });
                });
            }
        });
}


