﻿var hasformErrors = false;

$(document).ready(function () {

    classGrid();

    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $("#Grade").change(function () {
        //$('#gradesTable').dataTable().fnFilter(this.value);
        var grdVal = $(this).val();
        ScheduleDropDown(grdVal);
        classGrid();
    });

    $("#schedule").change(function () {
        //$('#gradesTable').dataTable().fnFilter(this.value);
        //if ($("#schedule").val() == "") {
        //    $('#gradesTable').dataTable().fnFilter($("#Grade").val());

        //}
        var scheduleVal = $(this).val();
        classGrid();
    });
 

 
    $(".addRow").click(function () {
        var rowId = $(this).attr('id');
        var rowCount = $("[parentId='" + rowId+"']").length;
        var nextElement = parseInt(rowCount) + 1;
        

        $("#row_" + rowId).append('<div  id = "row_' + rowId + '_' + nextElement + '" parentId = "' + rowId +'" class= "col-sm-9 row append_row" style="margin-top:10px;float:right;margin-right:3px;">'
            +'<div class= "col-sm-3" >'
            +'<label class="control-label " for="classTime"></label>'
            + '<input type="time" class="form-control col-sm-3 time1 timeValidate date_1" id="startTime_' + rowId + '_' + nextElement +'" placeholder="Enter Class Time" name="classTime" date-id="1" required style="margin-top:10px;">'
            +'</div>'
            +'<div class="col-sm-2">'
            + '<label class="control-label " for="classTime"></label>'
            + '<input type="time" class="col-sm-1 form-control time2 timeValidate date_1" id="endTime_' + rowId + '_' + nextElement +'" placeholder = "Finishing Time" name = "classTime" date-id="1" required style="margin-top:10px;">'
                                +'</div>'
                +'<div class="col-sm-1 add">'
                    +'<label class="control-label" for="AddTime"></label>'
            + '<a  class="glyphicon glyphicon-trash removediv"  id="' + nextElement +'" name="removeDom" style="margin-top:25px;color:red;"></a>'
                +'</div>'
            + '</div>');

        $(".time1").focusout(function () {
            var startTime = $(this).val();
            var startDate = new Date("1/1/1900 " + startTime);
            var hours1 = startDate.getHours();
            var mins = startDate.getMinutes();
            $(".time2").focusin(function () {

                var hours2 = (hours1 + 1);
                if (hours2 > 9 && mins > 9) {
                    $(this).val(hours2 + ":" + mins);
                }
                else if (hours2 > 9 && mins < 9) {
                    $(this).val(hours2 + ":0" + mins);
                }
                else if (hours2 < 9 && mins > 9) {
                    $(this).val("0" + hours2 + ":" + mins);
                }
                else {
                    $(this).val("0" + hours2 + ":0" + mins);
                }

            });

        });


    });

    $('.row').on("click", ".removediv", function () {
        $(".append_row").last().remove();
    });

    $(".time1").focusout(function () {
        var startTime = $(this).val();
        var startDate = new Date("1/1/1900 " + startTime);
        var hours1 = startDate.getHours();
        var mins = startDate.getMinutes();
        $(".time2").focusin(function () {

            var hours2 = (hours1 + 1);
            if (hours2 > 9 && mins > 9) {
                $(this).val(hours2 + ":" + mins);
            }
            else if (hours2 > 9 && mins < 9) {
                $(this).val(hours2 + ":0" + mins);
            }
            else if (hours2 < 9 && mins > 9){
                $(this).val("0" + hours2 + ":" + mins);
            }
            else {
                $(this).val("0" + hours2 + ":0" + mins);
            }

        });

    });


  
    $(".direct").click(function () {

        $(this).find('.time1').focus();

    });

  

});


function openModal() {
    $('#addGrademodal').modal();
    $(".formHasError").addClass('hidden');
}

function addClass() {
    window.location.href = '/Grade/AddClass';
    $(".formHasError").addClass('hidden');
}

function ScheduleDropDown(grdVal) {
    $("#schedule").empty();
    $("#schedule").append('<option value="">Classes</option>');

    $.ajax({
        type: "GET",
        url: "/Grade/GetDropDownVal?grdVal=" + grdVal,
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            var data = (response.scheduleViewModels);



            for (var i = 0; i < data.length; i++) {
                var day = data[i].DateName;
                var startTime = data[i].StartTime;
                var endTime = data[i].EndTime;
                var schId = data[i].ScheduleId;


                $("#schedule").append($("<option></option>").val(schId).text(day + "-" + startTime + "-" + endTime));
            }

        }
    });
}




function SaveData() {

    var dataObj = [];
    var timeObj = [];


    var gradeName = $("#clsName").val();
    var classFee = $("#fees").val();

    $('input[type=checkbox]').each(function () {
        

        if (this.checked) {
            var date = $(this).val();
            var id = $(this).attr('id');

            var index = 1;

            $("[parentid='" + date + "']").each(function () {
                

                var startTime = $("#startTime_" + date + "_" + index).val();
                var endTime = $("#endTime_" + date + "_" + index).val();

                if (startTime && endTime != null) {
                    dataObj.push({ DateNameId: date, SartTime: startTime, EndTime: endTime });
                }

                index++;
            });
            
        }
    });
    

    $.each(dataObj, function (key, value) {
        time = true;

        if (value.SartTime == "") {
            time = false;
        }
        else if (value.EndTime == "") {
            time = false;
        }
        else {
            time = true;
        }

            if (!time) {
                 $(this).addClass("has-error");
                 hasformErrors = true;
            }
             else {
                $(this).removeClass("has-error");
                hasformErrors = false;
            }

    
    });

    if (hasformErrors) {
        $(".formHasError").removeClass('hidden');
        //alert("Please fix the errors on the form before submit");
        return false;
    }


    var response = true;
    var checked;
    checked = dataObj.join(',');

    if (checked.length > 0) {
        response = true;
    } else {
        response = false;
    }

          if (!response) {
            $(this).addClass("has-error");
            hasformErrors = true;
         }
         else {
             $(this).removeClass("has-error");
              hasformErrors = false;
         }



    var obj = { classSchedule: dataObj, GradeName: gradeName, Fees: classFee };

    for (var i = 0; i < obj.classSchedule.length; i++) {
        var startTime = dataObj[i].SartTime;
        var endTime = dataObj[i].EndTime;

        timeObj.push({startTime: startTime, endTime: endTime});
       
    }
   

    

    if (hasformErrors) {
        //pnotify("error", "Please fix the errors on the form before submit", 'error');
        return false;
    }
    else if ($("#GradeName").val() == "") {
        //pnotify("error", "please enter grade name", 'error');
        return false;
    }


    else {
        $.ajax({

            type: "POST",
            url: "/Grade/SaveData",
            contentType: "application/json",
            data: JSON.stringify(obj),
            success: function (response) {
                if (response == "Invalid") {
                    toastr.error('Unable to save class time is not valid...');
                }
     
                else {

                    toastr.success('Saved class...');
                    $("#addGrademodal").modal('hide');
                    location.reload();
                    $('#gradesTable').dataTable().fnDraw();
                    window.location.href = 'Index/';
                }
                    

            }
        });
    }
}

function classGrid() {
    var table = $('#gradesTable').DataTable();
    table.destroy();

    var grdVal = $("#Grade").val();
    var scheduleVal = $("#schedule").val();

    if (scheduleVal == 'value') {
        scheduleVal = null;
    }
    

    var table = $('#gradesTable').dataTable({
        "responsive": true,
        //"sAjaxSource": "/Grade/GetList",
        "ajax": {
            'url': '/Grade/GetList',
            'type': 'GET',
            'contentType': "application/json",
            "data": { grade: grdVal, scheduleId: scheduleVal},
        },
        "bProcessing": true,
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [1, 2] },
            { "width": "10px", "className": "dt-center", "targets": 7 },
            { "width": "50px", "className": "dt-center", "targets": 8 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 9 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 10 },
            { "searchable": false, "targets": 8 }
        ],
        "aoColumns": [
            {
                "render": function (data, type, full, meta) { return '<input type="checkbox" class="check" value=' + full.Id + '>'; }
            },
            { "data": "GradeId" },
            { "data": "Id" },
            { "data": "GradeName" },
            { "data": "ClassDate" },
            { "data": "StartTime" },
            { "data": "EndTime" },
            {"data": "Fees"},
            { "data": "NoOfStudents"},
            {
                "render": function (data, type, full, meta) { return '<a class="btn btn-edit glyphicon glyphicon-edit" style="float:center;"  onclick="GetSchedule(' + full.Id + ' ) " "></a>'; }
            },
            {
                data: null, render: function (data, type, row) {
                    return "<a class='glyphicon glyphicon-trash' style='float: center;color:red;margin-top:10px;' onclick='DeleteData("+ row.Id + ")'' ></a>";
                }
            },              
        ]
    
        
    });
}

var selectedItems = [];

$(document).on("change", ".check", function () {
    if ($(this).is(':checked')) {
        selectedItems.push($(this).val());
    }
    else {
        for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i] === $(this).val()) {
                selectedItems.splice(i, 1);
                i--;
            }
        }
    }
});

function sendMessageModal() {
    $("#messageContentModal").modal();

}

function sendSingleMessage() {
    var message = $("#messageContent").val();

    var messageData = { message: message, selectedItems: selectedItems };

    if (selectedItems.length > 0) {
        $.ajax({

            type: "POST",
            url: "/Grade/SendMessage",
            contentType: "application/json",
            data: JSON.stringify(messageData),
            success: function (response) {
                location.reload();
                $('#gradesTable').dataTable().fnDraw();
                toastr.success('Message sent...');

            }
        });
    }

    else {
        toastr.error('Please select a class...');
    }
}

function GetSchedule(Id) {
    $("#editGrademodal").modal('show');



    $.ajax({

            type: "GET",
            url: "/Grade/GetSchedule?Id=" + Id,
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(),
            success: function (data) {
            var response = data.returnList;

                console.log(response);

                $("#editGradeName").val(response[0].GradeName);
                $("#editFees").val(response[0].Fees);
                $("#editStartTime").val(response[0].StartTime);
                $("#editEndTime").val(response[0].EndTime);
                $("#clsDate").val((response[0].ClassDate));
                $("#hdnDateId").val((response[0].classDateId));
                $("#hdnGrdId").val(response[0].Id);
                $("#hdnGrdName").val(response[0].HdnGrdId);
                


        }
    });

}

function UpdateData() {
    var Id = $("#hdnGrdId").val();
    var gradeId = $("#hdnGrdName").val();
    var classDateId = $("#clsDate").val();
    var startTime = $("#editStartTime").val();
    var endTime = $("#editEndTime").val();
    var fees = $("#editFees").val();
    var hdnGrdName = $("#hdnGrdName").val();


    var obj = { Id: Id, ClassDateId: classDateId, StartTime: startTime, EndTime: endTime, Fees: fees, GradeId : gradeId};

    console.log(obj);

    $.ajax({

        type: "POST",
        url: "/Grade/UpdateClass",
        contentType: "application/json",
        data: JSON.stringify(obj),
        success: function (response) {
            console.log(response);
            if (response == 'False') {
                toastr.error('Time is not valid...');
            }
            else {
                $("#editGrademodal").modal('hide');
                location.reload();
                $('#gradesTable').dataTable().fnDraw();
                toastr.success('Class updated...');
            }

        }

    });
    
}

function DeleteData(Id) {
    //if (confirm("Are you Sure You Want To Delete")) {
    //    Delete(Id);
    //}
    //else {
    //    return false;
    //}
    Delete(Id);
}

function Delete(Id) {
    var url = "/Grade/DeleteGrade";

    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete class?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    $.post(url, { Id: Id }, function (data) {
                        if (data == "error") {
                            toastr.error('can not delete the class...');
                        }
                        else if (data == "NotAuthorized") {
                            toastr.error('Not authorized to delete the class...');
                        }
                        else if (data == "Deleted") {
                            $('#gradesTable').dataTable().fnDraw();
                            location.reload();
                            toastr.success('Class deleted...');
                        }
                        else {
                            toastr.error('Something Went Wrong...');
                        }
                    });
                });
            }
        });
}

function viewInbox() {
    $.ajax({

        type: "POST",
        url: "/Grade/ViewMails",
        contentType: "application/json",
        success: function (response) {
            console.log(response);

        }

    });
}