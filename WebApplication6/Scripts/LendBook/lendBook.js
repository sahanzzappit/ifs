﻿$(document).ready(function () {
    lendBooksGrid($("#Category").val());

    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $("#stdNo").on("change paste keyup", function () {
        var val = $('#stdNo').val();

        $(".filter").each(function () {
            if ($(this).attr('id') == val) {
                $(this).show();
            }

            else {
                $(this).hide();
            }
        });

        if (val == "") {

            $('#fakeloader-overlay').hide();
            location.reload();
            $('#fakeloader-overlay').hide();
            $(".filter").show();

            $("#bookNo").prop("disabled", false);


        }
        else {

            $("#bookNo").prop("disabled", true);
        }
    });



    $("#Category").change(function () {

        lendBooksGrid($(this).val());
        
    });

    $("#bookNo").on("change paste keyup", function () {
        var val = $("#bookNo").val();
        target = 'book_' + val;

        $(".check").each(function () {
            if ($(this).attr('id') == val) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });

        if (val == "") {
            var val = $("#Category").val(),
                target = '.' + val;
            target1 = '.' + val;

            $('.check').hide();
            $('.catFilter').hide();
            $(target).show();
            $(target1).show();
            $("#Category").prop("disabled", false);
            $("#stdNo").prop("disabled", false);

            $('#fakeloader-overlay').hide();
            //location.reload();
            $('#fakeloader-overlay').hide();
        }

        else {
            $('.catFilter').hide();


            $('#book_' + val).show();
            $('#book_1_' + val).show();

            $("#Category").prop("disabled", true);
            $("#stdNo").prop("disabled", true);
        }
    });

});


function lendBooksGrid(catId) {

    $('#booksResult').empty();
    $.ajax({
        type: 'GET',
        url: '/LendBooks/GetLibraryDet',
        data: { CategoryId: catId },
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            
            $.each(response.BookList, function (key, value) {
                console.log(value.IsLent);
                if (value.IsAvailable) {
                    if (!value.IsLent) {

                        $('#booksResult').append('<div class="card text-white bg-dark mb-3" style="max-width: 18rem;"><div class= "card-header" > Available<span><label class="glyphicon glyphicon-book available"></label></span></div> ' +
                        '<div class="card-body">' +
                            '<h5 class="card-title">'  + value.CategoryName + '</h5> ' +
                            '<p class="card-text">' + value.BookName + '</p>' + '<div class="card-footer bg-transparent border-success"><button class="btn btn-info cardLendButton" onClick="LendBook(' + value.BookId + ')">Lend</button></div>' + 
                        '</div>' +
                        '</div>')
                    }
                    else {

                        $('#booksResult').append('<div class="card text-white bg-dark mb-3" style="max-width: 18rem;"><div class= "card-header" > Available<span><label class="glyphicon glyphicon-book available"></label></span></div> ' +
                            '<div class="card-body">' +
                            '<h5 class="card-title">' + value.CategoryName + '</h5> ' +
                            '<p class="card-text">' + value.BookName + '</p>' + '<div class="card-footer bg-transparent border-success"><button class="btn btn-info cardLendButton col-sm-5" onClick="LendBook(' + value.BookId + ')">Lend</button><button class="btn btn-light cardRetButton col-sm-5" style="margin-left:10px;" onClick="ReturnBook(' + value.BookId + ')">Return</button></div>' +
                            '</div>' +
                            '</div>')
                    }
                }
                else {
                    if (!value.IsLent) {

                        $('#booksResult').append('<div class="card text-white bg-dark mb-3" style="max-width: 18rem;"><div class= "card-header" >Not Available<span><label class="glyphicon glyphicon-book available"></label></span></div> ' +
                            '<div class="card-body">' +
                            '<h5 class="card-title">' + value.CategoryName + '</h5> ' +
                            '<p class="card-text">' + value.BookName + '</p>' +
                            '</div>' +
                            '</div>')
                    }
                    else {

                        $('#booksResult').append('<div class="card text-white bg-dark mb-3" style="max-width: 18rem;"><div class= "card-header" >Not Available<span><label class="glyphicon glyphicon-book available"></label></span></div> ' +
                            '<div class="card-body">' +
                            '<h5 class="card-title">' + value.CategoryName + '</h5> ' +
                            '<p class="card-text">' + value.BookName + '</p>' + '<div class="card-footer bg-transparent border-success"><button class="btn btn-light cardRetButton" onClick="ReturnBook(' + value.BookId + ')">Return</button></div>' +
                            '</div>' +
                            '</div>');
                    }

                }

                copies = null;
            });

        }
    });
}


function LendBook(bookId) {
    $('#lendBook').modal();
    $('.formError').addClass('hidden');
    $('#hdnBookIdForLend').val(bookId);
    $('#stuIdForLend').val(null);
    $('#stuNameForLend').val(null);
    $("#bookCopies").empty();

    $.ajax({
        type: "GET",
        url: '/LendBooks/GetCopyDetails',
        data: { BookId: bookId },
        success: function (respone) {

            $.each(respone.copies, function (key, value) {

                $("#bookCopies").append('<option value="' + value + '">' + value + '</option>')
            });

        }
    });

}

function saveLend() {
    var studentId = $('#stuIdForLend').val();
    var bookId = $('#hdnBookIdForLend').val();
    var copyId = $("#bookCopies").val();

    if (copyId == null) {
        copyId = bookId;
    }

    if (studentId == "" || bookId == "") {
        $('.formError').removeClass('hidden');
    }
    
    var obj = { CopyId: copyId, StudentId: studentId, BookId: bookId };
    console.log(obj);
    $.ajax({
        type: "POST",
        url: '/LendBooks/SaveLend',
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(obj),
        success: function (response) {
            if (response == "not valid student") {
                toastr.error("Student isn't eligible for the category...");
            }
            else {
                $("#lendBook").modal('hide');
                location.reload();
               
            }
        }
    });

}


function ReturnBook(bookId) {
    $('#returnBook').modal();
    $('#lentRecords').empty();

    $.ajax({
        type: 'GET',
        url: '/LendBooks/GetRetDetails',
        data: { BookId: bookId },
        success: function (response) {
            $.each(response.returnViewModals, function (key, value) {
                console.log(value);
                $('#lentRecords').append('<div class="form-inline" style="margin-top:6px;margin-right:6px;"><label class="col-sm-8" style="background-color:yellow;">' + value.BookName + '(book no :-' + value.BookNo + ') / (copy no:-' + value.CopyNo + ')</label>' +
                    '<button type="button" class="btn btn-info col-sm-2" id="saveLend" onclick="SaveReturn(' + value.CopyNo + ', ' + value.BookNo + ', ' + value.StudentId + ')" style="margin:0px 0px 0px 10px;">Return</button>');
            });
        }
    });
}

function SaveReturn(copyNo, bookNo, stdId) {
    var obj = { CopyNo: copyNo, BookNo: bookNo, StudentId: stdId };

    $.ajax({
        type: "POST",
        url: "/LendBooks/SaveReturn",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(obj),
        success: function (response) {
            location.reload();
            $('#DynamicTable').dataTable().fnDraw();

        }
    });
}

function Change(val) {
    

    if (val != undefined) {
        $('.filter').each(function () {
            var std = $(this).attr('val');
            $(this).addClass('disabled');

            $('.check').each(function () {
                $('.lend').each(function () {

                    if ($(this).attr("stuid") == std && $(this).attr("catid") == val) {
                        $('.name').each(function () {
                            if ($(this).attr("val") == std) {
                                $(this).parent('tr').removeClass('disabled');
                            }

                        });

                    }
                });

            });


        });
    }

}


    
function CopiesModalData(bookId, date, stId, bkVal) {
    $("#copies").empty();

    $.ajax({
        type: "GET",
        url: "/LendBooks/GetCopyDetails",
        contentType: "application/json;charset=utf-8",
        data: { BookId: bookId },
        success: function (data) {
            if (data.error1 == "hasOnlyOneCopy") {
                toastr.warning('this book has only one copy...');
                SaveWithOneCopy(bookId, date, stId, bkVal);
            }

            if (data.error2 == "hasNoCopies") {
                toastr.warning('this book has no copies...');
                SaveWithOneCopy(bookId, date, stId, bkVal);
            }

            if (data.copiesModal.length == 0) {
                $("#bookSelection").modal('hide');
                toastr.error('no copies available...');
            }

            else {
                $("#bookSelection").modal();
                for (var i = 0; i < data.copiesModal.length; i++) {
                    var copy = data.copiesModal[i].CopyNo;

                    $("#copies").append('<option value="' + copy + '">' + copy + '</option>')
                }

            }
   
            
            $("#hdnStdId").val(stId);
            $("#hdnBookId").val(bkVal);
            
            
        }
    });
    $("#hdnDate").val(date);
   
}

function CopiesModalData2(bookId, stId) {

    $.ajax({
        type: "GET",
        url: "/LendBooks/GetRetDetails",
        contentType: "application/json;charset=utf-8",
        data: { BookId: bookId, StudentId: stId },
        success: function (data) {
            var list = data.returnViewModals[0];
            var copyNo = list.CopyNo;
            var bookNo = list.BookNo;
            var stdId = list.StudentId;

            SaveReturn(copyNo,bookNo, stdId);

        }
    });
   
    
}

function SaveCopy() {
    var val = $("#copies").val();
    var date = $("#hdnDate").val();
    var stdId = $("#hdnStdId").val(); 
    var bkId = $("#hdnBookId").val(); 

    var newDate = new Date(date);

    var obj = { CopyId: val, LentDate: newDate, StudentId: stdId, BookId: bkId};

    $.ajax({
        type: "POST",
        url: "/LendBooks/SaveLend",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(obj),
        success: function (data) {
            $("#bookSelection").modal('hide');
            location.reload();
            $('#DynamicTable').dataTable().fnDraw();


        }
    });

}

function SaveWithOneCopy(bookId, date, stId) {
   
    var newDate = new Date(date);

    var obj = { LentDate: newDate, StudentId: stId, BookId: bookId };

    $.ajax({
        type: "POST",
        url: "/LendBooks/SaveLendWithOneCopy",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(obj),
        success: function (data) {
            
            if (data == "noCopiesAvailable") {        
                toastr.error('no copies available...');
            }



            if(data == "done") {
                $("#bookSelection").modal('hide');
                location.reload();
                $('#DynamicTable').dataTable().fnDraw();
            }
        }
    });
}

function SaveReturn(copyNo, bookNo, stdId) {
    //var newDate = new Date(date);

    var obj = { CopyNo: copyNo, BookNo: bookNo, StudentId: stdId};

    $.ajax({
        type: "POST",
        url: "/LendBooks/SaveReturn",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(obj),
        success: function (data) {
            location.reload();
            $('#DynamicTable').dataTable().fnDraw();

        }
    });
}



