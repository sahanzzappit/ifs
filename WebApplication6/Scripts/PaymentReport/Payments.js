﻿$(document).ready(function () {

    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var from = day + "/" + month + "/" + year;


    PaymentGrid();

    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $("#Name").change(function () {
        //$('#paymentTable').dataTable().fnFilter(this.value);
        var grdVal = $(this).val();
        ScheduleDropDown(grdVal);
        PaymentGrid();
    });
    $("#schedule").change(function () {
        //$('#paymentTable').dataTable().fnFilter(this.value);
        //if ($("#schedule").val() == "") {
        //    $('#paymentTable').dataTable().fnFilter($("#Name").val());

        //}
        PaymentGrid();
    });


    $('#min').change(function () {
        PaymentGrid();
    });
    $('#max').change(function () {
        PaymentGrid();
    });

        
});


function ScheduleDropDown(grdVal) {
    $("#schedule").empty();
    $("#schedule").append('<option value="">Classes</option>');

    $.ajax({
        type: "GET",
        url: "/PaymentReport/GetDropDownVal?grdVal=" + grdVal,
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            var data = (response.scheduleViewModels);



            for (var i = 0; i < data.length; i++) {
                var day = data[i].DateName;
                var startTime = data[i].StartTime;
                var endTime = data[i].EndTime;
                var schId = data[i].ScheduleId;




                $("#schedule").append($("<option></option>").attr("value", schId).text(day + "-" + startTime + "-" + endTime));
            }

        }
    });
}


function PaymentGrid() {
    var table = $('#paymentTable').DataTable();
    table.destroy();

    var grdVal = $("#Name").val();
    var scheduleVal = $("#schedule").val();
    var fromDate = $("#min").val();
    var toDate = $("#max").val();

    if (scheduleVal == 'value') {
        scheduleVal = null;
    }

    var table = $('#paymentTable').dataTable({
        language: {
            "zeroRecords": ""
        },
        "responsive": true,
        //"sAjaxSource": "/PaymentReport/GetDetail",
        "ajax": {
            'url': '/PaymentReport/GetDetail',
            'type': 'GET',
            'contentType': "application/json",
            "data": { grade: grdVal, scheduleId: scheduleVal, FromDate: fromDate, Todate: toDate },
        },
        "bProcessing": true,
        'columnDefs': [
            {
                "targets": 1,
                "className": "text-center",
            },
            {
                "targets": 2,
                "PaymentAmount": "text-right",
            },
        ],
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [5, 6] },
            { "className": "dt-center text-wrapper-1 ;  ;", "targets": 0 },
        ],
        "aoColumns": [
            {
                "data": "InvoiceDocument" ,
                "render": function (data, type, full, meta) {
                    if (full.InvoiceDocument == "")
                        return "";
                    else
                        return '<a href="' + full.InvoiceDocument +'" target="_blank"><span><i class="fas fa-file-pdf-o"></i></span></a>';
                }
            },
            { "data": "Grade" },
            { "data": "StudentName"},
            { "data": "PaymentAmount" },
            { "data": "PaidDate" },
            { "data": "year"},
            { "data": "GradeId" },
            { "data": "PaidFor" },

        ],


        "sPaginationType": "full_numbers",
        "footerCallback": function () {
            var api = this.api(), data;
            var intVal = function (i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };
            total_salary = api.column(3).data().reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

            total_page_salary = api.column(3, { page: 'current' }).data().reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

            total_page_salary = parseFloat(total_page_salary);
            total_salary = parseFloat(total_salary);

            $('#totalPayment').html(total_page_salary.toFixed(3) + "/" + total_salary.toFixed(3));
        },



  });


    //$.fn.dataTableExt.afnFiltering.push(
    //    function (settings, data, dataIndex) {
    //        min = $('#min').val();
    //        max = $('#max').val();
    //        date = data[3];

    //        getDate = date.split("/");
    //        startDateArray = min.split("-");
    //        endDateArray = max.split("-");

    //        var find = new Date(getDate[0], +getDate[1], getDate[2]).getTime();

    //        var startDateTimeStamp = new Date(startDateArray[0], +startDateArray[1], startDateArray[2]).getTime();
    //        var endDateTimeStamp = new Date(endDateArray[0], +endDateArray[1], endDateArray[2]).getTime();



    //        if (min == "" && max == "") {

    //            var dt = new Date();
    //            var date1 = dt.getFullYear() + "-" + "0" + (dt.getMonth() + 1) + "-" + "01";
    //            var date2 = dt.getFullYear() + "-" + "0" + (dt.getMonth() + 2) + "-" + "01";
    //            $('#min').val(date1);
    //            $('#max').val(date2);

    //            return true;
    //        }
    //        else if (startDateTimeStamp != "" && find > startDateTimeStamp && endDateTimeStamp != "" && find < endDateTimeStamp) {
    //            return true;
    //        }


    //        return false;
    //    }
    //);


    //var table = $('#paymentTable').DataTable();
    //$('#min').change(function () {
    //    table.draw();
    //});
    //$('#max').change(function () {
    //    table.draw();
    //});

}

function GetExcelRep() {

    var table = $('#paymentTable').DataTable();    

    
    var table2 = [];


    var obj = table
        .rows()
        .data();

    console.log(obj);

    for (var i = 0; i < obj.length; i++) {
        table2.push({ StudentName: obj[i].StudentName, Grade: obj[i].Grade, PaidDate: obj[i].PaidDate, PaymentAmount: obj[i].PaymentAmount, year: obj[i].year });
    }

    var dataObj = { tableList: table2 };

    $.ajax({
        type: "POST",       
        url: "/PaymentReport/Generate",
      
        data: dataObj,
        success: function (response) {
            console.log(response);
            window.location.href = response;
            $("#fakeloader-overlay").hide();
        }
    });
}

