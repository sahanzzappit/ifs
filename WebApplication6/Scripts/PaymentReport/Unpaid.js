﻿$(document).ready(function () {
    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });


    var d = new Date();

    var n2 = d.getMonth();


    $('#from option:eq(' + 1 + ')').prop('selected', true);
    $('#to option:eq(' + n2 + ')').prop('selected', true);

    var Startmonth = $("#from").val();
    var intVal1 = parseInt(Startmonth);

    $(function () {
        $("#getReport").trigger("click");
    });

    $("#stuSearchBox").click(function () {
        var val = $("#stdNo").val();
        $('#unpaidTable').dataTable().fnFilter(val);

    });

    $("#grade").change(function () {
        //$('#unpaidTable').dataTable().fnFilter(this.value);
        var grdVal = $(this).val();
        ScheduleDropDown(grdVal);
        UnpaidGrid();
    });
    $("#schedule").change(function () {
        UnpaidGrid();
    });

});

function ScheduleDropDown(grdVal) {
    $("#schedule").empty();
    $("#schedule").append('<option value="">Classes</option>');
    $(".panel").hide();
    $.ajax({
        type: "GET",
        url: "/Outstanding/GetDropDownVal?grdVal=" + grdVal,
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            var data = (response.scheduleViewModels);
            for (var i = 0; i < data.length; i++) {
                var day = data[i].DateName;
                var startTime = data[i].StartTime;
                var endTime = data[i].EndTime;
                var schId = data[i].ScheduleId;


                $("#schedule").append($("<option></option>").attr("value", schId).text(day + "-" + startTime + "-" + endTime));
            }
            
        }
    });
}

function UnpaidGrid() {

    var year = $("#year").val();
    var grade = $("#grade").val();
    var sch = $("#schedule").val();
    var Startmonth = $("#from").val();
    var Endmonth = $("#to").val();

    $(".panel").show();
    $("#unpaidTable").removeClass('hidden');
    $("#total").removeClass('hidden');

    $('#unpaidTable').DataTable({
        "ajax": {
            "url": "/Outstanding/GetList",
            "type": "GET",
            "datatype": "json",
            "data": { year: year, Startmonth: Startmonth, Endmonth: Endmonth, grade: grade, SchId: sch }
        },
        "columnDefs": [
            { "width": "10px", "className": "dt-center", "targets": 3 },


        ],
        destroy: true,

        "columns": [
            {
                "targets": 0,
                "data": "StuName",
                "orderable": true,
                "createdCell": function (td, cellData, rowData, row, col) {
                    if (rowData.IsGracePeriodExceeded) {
                        
                        var html = '<span title="Student has more than three outstanding payments for the year">' + row.StuName + '</span>';
                        $(td).css('background-color', '#F0E68C');
                        return html;
                    } else {
                        return '<span>' + row.StuName + '</span>';
                    }
                }
            },
            {

                "targets": 1,
                "data": "GrdName",
                "orderable": true,
                "render": function (data, type, row) {
                    return '<span>' + row.GrdName + '</span>';
                }
            },
            {
                "targets": 2,
                "data": "UnpaidMonth",
                "orderable": true,
                "render": function (data, type, row) {
                    return '<span>' + row.UnpaidMonth + '</span>';
                }
            },
            {

                "targets": 3,
                "data": "PaymentAmount",
                "orderable": true,
                "render": function (data, type, row) {
                    return '<span>' + row.PaymentAmount + '</span>';
                }
            },

        ],
        "sPaginationType": "full_numbers",
        "footerCallback": function () {
            var api = this.api(), data;
            var intVal = function (i) {
                return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
            };
            total_salary = api.column(3).data().reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

            total_page_salary = api.column(3, { page: 'current' }).data().reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

            total_page_salary = parseFloat(total_page_salary);
            total_salary = parseFloat(total_salary);

            $('#totalOutstanding').html(total_page_salary.toFixed(3) + "/" + total_salary.toFixed(3));
        },

    }); 
}


