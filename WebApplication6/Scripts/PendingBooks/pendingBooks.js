﻿$(document).ready(function () {
    PendingTable();

    $("#bookNo").on("change paste keyup", function () {

        PendingTable();

    });


    $("#stdNo").on("change paste keyup", function () {

        PendingTable();
    });
});


function PendingTable() {
    var bookNo = $("#bookNo").val();
    var studentNo = $("#stdNo").val();

    var table = $('#pending').DataTable();
    table.destroy();
    var table = $('#pending').dataTable({
        "responsive": true,
        "processing": true,
        "ajax": {
            'url': '/PendingBooks/GetList',
            'type': 'GET',
            'contentType': "application/json",
            "data": { BookNo: bookNo, studentNo: studentNo },
        },
        "bProcessing": true, 
        "columnDefs": [
            { "width": "30px", "className": "dt-center", "targets": 0 },
            { "width": "150px", "className": "dt-center", "targets": 1 },
            { "width": "100px", "className": "dt-center", "targets": 2 },
            { "width": "80px", "className": "dt-center", "targets": 3 },
            { "bVisible": false, "aTargets": [4] },
            { "searchable": false, "targets": [3] },
            { "searchable": false, "targets": [1] },
        ],
        "aoColumns": [
            { "data": "BookNo" },
            { "data": "BookName" },
            { "data": "StudentName" },
            { "data": "LentDate" },
            { "data": "CategoryName" },
        ]


    });
}

function GetExcelRep() {

    var bookNo = $("#bookNo").val();
    var stdNo = $("#stdNo").val();

    var filter = { BookNo: bookNo, StudentId: stdNo };

    $.ajax({
        type: "POST",
        url: "/PendingBooks/Generate",
        data: filter,
        success: function (response) {
            window.location.href = response;
            $("#fakeloader-overlay").hide();
        }
    });
}