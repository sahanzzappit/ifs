﻿$(document).ready(function () {
    studentGrid();

    
    getStudentAddFormData();

    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $("#Grade").change(function () {
        $('#studentTable').dataTable().fnFilter(this.value);
        var grdVal = $(this).val();
        ScheduleDropDown(grdVal);
        studentGrid()
    });

    $("#schedule").change(function () {
        //$('#studentTable').dataTable().fnFilter(this.value);
        //if ($("#schedule").val() == "") {
        //    $('#studentTable').dataTable().fnFilter($("#Grade").val());

        //}
        studentGrid()
    });


    $(".dropDown").change(function () {
        $("#clsName").empty();
        $("#clsDate").empty();
        $("#clsTime1").empty();
        $("#clsTime2").empty();
        $("#newClss").empty();
        $("#clsSelection").removeClass('hidden');

        $.get("/Student/GetData", { GradeId: $("#grdName").val() }, function (data) {



            var response = data.returnList;


            for (i = 0; i < response.length; i++) {

                $("#hdnScheduleId").val(response[i].Id);
                $("#clsName").val(response[i].ClassDate);
                $("#clsDate").val(response[i].ClassDateId);
                $("#hdnTime1").val(response[i].StartTime);
                $("#hdnTime2").val(response[i].EndTime);

                var time1 = $("#hdnTime1").val();
                var time2 = $("#hdnTime2").val();
                var dateId = $("#clsDate").val();
                var dateName = $("#clsName").val();
                var scheduleId = $("#hdnScheduleId").val();


                $("#clsTime1").append('<p id="clsTime1" style="float:left;"> ' + time1 + '</p><br/>');
                $("#clsTime2").append('<p id="clsTime1" style="float:right;margin-left:20px;"> ' + time2 + '</p><br/>');

                $("#newClss").append('<p class="col-sm-5">' + time1 + '</p>');
                $("#newClss").append('<p  class="col-sm-5">' + time2 + '</p>');

                $("#clsName").append('<p id="clsName" style="margin-top:5px;"> ' + dateName + '</p>');
                $("#clsDate").append('<input type="radio"  value="' + dateId + '"   data-id="' + scheduleId + '" name="dates" style="margin-top:20px;"/>');



            }
  

            });

    });


    $(".editDropDown").change(function () {
        $("#editClsName").empty();
        $("#editClsDate").empty();
        $("#editClsTime1").empty();
        $("#editClsTime2").empty();
        $("#newCls").empty();

        $.get("/Student/GetEditData", { GradeId: $("#editGrdName").val() }, function (data) {

            var response = data.returnList;

            for (i = 0; i < response.length; i++) {

                $("#editHdnScheduleId").val(response[i].Id);
                $("#editClsName").val(response[i].ClassDate);
                $("#editClsDate").val(response[i].ClassDateId);
                $("#editClsTime1").val(response[i].StartTime);
                $("#editClsTime2").val(response[i].EndTime);

                var time1 = $("#editClsTime1").val();
                var time2 = $("#editClsTime2").val();
                var dateId = $("#editClsDate").val();
                var dateName = $("#editClsName").val();
                var scheduleId = $("#editHdnScheduleId").val();


                $("#editClsTime1").append('<p id="editClsTime1" style="float:left;"> ' + time1 + '</p><br/>');
                $("#editClsTime2").append('<p id="editClsTime2" style="float:right;margin-left:20px;"> ' + time2 + '</p><br/>');

                $("#newCls").append('<p class="col-sm-5">' + time1 + '</p>');
                $("#newCls").append('<p class="col-sm-5">' + time2 + '</p>');

                $("#editClsName").append('<p id="editClsName" style="margin-top:5px;"> ' + dateName + '</p>');
                $("#editClsDate").append('<input type="radio" value="' + dateId + '"   data-id="' + scheduleId + '" name="editDates" style="margin-top:20px;"/>');
                
            }

        
        });

    });

    $(".studentNameCheck").on("change paste keyup", function () {
        if ($(this).val() != "") {
            $(".studentNameEmpty").addClass('hidden');
        }
        else {
            $(".studentNameEmpty").removeClass('hidden');
        }
    });
    $(".studentIdCheck").on("change paste keyup", function () {
        if ($(this).val() != "") {
            $(".studentIdEmpty").addClass('hidden');
        }
        else {
            $(".studentIdEmpty").removeClass('hidden');
        }
    });
    $(".studentScheduleCheck").on("change paste keyup", function () {
        if ($(this).val() != "") {
            $(".studentScheduleEmpty").addClass('hidden');
        }
        else {
            $(".studentScheduleEmpty").removeClass('hidden');
        }
    });
    $(".studentEmailCheck").on("change paste keyup", function () {
        if (IsEmail($("#email").val()) == true) {
            $(".studentEmailInvalid").addClass('hidden');
        }
        else if ($(this).val() != "" && IsEmail($("#email").val()) == false) {
            $(".studentEmailInvalid").removeClass('hidden');
        }
    });
});



function ScheduleDropDown(grdVal) {
    $("#schedule").empty();
    $("#schedule").append('<option value="">Classes</option>');

    $.ajax({
        type: "GET",
        url: "/Student/GetDropDownVal?grdVal=" + grdVal,
        contentType: "application/json;charset=utf-8",
        success: function (response) {
            var data = (response.scheduleViewModels);

            for (var i = 0; i < data.length; i++) {
                var day = data[i].DateName;
                var startTime = data[i].StartTime;
                var endTime = data[i].EndTime;
                var schId = data[i].ScheduleId;

                $("#schedule").append($("<option></option>").val(schId).text(day + "-" + startTime + "-" + endTime));
            }

        }
    });
}

function SaveData() {
    var dataObj = [];
    var obj = [];

    $("input[name='libDetails']:checked").each(function () {
        var val = $(this).val();
        obj.push({ Id: val });

    });

    var stdId = $("#studentId").val();
    var stdName = $("#studentName").val();

 
    var id = $("input[name='dates']:checked").data("id");

 

    var tel = $("#telephoneNumber").val();
    var mob = $("#mobileNumber").val();
    var email = $("#email").val();

    $("input[name='dates']:checked").each(function () {
        dataObj.push({Id: id, StudentId: stdId, Name: stdName, MobileNo: mob, LandNo: tel, Email: email});

    });

    if ($("#studentName").val() == "") {
        $(".studentNameEmpty").removeClass('hidden');  
        return false;
    }
    if ($("#studentId").val() == "") {
        $(".studentIdEmpty").removeClass('hidden');  
        return false;
    }
    if (IsEmail($("#email").val()) == false) {
        $(".studentEmailInvalid").removeClass('hidden');
        return false;
    }
    if (id == undefined) {
        $(".studentScheduleEmpty").removeClass('hidden');
        return false;
    }

    else {
        
        $(".studentIdEmpty").addClass('hidden');
        $(".studentNameEmpty").addClass('hidden');
        $(".studentScheduleEmpty").addClass('hidden');
        $(".studentEmailInvalid").addClass('hidden');

        $.ajax({

            type: "POST",
            url: "/Student/SaveData",
            contentType: "application/json",
            data: JSON.stringify(dataObj[0]),
            success: function (response) {
                if (response == "True") {
                    SaveLibData(stdId);
                    toastr.success('Saved student...');
                    window.location.href = 'Index/';
                }
                else {
                    toastr.error("Already created student record");
                }
            }

        });

    }
 
}


function addStudent() {
    $(".studentNameEmpty").addClass('hidden');
    $(".studentIdEmpty").addClass('hidden'); 
    window.location.href = '/Student/AddStudent';

}

function getStudentAddFormData() {
    $.ajax({
        type: "GET",
        url: "/Student/GetMaxValue",
        contentType: "application/json",
        success: function (response) {
            console.log(response.maxVal);
            var suggest = response.maxVal + 1;
            $("#studentId").val(suggest);
           
        }
    });

    Library();
}

function studentGrid() {
    var table = $("#studentTable").DataTable();
    table.destroy();

    var grdVal = $("#Grade").val();
    var scheduleVal = $("#schedule").val();

    if (scheduleVal == 'value') {
        scheduleVal = null;
    }

    var table = $('#studentTable').dataTable({
        "responsive": true,
        //"sAjaxSource": "/Student/GetList",
        "ajax": {
            'url': '/Student/GetList',
            'type': 'GET',
            'contentType': "application/json",
            "data": { grade: grdVal, scheduleId: scheduleVal },
        },
        "bProcessing": true,
        "aoColumnDefs": [            
            { "searchable": false, "targets": [2] },
            { "width": "10px", "className": "dt-center", "targets": 1 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 8 },
            { "width": "10px", "className": "dt-center text-wrapper-1 ;  ;", "targets": 9 },
        ],
        "aoColumns": [
            {
                "render": function (data, type, full, meta) { return '<input type="checkbox" class="check" value=' + full.Id + '>'; }
            },
         
            { "data": "StudentId" },
            { "data": "Name" },
            { "data": "MobileNo" },
            { "data": "Email" },
            { "data": "LandNo" },
            { "data": "GradeName" },
            { "data": "ClassDate" },
            {
                "render": function (data, type, full, meta) { return '<a class="btn btn-edit glyphicon glyphicon-edit"  style="float:center;" onclick="EditStudent(' + full.Id + ' ) " "></a>'; }
            },

            {
                data: null, render: function (data, type, row) {
                    return "<a class=' glyphicon glyphicon-trash' style='float:center;color:red;margin-top:10px;' onclick='DeleteData(" + row.Id + ")'' ></a>";
                }
            },
        ]

    });
    
}

var selectedItems = [];

$(document).on("change", ".check", function () {
    if ($(this).is(':checked')) {
        selectedItems.push($(this).val());
    }
    else {
        for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i] === $(this).val()) {
                selectedItems.splice(i, 1);
                i--;
            }
        }
    }
});



function sendEmailModal() {
    $("#emailContentModal").modal();

}


function sendSingleEmail() {
    $('.emailTitleEmpty').addClass('hidden');
    $('.emailContentEmpty').addClass('hidden');

    var subject = $("#emailSubject").val();
    var content = $("#emailContent").val();
    var emailData = { Subject: subject, Content: content, Selected: selectedItems };

    if (selectedItems.length <= 0) {
        toastr.error("no students selected");
        return false;
    }
    if ($("#emailSubject").val() == "") {
        $('.emailTitleEmpty').removeClass('hidden');
        return false;
    }
    if ($("#emailContent").val() == "") {
        $('.emailContentEmpty').removeClass('hidden');
        return false;
    }

    else {
        $.ajax({

            type: "POST",
            url: "/Student/SendEmail",
            contentType: "application/json",
            data: JSON.stringify(emailData),
            success: function (response) {
                location.reload();
                $('#studentTable').dataTable().fnDraw();
                toastr.success("Email sent");

            }
        });
    }


}

function EditStudent(Id) {
    window.location.href = '/Student/EditStudent?Id=' + Id;
}

function GetSchedule(Id) {

    //$("#editStudentmodal").modal('show');
    $(".studentNameEmpty").addClass('hidden');
    $(".studentIdEmpty").addClass('hidden'); 
    $("#editClsName").empty();
    $("#editClsDate").empty();
    $("#editClsTime1").empty();
    $("#editClsTime2").empty();
    $("#newCls").empty();
    $("#inactiveNowGrade").addClass('hidden');
   

    $.ajax({

        type: "GET",
        url: "/Student/GetSchedule?Id=" + Id,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(),
        success: function (data) {
            var response = data.studentViewModals;
            if (response[0].GradeCurrentlyInactive) {
                $("#inactiveNowGrade").removeClass('hidden');
            }

            $("#editStudentId").val(response[0].StudentId);
            $("#editStudentName").val(response[0].Name);
            $("#editGrdName").val(response[0].GradeId);
            $("#editTelephoneNumber").val(response[0].LandNo);
            $("#editMobileNumber").val(response[0].MobileNo);
            $("#editEmail").val(response[0].Email);
            $("#hdnStuId").val(response[0].Id);
            $("#editHdnScheduleId").val(response[0].ClassScheduleId);

            for (i = 0; i < response.length; i++) {

                $("#editClsName").val(response[i].ClassDate);
                $("#editClsDate").val(response[i].ClassDateId);
                $("#editClsTime1").val(response[i].StartTime);
                $("#editClsTime2").val(response[i].EndTime);

                var dateName = $("#editClsName").val();
                var time1 = $("#editClsTime1").val();
                var time2 = $("#editClsTime2").val();
               
                $("#newCls").append('<p style="float:left;">' + time1 + '</p>');
                $("#newCls").append('<p style="float:right;">' + time2 + '</p>');

                $("#editClsName").append('<p id="clsName" style="margin-top:5px;"> ' + dateName + '</p><br/>');
                $("#editClsTime1").append('<p id="editClsTime1"> ' + time1 + '</p>');
                $("#editClsTime2").append('<p id="editClsTime2"> ' + time2 + '</p><br/>');

                
            }
            GetCategories(Id);

        }
    });

    
}


function GetCategories(Id) {
    $("#editCategories").empty();
    
    obj1 = [];
    obj2 = [];
    obj3 = [];

    $.ajax({
        type: "GET",
        url: "/Student/GetCategories?Id=" + Id,
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(),
        success: function(data) {
            var response = data.studentViewModals;

            console.log(response);
            //for (var i = 0; i < response.length; i++) {
            //    $("#editCategories").append('<input type="checkbox" style="margin-left:20px;" value="' + response[i].CategoryId + '" name="libDetails" checked>' + '<label id="catName" style="margin-left:10px;">' + response[i].CategoryName + '</label><br>');
            //}

            $.ajax({

                type: "GET",
                url: "/Student/GetLibDetails",
                contentType: "application/json;charset=utf-8",
                data: JSON.stringify(),
                success: function (data) {
                    var list = data.libraryViewModals;

                    console.log(list);
                    for (var i = 0; i < list.length; i++) {
                        
                        obj1.push(list[i].CategoryId);
                    }
                    for (var j = 0; j < response.length; j++) {
                        obj2.push(response[j].CategoryId);
                        $("#editCategories").append('<input type="checkbox" class="cat"  value="' + response[j].CategoryId + '" name="libDetails" style="margin-right:5px;" checked>' + '<p id="catName" style="float:left;">' + response[j].CategoryName + '</p><br>');
                    }

                    console.log(obj2);

                    obj1 = obj1.filter(function (el) {
                        return !obj2.includes(el);
                    });


                    console.log(obj1);
                    for (var i = 0; i < obj1.length; i++) {
                        for (var j = 0; j < list.length; j++) {
                            if (obj1[i] == list[j].CategoryId) {
                                obj3.push({id: list[j].CategoryId, name: list[j].CategoryName});
                            }
                        }
                    }
                    console.log(obj3);

                    for (var i = 0; i < obj3.length; i++) {
                        $("#editCategories").append('<input type="checkbox" class="cat"  value="' + obj3[i].id + '" name="libDetails" style="margin-right:5px;">' + '<p id="catName" style="float:left;">' + obj3[i].name + '</p><br>');
                    }
            
                }
              
            });

        
        }
    });

}


function UpdateData() {
    var tel = $("#editTelephoneNumber").val();
    var mob = $("#editMobileNumber").val();
    var email = $("#editEmail").val();
    var stuName = $("#editStudentName").val();
    var stuId = $("#editStudentId").val();


    
    var id = $("#hdnStuId").val();
    var scheduleId = $("input[name='editDates']:checked").data("id");
    var currentScheduleId = $("#editHdnScheduleId").val();

    if (stuName == "") {
        return false;
    }

    $("input[name='editDates']:checked").each(function () {
        currentScheduleId = scheduleId;

    });

    $.ajax({

        type: "GET",
        url: "/Student/UpdateData",
        contentType: "application/json",
        data: { ClassScheduleId: currentScheduleId, Id: id, MobileNo: mob, LandNo: tel, studentName: stuName, studentId: stuId, Email: email },
        success: function (response) {

            UpdateLibData(id);
            //$('#studentTable').dataTable().fnDraw();
            //toastr.success('Student updated..');

        }


    });

}


function DeleteData(Id) {
    Delete(Id);
}

function Delete(Id) {
    var url = "/Student/DeleteStudent";

    toastr.warning("<br /><br /><button type='button' id='confirmationRevertYes' class='btn clear'>Yes</button>", 'delete student?',
        {
            closeButton: false,
            allowHtml: true,
            onShown: function (toast) {
                $("#confirmationRevertYes").click(function () {
                    $.post(url, { Id: Id }, function (data) {
                        if (data == "Deleted") {
                            $('#gradesTable').dataTable().fnDraw();
                            toastr.success('Student deleted...');
                            location.reload();
                        }
                        else if (data == "LoggedOut") {
                            window.location.href = "/User/Login/"
                        }
                        else {
                            toastr.error('Something Went Wrong...');
                        }
                    });
                });
            }
        });
}

function Library() {
    $("#categories").empty();

    $.ajax({

        type: "GET",
        url: "/Student/GetLibDetails",
        contentType: "application/json;charset=utf-8",
        data: JSON.stringify(),
        success: function (data) {
           var list = data.libraryViewModals;

            console.log(list);
            for (var i = 0; i < list.length; i++) { 
                $("#categories").append('<input type="checkbox"  value="' + list[i].CategoryId +'" name="libDetails" style="margin-right:5px;">' +'<p id="catName" style="float:left;">' + list[i].CategoryName + '</p><br>');
            }
            
        }

    });
}



function SaveLibData(stdId) {
    var obj = []

    $("input[name='libDetails']:checked").each(function () {
        var val = $(this).val();
        obj.push({ Id: val});

    });

    //obj.push({ StudentId: stdId });

    if (obj.length == 0) {
       // alert("no book category selected");
        location.reload();
        $('#studentTable').dataTable().fnDraw();
    }

    else {
        console.log(obj);
        $.ajax({

            type: "POST",
            url: "/Student/SaveLibDetails?stdId=" + stdId ,
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(obj),
            success: function (data) {
                var response = data.libraryViewModals;
                console.log(response);
                location.reload();
                $('#studentTable').dataTable().fnDraw();
                toastr.success('Student saved..');
            }
        });
    }

}

function UpdateLibData(id) {
    var obj = []

    $("input[name='libDetails']:checked").each(function () {
        var val = $(this).val();
        obj.push({ Id: val });

    });

    //obj.push({ StudentId: stdId });

        $.ajax({

            type: "POST",
            url: "/Student/UpdateLibDetails?id=" + id,
            contentType: "application/json;charset=utf-8",
            data: JSON.stringify(obj),
            success: function (data) {
                var response = data.libraryViewModals;
                window.location.href = 'Index/';
                $('#studentTable').dataTable().fnDraw();
                toastr.success('Student updated..');
            }
        });

}