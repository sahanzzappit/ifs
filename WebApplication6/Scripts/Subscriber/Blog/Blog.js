﻿$(document).ready(function () {
    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });

    $('.summernote').summernote({
        height: 500,
        callbacks: {

        },
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'fileupload']],
            ['view', ['fullscreen', 'codeview', 'help']]
        ],
        styleTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
    });
});


function publishBlog() {
    var heading = $("#blogHeading").val();
    var blogContent = $(".note-editable").html();
    var text = $(".summernote").summernote("code").replace(/&nbsp;|<\/?[^>]+(>|$)/g, "").trim();

    if (heading == "" || heading == null) {
        $("#Heading_required").removeClass('hidden');
        return false;
    }
    else {
        $("#Heading_required").addClass('hidden');
    }

    if (text.length == 0) {
        $("#Content_required").removeClass('hidden');
        return false;
    }
    else {
        $("#Content_required").addClass('hidden');
    }

    var obj = { heading: heading, content: blogContent };
    $.ajax({
        type: "POST",
        url: "/Blog/SaveBlog",
        contentType: "application/json",
        data: JSON.stringify(obj),
        success: function (response) {
            if (response.Response) {
                toastr.success('Blog published...');
                location.reload();
            }
            toastr.success('Blog published...');
        }
    });
}
