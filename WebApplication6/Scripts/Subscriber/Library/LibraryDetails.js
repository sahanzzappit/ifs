﻿$(document).ready(function () {
    studentLibraryDetails();
    $(function () {
        window.FakeLoader.init();
        auto_hide: true
        fade_timeout: 20000
        overlay_id: 'fakeloader-overlay'

    });
});

function studentLibraryDetails() {
    var table = $('#subsLibraryManagementTbl').DataTable();
    table.destroy();
    $(".dataTables_empty").text("There are currently no xyz available for this.");
    var table = $('#subsLibraryManagementTbl').dataTable({
        "responsive": true,
        "sAjaxSource": "/SubscriberLibraryManagement/GetStudentLibraryData",
        "bProcessing": true,
        "oLanguage": {
            "sEmptyTable": "No libary details found"
        },
        "columnDefs": [
        ],
        "aoColumns": [
            {

                "render": function (data, type, full, meta) {
                    if (full.IsAvailable) {
                        return '<i class="glyphicon glyphicon-ok" style="color:green;"></i>';
                    }
                    else {
                        return '<i class="glyphicon glyphicon-remove" style="color:red;"></i>';
                    }

                }
            },
            { "data": "BookNo" },
            { "data": "Name" },
            { "data": "LastLendDate" },
            { "data": "Summary" },
        ],
    });
}