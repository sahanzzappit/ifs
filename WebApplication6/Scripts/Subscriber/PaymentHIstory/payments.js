﻿
$(document).ready(function () {
    getPayemntDet();

});

var studentId = null;
var Year = null;
var Fee = null;

function getPayemntDet() {
    $.ajax({
        type: 'GET',
        url: 'SubscriberPaymentDetail/GetSubscriberPaymentRec',
        contentType: "application/json",
        success: function (response) {
            console.log(response);
            $.each(response.Payments, function (key, value) {

                $("#paymentHis").append('<div class="card text-white bg-success mb-3" style="max-width: 18rem;margin-right:10px;">'+
                    '<div class="card-header"> Paid for - ' + value.Month +'</div>' +
                    '<div class="card-body">' +
                    '<h5 class="card-title">Rs.' + value.Amount +'</h5>' +
                    '<p class="card-text">Paid date - ' + value.PaidDate +' </p>' +
                    '</div>' +
                    '</div >');
            });
            $.each(response.Unpaid, function (key, value) {

                $("#paymentHis").append('<div class="card text-white bg-warning mb-3" style="max-width: 18rem;margin-right:10px;">' +
                    '<div class="card-header"> Unpaid</div>' +
                    '<div class="card-body">' +
                    '<p class="card-text">Pending - ' + value.Name + ' </p>' +
                    '</div>' +
                    '</div >');
            });


            $('#amountOnButton').append(response.Fee);

            studentId = response.StudentId;
            Year = response.PaymentDetailYear;
            Fee = response.Fee;
        }
    });


}

function pay() {

    var month = $("#Select_Month").val();
    console.log(Fee);
    var data = { StudentId: studentId, MonthId: month, Year: Year, Fee: Fee };

    $.ajax({
        type: 'POST',
        url: '/Fees/SavePaid',
        contentType: "application/json",
        data: JSON.stringify(data),
        success: function (response) {


            var delay = 8000;
            if (response == "True") {

                $("#successAlert").modal();
                $('#paidAmount').append(Fee + "Rs.");

                var url = "/Fees/SendInvoice";
                $.post(url, { StudentId: studentId, MonthId: month, Year: Year }, function (data) {

                });
            }

            setTimeout(function () { window.location = "/SubscriberPaymentDetail"; }, delay);
            
        }
    });


}
